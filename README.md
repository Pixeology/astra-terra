# Astra Terra

[![Code Triagers Badge](https://www.codetriage.com/christopherdumas/skyspace/badges/users.svg)](https://www.codetriage.com/christopherdumas/skyspace)

## A Survival City Building Roguelike

![Main Screenshot](assets/screenshots/Forest%20Scenery.png?raw=true)
![](assets/screenshots/Paninsula%20Paridise.png?raw=true)
![](assets/screenshots/Lagoon.png?raw=true)

In *Astra Terra* you are the first settler on a new world
designated for colonization by Starways Contress. Your four colonists
are all alone, but if you succed in showing your kind that this new
frontier is habitable and profitable, more settlers will join you.

*Astra Terra* is a strategically deep, complex, and permissive
world. While it may be complex, with intricate systems allowing you to
do largely whatever you want that makes sense, it aims to have a
simple, useable user interface. It wants to be your sandbox: there are
as few restrictions as possible on what you can do. You start with
only a few tools in your toolbox and through your own prowess, smarts
and enginuity you can expand your abilities. There's no limit to how
far you can go!

In *Astra Terra* you start out with four characters. These
are your first settlers. You designate jobs and your settlers
will proceed to do them. You can build workshops that allow your settlers
to do things that they can't do without help, such as build tools to help
them chop down trees and kill animals. Your goal for the first part of the game
is to survive. If you keep your settlers alive long enough, others will start
coming to help you. Each character has their own special talents, but
they can learn new ones as you assign jobs. You can tell each
character what jobs they're allowed to do, and which they are not. By
default, they'll be allowed to do some basic things to keep themselves
alive and help others, as well as their beginning talents. As they
gain skill in certain areas, they'll start being able to expand to
similar areas, as well as completing the jobs they're talented at
faster.

In *Astra Terra* your entire environment is interacive. You can
cut down trees, gather plants, delve into mountains, re-channel
rivers, dig holes, harness the flow of magma from underground and
build any number of things. Plants have different medicinal properties
and stones have different values and strengths. Woods have different
values, strengths, and weights, and water can be varying levels of
dirty. Rope, levers, pressure plates, mills, wheels, drawbridges,
waterwheels, corkscrews, tracks, steam and plenty of other mechanical
elements will be added eventually to allow for even more fun.

When managing a *Astra Terra* settlement, as the seasons come and go you
must provide food, shelter, occupation, and creature comforts for your
characters, or they might die or go insane. You must also defend not
only against the elements but also against beasts and malevolent
aliens.

## Installation

For now, since the project is currently in development, we do not have a binary executable. Instead you will have to...

- Install SDL2
- Install Rust
- Install Cargo (should come with Rust)
- Download the project
- Run `cargo run` in the project's directory

## Feature List (1000ft view)

After about very two-three stages, I'll do some kind of codebase
cleanup, from a refactor to prehaps a performance issue.

- [x] Overall world generation (mountains, hills, plants, cliffs, water)
- [x] UI
- [x] Time
- [x] Seasons
- [x] Animal AI (basic escaping)
- [x] Environmental effects (biomes, weather)
- [x] Settler AI
- [ ] Movement, place designation for settler AI
- [ ] Switch to BearLibTerminal for better graphics.
- [ ] Destrutive directives (chop down trees, delve, etc) for settler AI
- [ ] Settler health/hunger/water/centralized colony tracking stuff
- [ ] Constructive directives for settler AI
- [ ] Battle/RTS stuff
- [ ] Fluid Physics
- [ ] Workshops, constructions
- [ ] Machenery
- [ ] Tech tree
- [ ] Multiplayer

## More Info

- [screenshots](assets/screenshots) are available.
- Christopher can be emailed at any time.
- For information on contributing, see [CONTRUBITING.md](CONTRIBUTING.md)
- For contact information, see [CREDITS.md](CREDITS.md)

# Dev Info

- `src/life/animal.rs` line 26 contains the registry for all
animals. The format is, put the names of the animals (seperated by
commas) along with a comment BEFORE the animal name concerning what
the animal does special.
- `src/life/animal.rs` line 88 contains the registry for all the
static properties of the animal bodies: health, tile used, etc.  Non
character tiles (stuff that doesn't look like `'x'`), should be: the
number of the tile, counting from the first tile in the tileset (but
not the text!) plus 256, in this format: ` std::char::from_u32(256 +
x).unwrap()` where `x` is 256 plus the number of the tile.
- `src/worldgen/terrain.rs` line 20 contains the registry for all terrain tiles.
- Fish tiles with backround should be used until BLT.
- Biomes- **Desert, Forest, Grasslands, Ocean/Islands, Swamp.**
- Ideas: Chests spawning randomly with items of varying rarity, underater bases and rideable life.

## Docs:

```
cargo rustdoc --open -- --no-defaults --passes collapse-docs --passes unindent-comments --passes strip-priv-imports
```