extern crate rand;
use self::rand::Rng;
use crate::physics::PhysicsActor;
use std::any::Any;
use std::collections::HashMap;

use crate::life::{
    Living, Mood, Order, OrderTypes, Species, SpeciesProperties,
    WorldDiff,
};
use crate::utils;
use crate::utils::math::*;
use crate::utils::misc::find_path;
use std;
use std::cell::RefCell;

use crate::worldgen::terrain::{Item, Material, StoneTypes, Tile};
use crate::worldgen::{ItemType, Orders, Unit, World};

fn item_type_eq(a: ItemType, b: ItemType) -> bool {
    use crate::ItemType::*;
    match (a, b) {
        (Wood, Wood) => true,
        (Metal, Metal) => true,
        (Stone, Stone) => true,
        (Tool, Tool) => true,
        _ => false,
    }
}

fn replace_block_setup(
    mine_point: &Point3D,
    tile: Option<Tile>,
) -> IntermediateOrder {
    let mut trng = rand::thread_rng();
    match tile {
        Some(Tile::Stone(StoneTypes::Soil(..), _)) => {
            IntermediateOrder::RemoveBlock(*mine_point)
        }
        Some(Tile::Stone(st, state)) => {
            if trng.gen_weighted_bool(4) {
                IntermediateOrder::MineBlock(
                    *mine_point,
                    Tile::Item(Item::Material(
                        utils::misc::get_material_from(st, state),
                    )),
                )
            } else {
                IntermediateOrder::RemoveBlock(*mine_point)
            }
        }
        _ => IntermediateOrder::RemoveBlock(*mine_point),
    }
}

fn replace_stack_setup(
    mine_point: &Point3D,
    tile: Option<Tile>,
) -> IntermediateOrder {
    let tree_height = match tile {
        Some(Tile::Vegetation(_, h, _)) => h as usize + 1,
        _ => 0,
    };
    let replacement = match tile {
        Some(Tile::Vegetation(vt, h, _)) => {
            vec![Tile::Item(Item::Material(Material::Wood(vt)))]
        }
        _ => vec![],
    };
    IntermediateOrder::ReplaceStack(
        *mine_point,
        tree_height,
        replacement,
    )
}

#[derive(Clone, PartialEq, Debug)]
pub enum ActionResult {
    Done(WorldDiff),
    Continue(WorldDiff),
    Error,
}
#[derive(Copy, Clone, PartialEq, Debug)]
/// Specific addons that determine the speed and abilities of the robot,
/// including which jobs they will attempt to fulfill. Addons can draw or give
/// power buffs, effect the speed of the completion of orders, etc.
pub enum Addon {
    MiningLaser,
    LifterArm,
    MachineGun,
    RifleBoreGun,
    PlateArmor,
    LithiumIonCore,
    Empty,
}

/// Addon statistics
pub struct AddonStats {
    pub speed_buff: i32,
    pub armor_value: usize,
    pub power_draw: usize,
    pub power_boost: usize,
}

impl Addon {
    /// Returns the proper directional image (FL, FR, BL, BR) of the addon
    pub fn get_graphic(&self, delta: (i32, i32)) -> String {
        let prefix = utils::misc::direction(delta);
        use self::Addon::*;
        prefix.to_owned()
            + match &self {
                &MiningLaser => "_mining_laser",
                &LithiumIonCore => "_lithium_ion_core",
                _ => "_empty",
            }
    }

    /// Determines whether an addon can be used for a task
    pub fn can_be_used_for(&self, o: OrderTypes) -> bool {
        use self::Addon::*;
        match self {
            &MiningLaser => {
                o == OrderTypes::Forestry || o == OrderTypes::Mining
            }
            &LifterArm => o == OrderTypes::Construction,
            &MachineGun => o == OrderTypes::Fighting,
            &RifleBoreGun => o == OrderTypes::Fighting,
            &PlateArmor => o == OrderTypes::Fighting,
            &LithiumIonCore => o == OrderTypes::Movement,
            &Empty => false,
        }
    }

    pub fn get_stats(&self) -> AddonStats {
        use self::Addon::*;
        match self {
            &RifleBoreGun => AddonStats {
                speed_buff: 0,
                armor_value: 5,
                power_draw: 10,
                power_boost: 0,
            },
            &PlateArmor => AddonStats {
                speed_buff: 0,
                armor_value: 20,
                power_draw: 0,
                power_boost: 0,
            },
            &LifterArm => AddonStats {
                speed_buff: 0,
                armor_value: 5,
                power_draw: 50,
                power_boost: 0,
            },
            &MiningLaser => AddonStats {
                speed_buff: 0,
                armor_value: 5,
                power_draw: 50,
                power_boost: 0,
            },
            &MachineGun => AddonStats {
                speed_buff: 0,
                armor_value: 5,
                power_draw: 30,
                power_boost: 0,
            },
            &LithiumIonCore => AddonStats {
                speed_buff: 0,
                armor_value: 0,
                power_draw: 0,
                power_boost: 100,
            },
            &Empty => AddonStats {
                speed_buff: 0,
                armor_value: 0,
                power_draw: 0,
                power_boost: 0,
            },
        }
    }
}

pub fn random_speed(rng: &mut rand::IsaacRng) -> (usize, bool) {
    let speed = rng.gen_range(40, 100);
    (speed, speed > 60)
}

fn felltrees_test(u: &Unit, p: Point3D) -> Option<usize> {
    get_tile!(tiles: unit = u);
    tiles
        .iter()
        .enumerate()
        .find(|(_, t)| utils::misc::is_tree(**t))
        .map(|(i, _)| i)
}

fn mining_test(u: &Unit, (_, _, z): Point3D) -> Option<usize> {
    get_tile!(tile: unit = u, z);
    tile.map(|t| t.solid())
        .and_then(|s| if s { Some(z) } else { None })
}

fn build_test(u: &Unit, (_, _, z): Point3D) -> Option<usize> {
    get_tile!(tile: unit = u, z);
    match tile {
        Some(Tile::Empty(_)) => Some(z),
        _ => None,
    }
}

fn haul_test(u: &Unit, _: Point3D) -> Option<usize> {
    let mut z = 0;
    let arc = u.tiles.clone();
    let tiles = arc.read().unwrap();
    for tile in tiles.iter() {
        z += 1;
        if !tile.solid() && !matches!(tile, Tile::Item(..)) {
            break;
        }
    }
    if z >= tiles.len() {
        None
    } else {
        Some(z)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum IntermediateOrder {
    MineBlock(Point3D, Tile),
    RemoveBlock(Point3D),
    BuildBlock(Point3D, ItemType, Tile),
    FollowPath(Vec<Point3D>),
    ReplaceBlock(Point3D, Tile),
    ReplaceStack(Point3D, usize, Vec<Tile>),
    GrabItem(Point3D),
    DropItem(Point3D),
}

#[derive(Clone)]
/// This is an autonimous AI that completes tasks that they player adds to the
/// queue of tasks, and therefore is indirectly controllable by the player.
pub struct Drone {
    /// The path (out of acceptable paths, modulo the amount of acceptable
    /// paths) to take when doing pathfinding. This is useful when trying to
    /// find an alternate route
    number_of_sharers: usize,
    sharers_at_start: usize,
    path_index: usize,
    step_index: usize,
    total_power: usize,
    remaining_power: i32,
    finished_list: Vec<Order>,
    standing_on: Tile,
    /// The number of the attempt to do something
    r#try: usize,
    /// Whether it has finished a job, succesfully or not
    frame_last_action: usize,
    addon_count: usize,
    pub steps: Vec<IntermediateOrder>,
    pub current_order: Option<Order>,
    pub name: String,
    pub inventory: Vec<Item>,
    pub id: usize,
    pub addons: [Addon; 3],
    pub species: SpeciesProperties,
    pub pos: (usize, usize, usize),
}

impl Drone {
    pub fn new(id: usize, pnt: Point3D, addons: [Addon; 3]) -> Drone {
        let total_power = addons
            .iter()
            .map(|x| x.get_stats().power_boost)
            .fold(0, |acc, x| acc + x);
        Drone {
            sharers_at_start: 0,
            number_of_sharers: 0,
            standing_on: Tile::Empty(None),
            step_index: 0,
            r#try: 0,
            path_index: 0,
            finished_list: vec![],
            steps: vec![],
            total_power: total_power,
            remaining_power: total_power as i32
                - addons
                    .iter()
                    .map(|x| x.get_stats().power_draw)
                    .fold(0, |acc, x| acc + x)
                    as i32,
            addon_count: addons
                .iter()
                .filter(|x| **x != Addon::Empty)
                .count(),
            frame_last_action: 0,
            species: SpeciesProperties {
                speed: 8,
                health: 225,
                chr: '\0',
                sight: 15,
                mood: Mood::Wary,
                species: Species::Robot,
            },
            name: utils::misc::namegen(),
            inventory: vec![],
            id: id,
            addons: addons,
            pos: pnt,
            current_order: None,
        }
    }

    fn finished_job(
        &mut self,
        ws: &World,
        time: usize,
        dt: usize,
        frame: usize,
        orders: &mut Orders,
        stockpiles: &mut HashMap<ItemType, usize>,
    ) {
        if let Some(ref order) = self.current_order {
            let mut remove = None;
            for (i, (o, n)) in orders.iter_mut().enumerate() {
                if *o == *order {
                    *n = std::cmp::max(0, *n as i32 - 1) as usize;
                    self.finished_list.push(o.clone());
                    if *n < 1 {
                        remove = Some(i);
                    }
                    break;
                }
            }
            if let Some(i) = remove {
                if i < orders.len() {
                    orders.remove(i);
                }
            }
        }
        self.step_index = 0;
        self.sharers_at_start = 0;
        self.number_of_sharers = 0;
        self.steps = vec![];
        self.current_order = None;
    }

    pub fn insert_addon(&mut self, i: usize, a: Addon) {
        self.addons[i] = a;
        self.addon_count = self
            .addons
            .iter()
            .filter(|x| **x != Addon::Empty)
            .count();
        self.total_power = self
            .addons
            .iter()
            .map(|x| x.get_stats().power_boost)
            .fold(0, |acc, x| acc + x);
        self.remaining_power = self.total_power as i32
            - self
                .addons
                .iter()
                .map(|x| x.get_stats().power_draw)
                .fold(0, |acc, x| acc + x) as i32;
    }

    fn in_sight(&self, map: &World) -> Vec<(Unit, Point3D)> {
        let r = self.species.sight as i32;
        let mut in_sight = vec![];
        for x in (-r)..r {
            for y in (-r)..r {
                let posn = (
                    (self.pos.0 as i32 + x).abs() as usize,
                    (self.pos.1 as i32 + y).abs() as usize,
                );
                let maybe_unit = map.get(posn);

                if let Some(unit) = maybe_unit {
                    in_sight.push((
                        unit,
                        (
                            posn.0,
                            posn.1,
                            utils::misc::location_z(&map.map, posn),
                        ),
                    ));
                }
            }
        }
        in_sight
    }

    /// Find and adds a path to a certain spot. Pretty much just an
    /// interface/wrapper for the utils pathfinding stuff.
    fn make_path_to(
        &mut self,
        map: &World,
        goal: Point3D,
        partial_ok: bool,
    ) -> Option<Vec<Point3D>> {
        let start = self.pos;
        let s = self.species.species.properties();

        // get point if in map
        map.get((goal.0, goal.1))
            // get tile if in z axis
            .and_then(|unit| {
                let arc = unit.tiles.clone();
                let tiles = arc.read().unwrap();
                tiles.get(goal.2).cloned()
            })
            // If not, just pretend its empty
            .or(Some(Tile::Empty(None)))
            // get adjusted point if traversable at all
            .and_then(|goal_tile| {
                utils::misc::is_traversable(&map.map, goal)
            })
            // get path if there is one
            .and_then(|goal_point| {
                let mpath = find_path(
                    &map.map,
                    start,
                    goal_point,
                    |_map, _point| true,
                );

                mpath
            })
            // or print an error
            .or_else(|| {
                println!("{} - No path to goal: {:?}", self.id, goal);
                None
            })
    }

    /// Perform the action the drone was sent on a mission to do, if it has
    /// arrived in a spot where it can do that action.
    fn can_do(&self, o: &Order) -> bool {
        let ot = o.order_type();
        self.addons.iter().find(|x| x.can_be_used_for(ot)).is_some()
    }

    fn buff_for_order(&self, o: &Order) -> i32 {
        let ot = o.order_type();
        self.addons
            .iter()
            .find(|x| x.can_be_used_for(ot))
            .map(|x| x.get_stats().speed_buff)
            .unwrap_or(0)
    }

    fn speed_for_order(&self, o: &Order) -> usize {
        (std::cmp::min(
            std::cmp::max(1, 50 - self.buff_for_order(o)),
            40,
        ) / 8) as usize
    }

    pub fn total_power(&self) -> usize {
        self.total_power
    }
    pub fn remaining_power(&self) -> i32 {
        self.remaining_power
    }
    pub fn create_steps(
        &mut self,
        ws: &World,
        ns: usize,
        stockpiles: &mut HashMap<ItemType, usize>,
    ) -> Option<WorldDiff> {
        self.path_index = 0;
        self.current_order.and_then(|order| {
            self.create_steps_for_order(order, ws, stockpiles, ns)
        })
    }

    fn get_order_selection(
        &self,
        amount: usize,
        number_of_sharers: usize,
    ) -> (usize, usize) {
        let bite_size = amount / number_of_sharers;
        let bite_index = self.sharers_at_start;
        let start = bite_size * bite_index;
        let end = start + bite_size;
        (start, end)
    }

    fn create_steps_for_order(
        &mut self,
        order: Order,
        ws: &World,
        stockpiles: &mut HashMap<ItemType, usize>,
        number_of_sharers: usize,
    ) -> Option<WorldDiff> {
        match order {
            Order::Go(pnt) => {
                if let Some(path) = self.make_path_to(&ws, pnt, true)
                {
                    self.steps
                        .push(IntermediateOrder::FollowPath(path));
                    Some(WorldDiff::NoResult)
                } else {
                    None
                }
            }
            Order::BuildBlock(rect, itype) => {
                if let Some(stock) = stockpiles.get(&itype) {
                    if *stock > 0 {
                        let build_points = utils::misc::rect_points(
                            &ws.map, rect, build_test,
                        );
                        let (start, end) = self.get_order_selection(
                            build_points.len(),
                            number_of_sharers,
                        );
                        for i in start..end {
                            let build_point = build_points[i];
                            let to_stand =
                                utils::math::strict_3d_adjacent(
                                    build_point,
                                );
                            for stand in to_stand {
                                if let Some(path) = self
                                    .make_path_to(&ws, stand, false)
                                {
                                    self.steps.append(&mut vec![
                                        IntermediateOrder::FollowPath(
                                            path,
                                        ),
                                        IntermediateOrder::BuildBlock(
                                            build_point,
                                            itype,
                                            Tile::SolidBlock(
                                                itype.to_item(),
                                            ),
                                        ),
                                    ]);

                                    return Some(WorldDiff::NoResult);
                                }
                            }
                        }
                    }
                }
                None
            }
            Order::Mine(rect)
            | Order::FellTrees(rect)
            | Order::Channel(rect) => {
                let felltrees_mode = matches!(
                    self.current_order.clone(),
                    Some(Order::FellTrees(..))
                );
                let mine_points = utils::misc::rect_points(
                    &ws.map,
                    rect,
                    if felltrees_mode {
                        felltrees_test
                    } else {
                        mining_test
                    },
                );
                let (start, end) = self.get_order_selection(
                    mine_points.len(),
                    number_of_sharers,
                );
                for i in start..end {
                    if let Some(mine_point) = mine_points.get(i) {
                        let mine_point = *mine_point;
                        get_tile!(
                            tile: map = ws,
                            (mine_point.0, mine_point.1),
                            mine_point.2
                        );
                        let to_stand = if felltrees_mode {
                            utils::misc::easy_3d_adjacent(
                                mine_point, &ws.map,
                            )
                        } else {
                            utils::math::strict_3d_adjacent(
                                mine_point,
                            )
                        };
                        for stand in to_stand.iter() {
                            if let Some(path) =
                                self.make_path_to(&ws, *stand, false)
                            {
                                self.steps.append(&mut vec![
                                    IntermediateOrder::FollowPath(
                                        path,
                                    ),
                                    if matches!(
                                        self.current_order.clone(),
                                        Some(Order::FellTrees(_))
                                    ) {
                                        replace_stack_setup(
                                            &mine_point,
                                            tile,
                                        )
                                    } else {
                                        replace_block_setup(
                                            &mine_point,
                                            tile,
                                        )
                                    },
                                ]);
                                return Some(WorldDiff::NoResult);
                            } else {
                                get_tile!(
                                    st: map = ws,
                                    (stand.0, stand.1),
                                    stand.2
                                );
                            }
                        }
                        if utils::math::distance_3d(
                            self.pos, mine_point,
                        ) <= 1
                        {
                            self.steps.append(&mut vec![
                                if matches!(
                                    self.current_order.clone(),
                                    Some(Order::FellTrees(_))
                                ) {
                                    replace_stack_setup(
                                        &mine_point,
                                        tile,
                                    )
                                } else {
                                    replace_block_setup(
                                        &mine_point,
                                        tile,
                                    )
                                },
                            ]);
                            return Some(WorldDiff::NoResult);
                        }
                    }
                }
                None
            }
            _ => None,
        }
    }

    pub fn do_step(
        &mut self,
        step: &IntermediateOrder,
        ws: &World,
        stockpiles: &mut HashMap<ItemType, usize>,
    ) -> ActionResult {
        match step {
            IntermediateOrder::RemoveBlock(place) => {
                ActionResult::Done(WorldDiff::RemoveTile(*place))
            }
            IntermediateOrder::BuildBlock(place, itemtype, block) => {
                get_tile!(
                    tile: map = ws,
                    (place.0, place.1),
                    place.2
                );
                match tile {
                    Some(Tile::Empty(None)) => {
                        *stockpiles.get_mut(itemtype).unwrap() -= 1;
                        println!(
                            "{}: Place {:?} at {:?}",
                            self.id, *block, *place
                        );
                        ActionResult::Done(WorldDiff::ReplaceTile(
                            *place, *block,
                        ))
                    }
                    Some(Tile::Empty(Some(id))) => {
                        if id != self.id {
                            let life = {
                                if let Some(ref life) = ws.life[id] {
                                    life
                                } else {
                                    println!("ERROR: Outdated entity in the way: {}", id);
                                    return ActionResult::Error;
                                }
                            };
                            let mut actor = life.borrow_mut();
                            actor
                                .update_standing_on(*block, &ws.life);
                            ActionResult::Done(WorldDiff::NoResult)
                        } else {
                            self.standing_on = *block;
                            ActionResult::Done(WorldDiff::NoResult)
                        }
                    }
                    Some(Tile::Item(item)) => {
                        if *itemtype == item.get_type() {
                            ActionResult::Done(WorldDiff::NoResult)
                        } else {
                            println!("ERROR: Can't build block on non-empty space!");
                            ActionResult::Error
                        }
                    }
                    _ => {
                        println!("ERROR: Can't build block on non-empty space!");
                        ActionResult::Error
                    }
                }
            }
            IntermediateOrder::FollowPath(path) => {
                let i = self.step_index;
                let opos = self.pos;
                if let Some(npos) = path.get(self.path_index) {
                    if let Some(npos) =
                        utils::misc::is_traversable(&ws.map, *npos)
                    {
                        self.pos = npos;
                        self.path_index += 1;
                        ActionResult::Continue(WorldDiff::Move(
                            opos, self.pos,
                        ))
                    } else if let Some(path) = self.make_path_to(
                        &ws,
                        *path.last().unwrap(),
                        true,
                    ) {
                        get_tile!(
                            tile: map = ws,
                            (npos.0, npos.1),
                            npos.2
                        );
                        self.steps[i] =
                            IntermediateOrder::FollowPath(path);
                        self.path_index = 0;
                        ActionResult::Continue(WorldDiff::NoResult)
                    } else {
                        ActionResult::Error
                    }
                } else {
                    ActionResult::Done(WorldDiff::Move(
                        opos, self.pos,
                    ))
                }
            }
            IntermediateOrder::ReplaceBlock(place, tile) => {
                ActionResult::Done(WorldDiff::ReplaceTile(
                    *place, *tile,
                ))
            }
            IntermediateOrder::MineBlock(place, tile) => {
                if let Tile::Item(item) = tile {
                    *stockpiles
                        .entry(item.get_type())
                        .or_insert(0) += 1;
                }
                ActionResult::Done(WorldDiff::ReplaceTile(
                    *place, *tile,
                ))
            }
            IntermediateOrder::ReplaceStack(place, height, tile) => {
                if let Tile::Item(item) = tile[0] {
                    *stockpiles
                        .entry(item.get_type())
                        .or_insert(0) += height;
                }
                ActionResult::Done(WorldDiff::ReplaceTileStack(
                    *place,
                    *height,
                    tile.iter()
                        .map(|x| x.clone())
                        .collect::<Vec<Tile>>(),
                ))
            }
            IntermediateOrder::DropItem(itempos) => {
                if let Some(item) = self.inventory.pop() {
                    ActionResult::Done(WorldDiff::ReplaceTile(
                        *itempos,
                        Tile::Item(item),
                    ))
                } else {
                    ActionResult::Error
                }
            }
            IntermediateOrder::GrabItem(itempos) => {
                get_tile!(
                    item: map = ws,
                    (itempos.0, itempos.1),
                    itempos.2
                );

                match item {
                    Some(Tile::Item(i)) => {
                        self.inventory.push(i);
                        return ActionResult::Done(
                            WorldDiff::RemoveTile(*itempos),
                        );
                    }
                    Some(_) => {
                        return ActionResult::Error;
                    }
                    None => {
                        return ActionResult::Done(
                            WorldDiff::NoResult,
                        );
                    }
                }
            }
        }
    }

    fn done_order(
        &self,
        ws: &World,
        stockpiles: &mut HashMap<ItemType, usize>,
    ) -> bool {
        self.current_order
            .map(|o| match o {
                Order::FellTrees(rect)
                | Order::Mine(rect)
                | Order::Channel(rect) => {
                    utils::misc::nearest_rect_point(
                        &ws.map,
                        rect,
                        self.pos,
                        if matches!(
                            self.current_order.clone(),
                            Some(Order::FellTrees(..))
                        ) {
                            felltrees_test
                        } else {
                            mining_test
                        },
                    )
                    .is_none()
                }
                Order::BuildBlock(rect, type_filter) => {
                    utils::misc::points_in_rect(&ws.map, rect)
                        .iter()
                        .all(|p| {
                            get_tile!(
                                tile: map = ws,
                                (p.0, p.1),
                                p.2
                            );
                            tile.map_or(true, |t| t.solid())
                        })
                }
                Order::Go(dest) => {
                    self.pos
                        == utils::misc::is_traversable(&ws.map, dest)
                            .or_else(|| Some(self.pos))
                            .unwrap()
                }
                _ => true,
            })
            .unwrap_or(false)
    }
}

impl Living for Drone {
    fn update_standing_on(
        &mut self,
        t: Tile,
        life: &Vec<Option<RefCell<Box<dyn Living>>>>,
    ) {
        if let Tile::Empty(Some(i)) = t {
            if i == self.id {
                self.standing_on = Tile::Empty(None);
            } else if let Some(ref a) = life[i] {
                let actor = a.borrow();
                self.standing_on = actor.standing_on();
            } else {
                self.standing_on = Tile::Empty(None);
            }
        } else {
            self.standing_on = t;
        }
    }
    fn standing_on(&self) -> Tile {
        self.standing_on
    }
    fn identity(&self) -> usize {
        self.id
    }

    fn execute_mission(
        &mut self,
        ws: &World,
        time: usize,
        dt: usize,
        frame: usize,
        orders: &mut Orders,
        stockpiles: &mut HashMap<ItemType, usize>,
    ) -> WorldDiff {
        if self.remaining_power() < 0 || self.remaining_power() >= 100
        {
            return WorldDiff::NoResult;
        }
        if self.species.health <= 0 {
            return WorldDiff::Die;
        }
        if self.step_index >= self.steps.len()
            && self.current_order.is_some()
        {
            self.step_index = 0;
            self.steps = vec![];
        }
        if self.done_order(ws, stockpiles)
            || self
                .current_order
                .map_or(false, |o| self.finished_list.contains(&o))
        {
            self.finished_job(
                ws, time, dt, frame, orders, stockpiles,
            );
        }
        if let Some((_, ns)) = orders.iter().find(|(o, _)| {
            self.current_order.map_or(false, |c| *o == c)
        }) {
            if self.step_index < self.steps.len()
                && self.current_order.is_some()
            {
                let step =
                    self.steps.get(self.step_index).unwrap().clone();
                let res = self.do_step(&step, ws, stockpiles);
                self.frame_last_action = frame;
                match res {
                    ActionResult::Done(res) => {
                        self.path_index = 0;
                        self.step_index += 1;
                        res
                    }
                    ActionResult::Continue(res) => res,
                    ActionResult::Error => {
                        println!("Error");
                        self.current_order = None;
                        self.steps = vec![];
                        self.step_index = 0;
                        WorldDiff::NoResult
                    }
                }
            } else if self.steps.len() == 0
                && self.current_order.is_some()
            {
                if let Some(res) =
                    self.create_steps(ws, *ns, stockpiles)
                {
                    res
                } else {
                    if self.r#try >= 5 {
                        self.finished_list
                            .push(self.current_order.unwrap());
                        println!("Can't continue, too many tries.");
                        self.r#try = 0;
                    } else {
                        println!("Can't create steps.");
                        self.r#try += 1;
                    }
                    WorldDiff::NoResult
                }
            } else if self.number_of_sharers != *ns {
                self.current_order = None;
                WorldDiff::NoResult
            } else {
                WorldDiff::NoResult
            }
        } else if self.current_order.is_none() {
            for (i, (o, n)) in orders.iter_mut().enumerate() {
                if self.can_do(o)
                    && !self.finished_list.contains(&o.clone())
                    && !self
                        .current_order
                        .map_or(false, |o1| o1 == *o)
                {
                    self.current_order = Some(*o);
                    self.steps = vec![];
                    self.sharers_at_start = *n;
                    self.number_of_sharers = *n + 1;
                    if let Some(res) =
                        self.create_steps(ws, *n + 1, stockpiles)
                    {
                        *n += 1;
                        if self.finished_list.len() > 0 {
                            self.finished_list.remove(0);
                        }
                        return res;
                    } else {
                        self.sharers_at_start = 0;
                        self.number_of_sharers = 0;
                    }
                }
            }

            WorldDiff::NoResult
        } else {
            WorldDiff::NoResult
        }
    }

    fn as_mut_any(&mut self) -> &mut dyn Any {
        self
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn current_pos(&self) -> Point3D {
        self.pos
    }
    fn next_pos(&self) -> Point3D {
        let s = self.steps.last();
        match s {
            Some(IntermediateOrder::FollowPath(path)) => {
                *path.get(self.path_index + 1).unwrap_or(&self.pos)
            }
            _ => self.pos,
        }
    }
    fn last_pos(&self) -> Point3D {
        let s = self.steps.last();
        match s {
            Some(IntermediateOrder::FollowPath(path)) => *path
                .get(self.path_index.checked_sub(1).unwrap_or(0))
                .unwrap_or(&self.pos),
            _ => self.pos,
        }
    }
    fn species(&self) -> &SpeciesProperties {
        &self.species
    }
    fn action_speed(&self) -> usize {
        if self.remaining_power() != 0 {
            ((100.0 - (self.remaining_power() as f64 - 100.0).abs())
                / 5.0) as usize
        } else {
            50
        }
    }
    fn frame_last_action(&self) -> usize {
        self.frame_last_action
    }
    fn current_delta(&self) -> (i32, i32) {
        let np = self.next_pos();
        let dx = self.current_pos().0 as i32 - np.0 as i32;
        let dy = self.current_pos().1 as i32 - np.1 as i32;
        (
            if dx != 0 { dx / dx.abs() } else { 0 },
            if dy != 0 { dy / dy.abs() } else { 0 },
        )
    }
    fn last_delta(&self) -> (i32, i32) {
        (0, 0)
    }

    fn do_order(&mut self, order: Order, orders: &mut Orders) {
        if let Some(ref order) = self.current_order {
            let mut remove = None;
            for (i, (o, n)) in orders.iter_mut().enumerate() {
                if *o == *order {
                    *n = std::cmp::max(0, *n as i32 - 1) as usize;
                    self.finished_list.push(o.clone());
                    if *n < 1 {
                        remove = Some(i);
                    }
                    break;
                }
            }
            if let Some(i) = remove {
                if i < orders.len() {
                    orders.remove(i);
                }
            }
        }
        self.sharers_at_start = 0;
        self.number_of_sharers = 0;
        self.step_index = 0;
        self.steps = vec![];
        self.current_order = Some(order);
    }
}
