use crate::worldgen::terrain::Item;
use crate::worldgen::ItemType;
use std::any::Any;

use crate::utils::math::{Point2D, Point3D, Rect2D3D};
use crate::worldgen::terrain::{Biome, BiomeType, Tile};
use crate::worldgen::{Orders, World};
use std::cell::RefCell;
use std::collections::HashMap;

pub mod drone;

pub type Priority = usize;

pub type HealthLevel = usize;

#[derive(Copy, Clone, PartialEq, Debug)]
/// Just put the name of the animal, no spaces, capitalize every word.
/// Put a comment for behaviour, like the one for Dog.
pub enum Carnivore {
    /// comment Dogs are the rarest animal in the game and will automatically follow the player when it sees one. Extremely loyal.
    Dog,
    /// comment Cats sit on your bed and meow.....
    Cat,
    /// Travel in packs and relentlessly stalk thier prey.
    Wolf,
    /// comment Sharks have a large aggro range and can move extremely fast underwater.
    Shark,
    /// comment Alligators are slow and not easily provoked.
    Alligator,
    /// comment GigaSnails travel in packs and move at an incredible speed when disturbed. If tamed GigaSnails make great pack animals.
    GigaSnail,
    /// comment Reapers blend in with thier surroundings and are a force to be reckoned with.
    Reaper,
    /// comment Scarabs only attack if something has been killed nearby.
    Scarab,
    /// comment Male bears are neutral but female bears are vicious and extremely deadly.
    Bear,
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Herbivore {
    Cow,
    Sheep,
    Hippo,
    Rabbit,
    Armadillo,
    Fish,
    Whale,
    /// comment PLantMan (or plantmen) are shoulder pets that help u attack enemies.
    PlantMan,
    /// comment generic bird is useless except to eat.
    GenericBird,
    /// comment Wooly Wevils are tiny and travel in packs.
    WoolyWevil,
}

/// Possible animal species.
#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Species {
    /// Eat meat
    Carnivore(Carnivore),
    /// Eat herbs
    Herbivore(Herbivore),
    Robot,
}

#[derive(Clone)]
pub struct SpeciesProperties {
    pub speed: usize,
    pub health: i32,
    pub chr: char,
    pub sight: u8,
    pub mood: Mood,
    pub species: Species,
}

impl SpeciesProperties {
    pub fn can_go(&self, biome: Biome) -> bool {
        use self::Carnivore::*;
        use self::Herbivore::*;
        use self::Species::*;

        let b = biome.biome_type;
        match self.species {
            Herbivore(Whale) => b == BiomeType::Water,
            Herbivore(Fish) => b == BiomeType::Water,
            Carnivore(Shark) => b == BiomeType::Water,
            _ => b != BiomeType::Water,
        }
    }
}

impl Species {
    /// This is a function on the species property of an
    /// animal AI. It matches the species of the animal (the
    /// thing its a function on), and returns a special set of
    /// properties. this is used just for intitialization, I
    /// think. After that what this function returns is stored
    /// on the animal itself.
    pub fn properties(&self) -> SpeciesProperties {
        use self::Carnivore::*;
        use self::Herbivore::*;
        use self::Species::*;

        match self {
            // So, the format is:
            // &AnimalKind(AnimalName) => {
            //     SpeciesProperties {
            //         health: a positive whole number,
            //         sight: a positive whole number,
            //         species: self.clone(), // don't change this
            //         mood: Mood::the default mood,
            //         chr: std::char::from_u32(256 + the # of the tile)
            //               .unwrap()
            //     }
            // }
            // - Health is how many hits an animal can take
            // - Sight is how far it can see
            // - Species is a reference to self. So that when these properties are stored in an animal AI, it has a reference to its species, not just what it knows about its species. Not sure why I did this, but I'm sure its useful.
            // - Mood is the default mood.
            // - CHR is the character of the tile (cast from 256 + x) that the drawring system should draw when it finds this species.
            // - Speed is how long the animal takes to move from place to place. Smaller is better.
            &Carnivore(Bear) => SpeciesProperties {
                health: 400,
                speed: 3,
                chr: std::char::from_u32(256 + 77).unwrap(),
                sight: 8,
                species: self.clone(),
                mood: Mood::Angry,
            },
            &Robot => SpeciesProperties {
                speed: 8,
                health: 225,
                chr: std::char::from_u32(320).unwrap(),
                sight: 15,
                mood: Mood::Wary,
                species: self.clone(),
            },
            &Carnivore(Dog) => SpeciesProperties {
                health: 200,
                speed: 3,
                chr: std::char::from_u32(256 + 48).unwrap(),
                sight: 8,
                species: self.clone(),
                mood: Mood::Contented,
            },
            &Carnivore(GigaSnail) => SpeciesProperties {
                health: 250,
                chr: std::char::from_u32(256 + 78).unwrap(),
                speed: 20,
                sight: 2,
                species: self.clone(),
                mood: Mood::Contented,
            },
            &Carnivore(Cat) => SpeciesProperties {
                health: 100,
                speed: 1,
                chr: std::char::from_u32(256 + 50).unwrap(),
                species: self.clone(),
                sight: 10,
                mood: Mood::Discontented,
            },
            &Carnivore(Wolf) => SpeciesProperties {
                health: 400,
                speed: 2,
                chr: std::char::from_u32(256 + 50).unwrap(),
                species: self.clone(),
                sight: 13,
                mood: Mood::Wary,
            },
            &Carnivore(Shark) => SpeciesProperties {
                health: 800,
                chr: 'S',
                speed: 2,
                species: self.clone(),
                sight: 6,
                mood: Mood::Agressive,
            },
            &Carnivore(Alligator) => SpeciesProperties {
                health: 800,
                chr: 'A',
                speed: 3,
                species: self.clone(),
                sight: 6,
                mood: Mood::Agressive,
            },
            &Carnivore(Reaper) => SpeciesProperties {
                health: 2000,
                speed: 1,
                chr: std::char::from_u32(256 + 79).unwrap(),
                species: self.clone(),
                sight: 4,
                mood: Mood::Agressive,
            },
            &Carnivore(Scarab) => SpeciesProperties {
                health: 400,
                chr: std::char::from_u32(256 + 45).unwrap(),
                speed: 4,
                species: self.clone(),
                sight: 13,
                mood: Mood::Agressive,
            },
            &Herbivore(Fish) => SpeciesProperties {
                health: 10,
                species: self.clone(),
                chr: 'f',
                speed: 1,
                sight: 9,
                mood: Mood::Fearful,
            },
            &Herbivore(PlantMan) => SpeciesProperties {
                speed: 8,
                health: 250,
                species: self.clone(),
                chr: std::char::from_u32(256 + 81).unwrap(),
                sight: 9,
                mood: Mood::Fearful,
            },
            &Herbivore(WoolyWevil) => SpeciesProperties {
                health: 500,
                speed: 4,
                species: self.clone(),
                chr: std::char::from_u32(256 + 83).unwrap(),
                sight: 9,
                mood: Mood::Contented,
            },
            &Herbivore(GenericBird) => SpeciesProperties {
                health: 10,
                species: self.clone(),
                speed: 2,
                chr: std::char::from_u32(256 + 80).unwrap(),
                sight: 30,
                mood: Mood::Fearful,
            },
            &Herbivore(Whale) => SpeciesProperties {
                health: 10000,
                chr: std::char::from_u32(256 + 49).unwrap(),
                speed: 5,
                species: self.clone(),
                sight: 5,
                mood: Mood::Contented,
            },

            &Herbivore(Cow) => SpeciesProperties {
                health: 300,
                speed: 7,
                chr: 'c',
                species: self.clone(),
                sight: 5,
                mood: Mood::Contented,
            },
            &Herbivore(Hippo) => SpeciesProperties {
                health: 300,
                speed: 5,
                chr: 'H',
                species: self.clone(),
                sight: 15,
                mood: Mood::Agressive,
            },
            &Herbivore(Sheep) => SpeciesProperties {
                health: 200,
                speed: 8,
                chr: 's',
                species: self.clone(),
                sight: 10,
                mood: Mood::Wary,
            },
            &Herbivore(Rabbit) => SpeciesProperties {
                health: 10,
                speed: 1,
                chr: 'r',
                species: self.clone(),
                sight: 9,
                mood: Mood::Fearful,
            },
            &Herbivore(Armadillo) => SpeciesProperties {
                health: 100,
                speed: 5,
                chr: 'a',
                species: self.clone(),
                sight: 15,
                mood: Mood::Wary,
            },
        }
    }
}

/// The mental mood of a living actor.
#[derive(Debug, Copy, Clone)]
pub enum Mood {
    Angry,
    Fearful,
    Agressive,
    Wary,
    Joyful,
    Happy,
    Contented,
    Discontented,
    Unhappy,
    Depressed,
}

/// Player assigned missions (orders)
#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Clone, Copy, Hash)]
pub enum Order {
    FellTrees(Rect2D3D),

    Attack(Rect2D3D),
    Guard(Rect2D3D),
    StandGround(Rect2D3D),

    Go(Point3D),

    Mine(Rect2D3D),
    Channel(Rect2D3D),

    BuildBlock(Rect2D3D, ItemType),
}

impl Order {
    pub fn order_type(&self) -> OrderTypes {
        use self::Order::*;
        match self {
            &FellTrees(_) => OrderTypes::Forestry,

            &Go(_) => OrderTypes::Movement,

            &Attack(_) | &Guard(_) | &StandGround(_) => {
                OrderTypes::Fighting
            }

            &Mine(_) | &Channel(_) => OrderTypes::Mining,

            &BuildBlock(..) => OrderTypes::Construction,
        }
    }
}

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Clone, Copy)]
pub enum OrderTypes {
    Forestry,
    Movement,
    Fighting,
    Mining,
    Construction,
}

/// Actions that can be performed on an eatable object.
pub trait Eatable {
    fn cook(self) -> Self;
}

/// Actions that can be performed on a drinkable object.
pub trait Drinkable {
    fn filter(self) -> Self;
    fn boild(self) -> Self;
    fn satisfaction(self) -> Self;
}

#[derive(Debug, Clone, PartialEq)]
pub enum WorldDiff {
    NoResult,
    Die,
    Move(Point3D, Point3D),
    Kill(usize),
    ReplaceTile(Point3D, Tile),
    AddToStockpile(Item),
    ReplaceTileStack(Point3D, usize, Vec<Tile>),
    RemoveTile(Point3D),
}

/// Basic missions that animals can assign to themselves
#[derive(Debug, Eq, PartialOrd, Clone)]
pub enum Mission {
    Eat(Priority),
    Drink(Priority),
    AttackEnemy(Priority),
    GoToArea(Rect2D3D, Priority),
    Go(Point2D, Priority),
    Obey(Priority, Order),
    Die,
}

use std::cmp::*;
impl Ord for Mission {
    fn cmp(&self, other: &Self) -> Ordering {
        use self::Mission::*;
        let priority_a = match self {
            &Eat(p)
            | &Drink(p)
            | &AttackEnemy(p)
            | &GoToArea(_, p)
            | &Obey(p, _) => p,
            &Go(_, p) => p,
            &Die => 1000,
        };

        let priority_b = match other {
            &Eat(p)
            | &Drink(p)
            | &AttackEnemy(p)
            | &GoToArea(_, p)
            | &Obey(p, _) => p,
            &Go(_, p) => p,
            &Die => 1000,
        };

        return priority_a.cmp(&priority_b);
    }
}

impl PartialEq for Mission {
    fn eq(&self, other: &Mission) -> bool {
        use self::Mission::*;
        return match (self, other) {
            (&Eat(..), &Eat(..)) => true,
            (&Drink(..), &Drink(..)) => true,
            (&AttackEnemy(..), &AttackEnemy(..)) => true,
            (&GoToArea(..), &GoToArea(..)) => true,
            (&Obey(..), &Obey(..)) => true,
            _ => false,
        };
    }
}

pub trait Living {
    /// Execute the current mission or find a new mission, either through the
    /// order queue or through current needs. Also checks on the current mission
    /// to be executed and changes behaviour to help others also working on that
    /// mission
    fn execute_mission(
        &mut self,
        ws: &World,
        time: usize,
        dt: usize,
        frame: usize,
        orders: &mut Orders,
        stockpiles: &mut HashMap<ItemType, usize>,
    ) -> WorldDiff;

    fn current_pos(&self) -> (usize, usize, usize);
    fn species(&self) -> &SpeciesProperties;
    fn identity(&self) -> usize;
    fn as_mut_any(&mut self) -> &mut dyn Any;
    fn as_any(&self) -> &dyn Any;
    fn next_pos(&self) -> Point3D;
    fn last_pos(&self) -> Point3D;
    fn action_speed(&self) -> usize;
    fn frame_last_action(&self) -> usize;
    fn current_delta(&self) -> (i32, i32);
    fn last_delta(&self) -> (i32, i32);
    fn standing_on(&self) -> Tile;
    fn update_standing_on(
        &mut self,
        t: Tile,
        life: &Vec<Option<RefCell<Box<dyn Living>>>>,
    );
    fn do_order(&mut self, order: Order, orders: &mut Orders);
}
