use crate::utils::math::strict_adjacent;
use crate::worldgen::terrain::Tile;
use crate::worldgen::WorldState;

pub mod liquid;
pub mod stone;

pub trait PhysicsActor {
    fn solid(&self) -> bool;
    fn heavy(&self) -> bool;
}

fn unsupported(
    tile: Tile,
    adj: Vec<Tile>,
    above: Tile,
    below: Tile,
) -> bool {
    let solid_cnt = adj.iter().take_while(|x| x.solid()).count();
    !below.solid()
        && (!above.solid() || !tile.heavy())
        && solid_cnt < 2
}

pub fn run(ws: &mut WorldState, _dt: usize) {
    if let Some(ref world) = ws.map {
        let map = &world.map;
        for y in 0..(world.map_size.1) {
            for x in 0..(world.map_size.0) {
                // TODO: Add or remove snow based on temperature.
                let unit = &map[y][x].tiles.clone();
                let mut ut = unit.write().unwrap();
                for h in 0..ut.len() {
                    let tile = ut[h];

                    let adj = strict_adjacent((x, y))
                        .iter()
                        .map(|pnt| {
                            let unit =
                                get!(world.get(*pnt)).tiles.clone();
                            let tiles = unit.read().unwrap();
                            Some(tiles[h])
                        })
                        .filter(|x| x.is_some())
                        .map(|x| x.unwrap())
                        .collect::<Vec<_>>();
                    if unsupported(
                        tile,
                        adj,
                        *ut.get(h + 1).unwrap_or(&Tile::Empty(None)),
                        *ut.get(h.checked_sub(1).unwrap_or(0))
                            .unwrap_or(&Tile::Empty(None)),
                    ) {
                        let tmp = ut[h];
                        ut[h] = Tile::Empty(None);
                        ut[h - 1] = tmp;
                    }
                }
            }
        }
    }
}
