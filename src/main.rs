#![allow(dead_code, unused_variables)]

/// An easy way to tes  if an expression matches a pattern, returns a boolean.
#[macro_export]
macro_rules! matches {
    ($e:expr, $p:pat) => {
        match $e {
            $p => true,
            _ => false,
        }
    };
}

#[macro_export]
macro_rules! get(
    ($e:expr) => (match $e { Some(e) => e, None => return None })
);

/// This macro facilitates the several-step process of getting a tile out of the
/// unit, refcell, and arc-nested tile setup. This macro has three forms:
/// 1. `(foo : unit = <your unit here>)`: this creates and sets the variable
///    `foo` (not previously defined by you) to the tile array of the given
///    unit.
/// 2. `(foo : unit = <unit>, <z level>)`: this creates the variable `foo` to be
///    equal to the tile in the given unit at the given z level.
/// 3. `(foo : map = <&worldgen::World>, (<x>, <y>), <z>)` sets the variable
///    `foo` to be the tile at those positions in the given `World`.
/// 4. `(foo : map = <&worldgen::World>, (<x>, <y>))` sets the variable `foo` to
///    be the unit at the given positions.
#[macro_export]
macro_rules! get_tile {
    ($i:ident : unit = $u:expr) => {
        let arc = $u.tiles.clone();
        let $i = arc.read().unwrap();
    };
    ($i:ident : unit = $u:expr, $z:expr) => {
        let arc = $u.tiles.clone();
        let tiles = arc.read().unwrap();
        let $i = tiles.get($z);
    };
    ($i:ident : map = $m:expr,($x:expr, $y:expr)) => {
        let unit = $m.get(($x, $y));
        let $i = match unit {
            Some(unit) => {
                let arc = unit.tiles.clone();
                Some(arc.read().unwrap())
            }
            None => None,
        };
    };
    ($i:ident : map = $m:expr,($x:expr, $y:expr), $z:expr) => {
        let unit = $m.get(($x, $y));
        let $i = match unit {
            Some(unit) => {
                let arc = unit.tiles.clone();
                let tiles = arc.read().unwrap();
                tiles.get($z).cloned()
            }
            None => None,
        };
    };
}

extern crate find_folder;
extern crate graphics;
extern crate piston;
extern crate piston_window;
extern crate regex;
extern crate walkdir;

#[macro_use]
extern crate lazy_static;

use crate::life::Living;
use crate::physics::PhysicsActor;
use crate::utils::math::*;
use crate::worldgen::terrain::State;
use graphics::Transformed;
use piston::input::*;
use piston::window::WindowSettings;
use regex::Regex;
use std::collections::HashMap;
use std::path::{Component, Path};

type TextureMap = HashMap<String, Vec<G2dTexture>>;

use piston_window::*;
pub mod ui;
use crate::ui::*;
pub mod utils;

pub mod draw;
pub mod life;
use crate::draw::draw_map;

pub mod physics;

use crate::life::Species;

pub mod worldgen;
use crate::worldgen::terrain::{BiomeType, StoneTypes, Tile};
use crate::worldgen::{GenProps, ItemType, World, WorldState};

mod time;

const SHOW_FONT: &'static str = "assets/master32x32_ro.png";

const HEADING_SIZE: i32 = 20;
const MINI_SIZE: i32 = 10;
const NORMAL_SIZE: i32 = 15;

const SHOW_SIZE: (i32, i32) = (59, 35);
const MAP_SIZE: (usize, usize) = (500, 500);
const WINDOW_SIZE: (i32, i32) = (1920, 1080);

const MOVE_DIST: i32 = 160;

fn tile_desc(t: Tile) -> String {
    match t {
        Tile::Water(clean, State::Liquid, _) => {
            let cs = format!("{:?}", clean).to_lowercase();
            "This is ".to_owned() + &cs + " water. Water is useful \
                                           for powering certain machenery, and providing water to \
                                           your troops."
        }
        Tile::Fire => {
            "This tile represents fire. It \
             spreads to nearby burnable vegetation/wood and \
             melts metal after a certain time period."
                .to_string()
        }
        Tile::Item(i) => {
            let is = format!("{:?}", i).to_lowercase();
            "This tile is a chunk of moveable ".to_owned() + &is
        }
        Tile::Stone(st, _) => {
            match st {
                StoneTypes::Soil(soil, ..) => {
                    let sts = format!("{:?}", soil).to_lowercase();
                    "This is ".to_owned() + &sts + " dirt. It can \
                                                    be dug for useful minerals, and also to form \
                                                    useful land for farm plots."
                }
                _ => {
                    let sts = format!("{:?}", st).to_lowercase();
                    "This is ".to_owned() + &sts + " stone. It can \
                                                    be mined for useful materials, ores, and metals, \
                                                    and also to form rooms for your base."
                }
            }
        }
        Tile::Vegetation(vt, ..) => {
            let vts = format!("{:?}", vt).to_lowercase();
            "This is a ".to_owned()
                + &vts
                + ". It can \
                   be harvested for useful food, natural materials, \
                   and trade goods which can be used to feed and \
                   construct your base."
        }
        _ => "".to_string(),
    }
}

/// The type that represents the UI's finite state machine.
#[derive(Eq, PartialEq, Clone)]
pub enum GameScreen {
    SelectMaterialType,
    Menu,
    WorldChoice,
    SelectArea,
    Game,
    GetSeed,
    Loading,
    Paused,
    JobView,
}

/// These are calculated constant properties of the world that the UI also needs
/// to know about.
pub struct Calc {
    pub max_screen_move: (i32, i32),
    pub highest_world: i32,
    pub screen_size: (i32, i32),
}

/// This is only used once, for selection, and represents a rectangle with two
/// optional corner points.
pub type ORect2D = (Option<(usize, usize)>, Option<(usize, usize)>);

/// This represents the state of the user interface and graphics, as well as
/// system time (not to be confused with game time) and contains the rest of the
/// game's states as well.
pub struct Game {
    /// Holds the UI cards associated with each drone the player controls. This
    /// can't be computed at draw time because we also need to interact with
    /// them.
    drone_cards: Vec<UIView>,
    /// The tile that the user right clicked on.
    inspected_tile: Option<(Point3D, Tile)>,
    /// The drone that the jump to drone command is currently centered on
    drone_focus: usize,
    /// The world properties used to do worldgen
    props: GenProps,
    /// Holds the textures as a list of frames of that animation. If there's no
    /// animation its just a list of one frame.
    texture_map: TextureMap,
    /// Useless
    show_hud: bool,
    constants: Calc,
    /// The time since the last update, in nanoseconds
    last_time: usize,
    /// The current in-progress user selection
    selection: ORect2D,
    /// This controls whether the command pallete is open or not
    select_command: bool,
    /// This is the world's seed
    seed: u32,
    /// The font glyphs for graphics display
    glyphs: Option<Glyphs>,
    gui_scale: f64,
    map_scale: f64,
    main_menu: ui::ButtonGroup,
    world_defaults: ui::ButtonGroup,
    game_tabs: ui::ButtonGroup,
    game_functions: ui::ButtonGroup,
    material_types: ui::ButtonGroup,
    pause_menu: ui::ButtonGroup,
    command_selector: ui::ButtonGroup,
    back_button: ui::Button,
    /// Current system time
    pub time: usize,
    /// Game screen state machine
    pub screen: GameScreen,
    /// Worldgenned state
    pub world_state: WorldState,
}

pub fn offset_pnt(screen: (i32, i32)) -> impl Fn(Point2D) -> Point2D {
    move |p| (screen.0 as usize + p.0, screen.1 as usize + p.1)
}

impl Game {
    /// Calculates the position associated with an order that falls within a
    /// given rectangle. This can be different for different orders: for
    /// instance, Mining talks about the current level that the user actually
    /// selected and no other, while Felling Trees works on the first ground
    /// level under the selection the user made, so they could select higher up
    /// or lower down and still be able to cut trees correctly.
    pub fn position_order(
        &self,
        order: &life::Order,
        npoint1: Point2D,
        npoint2: Point2D,
    ) -> life::Order {
        use crate::life::Order::*;
        match order {
            &FellTrees(x) => {
                if let Some(ref world) = self.world_state.map {
                    let new_point = (
                        npoint1,
                        npoint2,
                        std::cmp::min(
                            self.world_state.level as usize,
                            utils::misc::location_z(
                                &world.map, npoint1,
                            ),
                        ),
                    );
                    FellTrees(new_point)
                } else {
                    FellTrees(x)
                }
            }

            &Mine(x) => {
                if let Some(ref world) = self.world_state.map {
                    let new_point = (
                        npoint1,
                        npoint2,
                        std::cmp::min(
                            self.world_state.level as usize,
                            utils::misc::location_z(
                                &world.map, npoint1,
                            ),
                        ),
                    );
                    Mine(new_point)
                } else {
                    Mine(x)
                }
            }
            &Channel(x) => {
                if let Some(ref world) = self.world_state.map {
                    Channel((
                        npoint1,
                        npoint2,
                        std::cmp::min(
                            self.world_state.level as usize,
                            utils::misc::location_z(
                                &world.map, npoint1,
                            ),
                        ) - 1,
                    ))
                } else {
                    Channel(x)
                }
            }

            &BuildBlock(x, m) => {
                if let Some(ref world) = self.world_state.map {
                    BuildBlock(
                        (
                            npoint1,
                            npoint2,
                            std::cmp::min(
                                self.world_state.level as usize,
                                utils::misc::location_z(
                                    &world.map, npoint1,
                                ),
                            ),
                        ),
                        m,
                    )
                } else {
                    BuildBlock(x, m)
                }
            }
            x => *x,
        }
    }

    /// These are default world generation properties that are known to work.
    pub fn default_props() -> GenProps {
        GenProps {
            mountainousness: 2.90,
            hilliness: 600,
            raininess: 0.3,
            sea_level: 10.0,
            temp_bias: 0.0,
            biomes: vec![
                (BiomeType::Pasture, 20),
                (BiomeType::Forest, 40),
                (BiomeType::Pasture, 30),
            ],
        }
    }
    /// This initializes a new Game state, but it does not initialize a world
    /// yet since that's managed by the users interaction with the UI
    pub fn new(screen_size: (i32, i32)) -> Game {
        Game {
            select_command: false,
            gui_scale: 1.0,
            map_scale: 1.0,
            drone_cards: vec![],
            inspected_tile: None,
            drone_focus: 0,
            props: Self::default_props(),
            texture_map: HashMap::new(),
            show_hud: true,
            constants: Calc {
                max_screen_move: (-1, -1),
                highest_world: 0,
                screen_size: screen_size,
            },
            glyphs: None,
            selection: (None, None),
            last_time: 0,
            time: 0,
            screen: GameScreen::Menu,
            world_state: WorldState::new(),
            seed: 0,
            main_menu: ui::ButtonGroup::new(
                [
                    screen_size.0 as f64 * 32.0 / 5.0,
                    screen_size.1 as f64 * 32.0 * (2.0 / 3.0),
                    300.0,
                    150.0,
                ],
                vec!["Start Game", "Exit", "From Seed"],
                ui::Orientation::Vertical,
                vec![[0.1, 0.1, 0.1, 0.75]],
            ),
            back_button: ui::Button::new(
                [
                    screen_size.0 as f64 * 32.0 - 155.0,
                    screen_size.1 as f64 * 32.0 - 55.0,
                    150.0,
                    55.0,
                ],
                "Back".to_string(),
                [0.0, 0.0, 0.0, 0.75],
                false,
            ),
            command_selector: ui::ButtonGroup::new(
                [
                    screen_size.0 as f64 * 32.0 / 2.0 - 210.0,
                    screen_size.1 as f64 * 32.0 / 2.0 - 200.0,
                    420.0,
                    600.0,
                ],
                vec![
                    "FELL TREES",
                    "MINE",
                    "CHANNEL",
                    "BUILD WALL",
                    "CANCEL",
                ],
                ui::Orientation::Vertical,
                vec![[0.0, 0.0, 0.0, 0.75]],
            ),
            pause_menu: ui::ButtonGroup::new(
                [
                    screen_size.0 as f64 * 32.0 / 2.0 - 750.0,
                    screen_size.1 as f64 * 32.0 / 2.0 - 198.0,
                    1500.0,
                    300.0,
                ],
                vec!["Resume", "Exit"],
                ui::Orientation::Horizontal,
                vec![[0.0, 0.0, 0.0, 0.75], [1.0, 0.0, 0.0, 0.75]],
            ),
            world_defaults: ui::ButtonGroup::new(
                [
                    screen_size.0 as f64 * 32.0 / 2.0 - 150.0,
                    screen_size.1 as f64 * 32.0 / 2.0 - 100.0,
                    300.0,
                    200.0,
                ],
                vec!["Desert", "Humid", "Island", "Forest", "Cold"],
                ui::Orientation::Vertical,
                vec![[0.1, 0.1, 0.1, 0.75]],
            ),
            game_tabs: ui::ButtonGroup::new(
                [
                    screen_size.0 as f64 * 15.0 - 300.0,
                    5.0,
                    600.0,
                    30.0,
                ],
                vec!["Drones", "Orders"],
                ui::Orientation::Horizontal,
                vec![[0.0, 0.0, 0.0, 0.75]],
            ),
            game_functions: ui::ButtonGroup::new(
                [
                    screen_size.0 as f64 * 32.0 - 193.0,
                    35.0,
                    387.0,
                    166.0,
                ],
                vec!["Jump to Drones", "Jump to Sea Level", "Pause"],
                ui::Orientation::Vertical,
                vec![[0.0, 0.0, 0.0, 0.75]],
            ),
            material_types: ui::ButtonGroup::new(
                [
                    screen_size.0 as f64 * 32.0 / 2.0 - 150.0,
                    screen_size.1 as f64 * 32.0 / 2.0 - 100.0,
                    300.0,
                    200.0,
                ],
                vec!["Wood", "Stone", "Metal"],
                ui::Orientation::Vertical,
                vec![[0.0, 0.0, 0.0, 0.75]],
            ),
        }
    }

    /// Inintializes the game and prepares the UI state for actual gameplay.
    pub fn init_game(&mut self, seed: Option<u32>, props: &GenProps) {
        self.last_time = time::get_millisecond_time() as usize;
        self.seed = seed.unwrap_or(self.last_time as u32);
        let world = World::new(MAP_SIZE, self.seed, props);
        self.world_state.add_map(world);
        self.props = (*props).clone();
        self.center_on_drones();
        self.constants.max_screen_move = (6400, 3200);
        self.constants.highest_world =
            self.world_state.highest_level as i32 - 1;
        self.screen = GameScreen::Game;
    }

    pub fn draw(
        &mut self,
        e: &Event,
        args: &RenderArgs,
        window: &mut PistonWindow,
    ) {
        match self.screen {
            GameScreen::SelectMaterialType => {
                window.draw_2d(e, |mut c, g| {
                    c.transform = c.transform.scale(
                        1.0 / self.gui_scale,
                        1.0 / self.gui_scale,
                    );
                    clear([0.007, 0.011, 0.0156, 1.0], g);
                    image(
                        &self.texture_map.get("space").unwrap()[0],
                        c.transform.trans(100.0, 0.0),
                        g,
                    );
                    if let Some(ref mut glyphs) = self.glyphs {
                        self.material_types
                            .draw(e, &c, g, args, glyphs);
                    }
                });
            }
            GameScreen::WorldChoice => {
                window.draw_2d(e, |mut c, g| {
                    c.transform = c.transform.scale(
                        1.0 / self.gui_scale,
                        1.0 / self.gui_scale,
                    );
                    clear([0.007, 0.011, 0.0156, 1.0], g);
                    image(
                        &self.texture_map.get("space").unwrap()[0],
                        c.transform.trans(100.0, 0.0).zoom(self.gui_scale),
                        g,
                    );
                    if let Some(ref mut glyphs) = self.glyphs {
                        self.world_defaults
                            .draw(e, &c, g, args, glyphs);
                    }
                });
            }
            GameScreen::SelectArea => {
                self.draw_area_screen(window, e, args)
            }
            GameScreen::JobView => {
                self.draw_jobview_screen(window, e, args)
            }
            GameScreen::GetSeed => {}
            GameScreen::Loading => {}
            GameScreen::Menu => {
                self.draw_menu_screen(window, e, args)
            }
            GameScreen::Game => {
                self.draw_game_screen(window, e, args)
            }
            GameScreen::Paused => {
                window.draw_2d(e, |mut c, g| {
                    c.transform = c.transform.scale(
                        1.0 / self.gui_scale,
                        1.0 / self.gui_scale,
                    );
                    clear([0.007, 0.011, 0.0156, 1.0], g);
                    if let Some(ref mut glyphs) = self.glyphs {
                        image(
                            &self.texture_map.get("space").unwrap()
                                [0],
                            c.transform.trans(100.0, 0.0),
                            g,
                        );
                        self.pause_menu.draw(e, &c, g, args, glyphs);
                    }
                });
            }
        }
    }

    fn draw_items(
        &mut self,
        ws: piston_window::Size,
        e: &Event,
        args: &RenderArgs,
        c: &graphics::Context,
        g: &mut G2d,
    ) {
        let mut typs = self
            .world_state
            .stockpiles
            .iter()
            .map(|(item_type, amount)| {
                let image_name = draw::texture_name(
                    item_type.to_item_tile(),
                    0,
                    0,
                    0,
                );
                let texture =
                    self.texture_map.get(&image_name).unwrap()[0]
                        .clone();
                ui::UITypes::ImageTextGroup(
                    texture,
                    format!("x{:?}", amount),
                    MINI_SIZE,
                    0.5,
                )
            })
            .collect::<Vec<_>>();
        typs.insert(
            0,
            ui::UITypes::Text("INVENTORY".to_string(), HEADING_SIZE),
        );

        if let Some(ref mut glyphs) = self.glyphs {
            ui::UIView::new(
                [5.0, 35.0, ws.width as f64 / 8.0, 210.0],
                typs,
            )
            .draw(e, c, g, args, glyphs);
        }
    }

    fn draw_inspector(
        &mut self,
        t: (Point3D, Tile),
        ws: piston_window::Size,
        e: &Event,
        args: &RenderArgs,
        c: &graphics::Context,
        g: &mut G2d,
    ) {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"\((.*)\)").unwrap();
        }
        if let Some(ref mut glyphs) = self.glyphs {
            if !matches!(t.1, Tile::Empty(_)) {
                let v = draw::texture_name(t.1, 0, 0, 0);
                let g2 = self.texture_map.get(&v).unwrap()[0].clone();
                let scale = 400.0 / (g2.get_height() as f64 * 2.0);
                ui::UIView::new(
                    [
                        ws.width as f64 - 405.0,
                        ws.height as f64 - 445.0,
                        400.0,
                        440.0,
                    ],
                    vec![
                        ui::UITypes::Image(g2, scale),
                        ui::UITypes::Text(
                            RE.captures(&format!("{:?}", t.1))
                                .unwrap()
                                .get(0)
                                .unwrap()
                                .as_str()
                                .replace("(", "")
                                .replace(")", "")
                                .to_uppercase(),
                            HEADING_SIZE,
                        ),
                        ui::UITypes::Text(
                            format!(
                                "{:?} {}",
                                t.0,
                                if t.1.solid() {
                                    "(Walkable)"
                                } else {
                                    ""
                                }
                            ),
                            MINI_SIZE,
                        ),
                        ui::UITypes::Text(
                            tile_desc(t.1),
                            NORMAL_SIZE,
                        ),
                    ],
                )
                .draw(e, &c, g, args, glyphs);
            } else if let Tile::Empty(Some(id)) = t.1 {
                if let Some(ref map) = self.world_state.map {
                    if let Some(ref life) = map.life[id] {
                        let life = life.borrow();
                        let s = life.species().species;
                        let g2 = self
                            .texture_map
                            .get(&draw::life_texture(s, &life)[1])
                            .unwrap()[0]
                            .clone();
                        let scale =
                            400.0 / (g2.get_height() as f64 * 2.0);

                        ui::UIView::new(
                                    [
                                        ws.width as f64 - 405.0,
                                        ws.height as f64 - 405.0,
                                        400.0,
                                        400.0,
                                    ],
                                    vec![
                                        ui::UITypes::Image(g2, scale),
                                        ui::UITypes::Text(
                                            format!(
                                                "{:?}",
                                                life.species()
                                                    .species
                                            ).replace("(", ", ")
                                                .replace(")", "")
                                                .to_uppercase(),
                                            HEADING_SIZE,
                                        ),
                                        ui::UITypes::Text(
                                            format!("{:?}", t.0),
                                            MINI_SIZE,
                                        ),
                                        ui::UITypes::Text(
                                            if s == Species::Robot {
                                                let r: &life::drone::Drone = match life
                                                .as_any()
                                                .downcast_ref::<life::drone::Drone>()
                                            {
                                                Some(b) => b,
                                                None => unreachable!(),
                                            };
                                                r.addons
                                                    .iter()
                                                    .map(|x| {
                                                        format!(
                                                            "{:?}",
                                                            x
                                                        )
                                                    })
                                                    .collect::<Vec<_>>(
                                                    )
                                                    .join("\n")
                                            } else {
                                                "".to_string()
                                            },
                                            NORMAL_SIZE,
                                        ),
                                    ],
                                ).draw(e, &c, g, args, glyphs);
                    }
                }
            }
        }
    }

    fn draw_commands(
        &mut self,
        ws: piston_window::Size,
        e: &Event,
        args: &RenderArgs,
        c: &graphics::Context,
        g: &mut G2d,
    ) {
        if let Some(ref mut glyphs) = self.glyphs {
            let mut transform = c.transform.trans(5.0, 250.0);
            let line_height = NORMAL_SIZE as f64 / 0.5;
            let height = 40.0
                + self.world_state.commands.len() as f64
                    * line_height
                + 40.0
                + HEADING_SIZE as f64 / 0.5;
            graphics::Polygon::new([0.0, 0.0, 0.0, 0.75]).draw(
                &[
                    [20.0, 0.0],
                    [395.0, 0.0],
                    [395.0, height],
                    [0.0, height],
                    [0.0, 20.0],
                ],
                &c.draw_state,
                transform,
                g,
            );
            transform = transform.trans(40.0, 40.0);
            text::Text::new_color(
                [1.0, 1.0, 1.0, 1.0],
                HEADING_SIZE as u32,
            )
            .draw("QUEUED JOBS", glyphs, &c.draw_state, transform, g)
            .unwrap();
            for (i, order) in
                self.world_state.commands.iter().enumerate()
            {
                let o = format!("{:?}", order)
                    .replace("(", " ")
                    .replace(")", "");
                text::Text::new_color(
                    [1.0, 1.0, 1.0, 1.0],
                    NORMAL_SIZE as u32,
                )
                .draw(
                    &o.to_uppercase(),
                    glyphs,
                    &c.draw_state,
                    transform.trans(
                        0.0,
                        (HEADING_SIZE as f64 / 0.5)
                            + i as f64 * line_height,
                    ),
                    g,
                )
                .unwrap();
            }
        }
    }

    fn draw_game_map(
        &mut self,
        ws: piston_window::Size,
        e: &Event,
        args: &RenderArgs,
        c: &mut graphics::Context,
        g: &mut G2d,
    ) {
        let props = self.props.clone();
        if let Some(ref mut glyphs) = self.glyphs {
            draw_map(
                e,
                args,
                &props,
                &self.world_state,
                self.selection,
                c,
                g,
                (
                    (ws.width as f64 * self.map_scale) as u32,
                    (ws.height as f64 * self.map_scale) as u32,
                ),
                self.map_scale,
                &self.texture_map,
                glyphs,
                self.last_time,
            );
        }
    }

    fn draw_area_screen(
        &mut self,
        window: &mut PistonWindow,
        e: &Event,
        args: &RenderArgs,
    ) {
        let sz = window.size();
        window.draw_2d(e, move |mut c, g| {
            clear([0.007, 0.011, 0.0156, 1.0], g);
            image(
                &self.texture_map.get("stars").unwrap()[0],
                c.transform,
                g,
            );
            let mut clone = c.clone();
            clone.transform = c
                .transform
                .scale(1.0 / self.map_scale, 1.0 / self.map_scale);
            self.draw_game_map(sz, e, args, &mut clone, g);

            // apply scale
            c.transform = c
                .transform
                .scale(1.0 / self.gui_scale, 1.0 / self.gui_scale);

            let (p1, p2) = self.selection;
            if let Some(p1) = p1 {
                utils::misc::display_terrain(
                    g,
                    &c,
                    args,
                    (p1.0, p1.1, 0),
                    &self.texture_map.get("cursor").unwrap()[0],
                );
            }
            if let Some(p2) = p2 {
                utils::misc::display_terrain(
                    g,
                    &c,
                    args,
                    (p2.0, p2.1, 0),
                    &self.texture_map.get("cursor").unwrap()[0],
                );
            }
        });
    }

    fn draw_select_screen(
        &mut self,
        window: &mut PistonWindow,
        e: &Event,
        args: &RenderArgs,
    ) {
        let sz = window.size();
        let props = self.props.clone();
        window.draw_2d(e, move |mut c, g| {
            clear([0.007, 0.011, 0.0156, 1.0], g);
            image(
                &self.texture_map.get("stars").unwrap()[0],
                c.transform,
                g,
            );
            let mut clone = c.clone();
            clone.transform = c
                .transform
                .scale(1.0 / self.map_scale, 1.0 / self.map_scale);
            self.draw_game_map(sz, e, args, &mut clone, g);

            // apply scale
            c.transform = c
                .transform
                .scale(1.0 / self.gui_scale, 1.0 / self.gui_scale);

            let (p1, p2) = self.selection;
            if let Some(p1) = p1 {
                utils::misc::display_terrain(
                    g,
                    &c,
                    args,
                    (p1.0, p1.1, 0),
                    &self.texture_map.get("cursor").unwrap()[0],
                );
            }
            if let Some(p2) = p2 {
                utils::misc::display_terrain(
                    g,
                    &c,
                    args,
                    (p2.0, p2.1, 0),
                    &self.texture_map.get("cursor").unwrap()[0],
                );
            }
        });
    }

    fn draw_jobview_screen(
        &mut self,
        window: &mut PistonWindow,
        e: &Event,
        args: &RenderArgs,
    ) {
        let sz = window.size();
        let props = self.props.clone();
        window.draw_2d(e, |mut c, g| {
            c.transform = c
                .transform
                .scale(1.0 / self.gui_scale, 1.0 / self.gui_scale);
            clear([0.007, 0.011, 0.0156, 1.0], g);
            let t = c.transform.clone();
            let screen = self.world_state.screen;
            self.world_state.screen = (0, 0);
            self.draw_game_map(sz, e, args, &mut c, g);
            if let Some(ref mut glyphs) = self.glyphs {
                self.back_button.draw(e, &c, g, args, glyphs);
                self.world_state.screen = (0, 0);
                c.transform = t.trans(
                    0.0,
                    -std::cmp::max(0, self.world_state.screen.1)
                        as f64,
                );
                for card in self.drone_cards.iter() {
                    card.draw(e, &c, g, args, glyphs);
                }
            }
        });
    }

    fn draw_menu_screen(
        &mut self,
        window: &mut PistonWindow,
        e: &Event,
        args: &RenderArgs,
    ) {
        window.draw_2d(e, |mut c, g| {
            c.transform = c
                .transform
                .scale(1.0 / self.gui_scale, 1.0 / self.gui_scale);
            clear([0.007, 0.011, 0.0156, 1.0], g);
            if let Some(ref mut glyphs) = self.glyphs {
                image(
                    &utils::misc::texture_get(
                        &self.texture_map,
                        "Splash",
                        0,
                        0.82,
                    ),
                    c.transform.trans(0.0, 0.0).zoom(self.gui_scale * 7.2),
                    g,
                );
                self.main_menu.draw(e, &c, g, args, glyphs);
            }
        });
    }

    fn draw_game_screen(
        &mut self,
        window: &mut PistonWindow,
        e: &Event,
        args: &RenderArgs,
    ) {
        let ws = window.size();
        let props = self.props.clone();
        window.draw_2d(e, move |mut c, g| {
            // draw background
            clear([0.007, 0.011, 0.0156, 1.0], g);
            image(
                &self.texture_map.get("stars").unwrap()[0],
                c.transform.zoom(self.gui_scale),
                g,
            );

            let mut clone = c.clone();
            clone.transform = c
                .transform
                .scale(1.0 / self.map_scale, 1.0 / self.map_scale);
            self.draw_game_map(ws, e, args, &mut clone, g);

            // apply scale
            c.transform = c
                .transform
                .scale(1.0 / self.gui_scale, 1.0 / self.gui_scale);

            if let Some(ref mut glyphs) = self.glyphs {
                // draw HUD UI
                self.game_tabs.draw(e, &c, g, args, glyphs);
                self.game_functions.draw(e, &c, g, args, glyphs);

                if self.select_command {
                    c.transform = c.transform.trans(
                        0.0,
                        std::cmp::min(-self.world_state.screen.1, 0)
                            as f64,
                    );
                    self.command_selector
                        .draw(e, &c, g, args, glyphs);
                }
            }
            // Draw popup windows...

            if let Some(t) = self.inspected_tile {
                self.draw_inspector(t, ws, e, args, &c, g);
            }
            if self.world_state.stockpiles.len() > 0 {
                self.draw_items(ws, e, args, &c, g);
            }
            if self.world_state.commands.len() > 0 {
                self.draw_commands(ws, e, args, &c, g);
            }
        });
    }

    /// This should be called before any attempt to change the UI screen to view the
    /// drones, because this updates the drone-related UIView info.
    fn setup_drone_cards(&mut self) {
        self.drone_cards = vec![];
        if let Some(ref map) = self.world_state.map {
            let mut i = 0;
            for life in map.life.iter() {
                if let Some(life) = life {
                    let life = life.borrow();
                    let s = life.species().species;
                    if s == Species::Robot {
                        let l: &life::drone::Drone = match life
                            .as_any()
                            .downcast_ref::<life::drone::Drone>()
                        {
                            Some(b) => b,
                            None => unreachable!(),
                        };
                        let g2 = self.texture_map.get("fl").unwrap()
                            [0]
                        .clone();
                        let scale =
                            300.0 / (g2.get_height() as f64 * 2.0);
                        let mut ui_peices = vec![
                            ui::UITypes::Image(g2, scale),
                            ui::UITypes::Text(
                                format!("{}", l.name).to_uppercase(),
                                20,
                            ),
                            ui::UITypes::Text(
                                format!("{:?}", l.current_pos()),
                                10,
                            ),
                            ui::UITypes::Text(
                                format!(
                                    "{:?}     R.P.: {}",
                                    l.current_order,
                                    (0..(l.remaining_power() / 10))
                                        .map(|_| '/')
                                        .collect::<String>()
                                ),
                                15,
                            ),
                        ];
                        for addon in l.addons.iter() {
                            let t =
                                &self
                                    .texture_map
                                    .get(&addon.get_graphic(
                                        l.current_delta(),
                                    ))
                                    .unwrap_or(
                                        self.texture_map
                                            .get("empty")
                                            .unwrap(),
                                    )[0];
                            ui_peices.push(UITypes::ImageTextGroup(
                                t.clone(),
                                format!(
                                    "{:?}\nP.D.: {}\nP.B.: {}",
                                    addon,
                                    (0..(addon
                                        .get_stats()
                                        .power_draw
                                        / 10))
                                        .map(|_| '|')
                                        .collect::<String>(),
                                    (0..(addon
                                        .get_stats()
                                        .power_boost
                                        / 10))
                                        .map(|_| '/')
                                        .collect::<String>()
                                ),
                                15,
                                1.0,
                            ));
                        }
                        self.drone_cards.push(ui::UIView::new(
                            [
                                (i % 4) as f64 * 480.0 + 40.0,
                                (i / 4) as f64 * 740.0 + 40.0,
                                400.0,
                                700.0,
                            ],
                            ui_peices,
                        ));
                        i += 1;
                    }
                }
            }
        }
    }

    /// Updates the:
    /// 1. Current time and frame time
    /// 2. worldstate
    fn update(&mut self, uargs: &UpdateArgs) {
        let current_time = time::get_millisecond_time() as usize;
        self.world_state.update(
            current_time,
            current_time - self.last_time,
            self.screen.clone(),
        );
        self.last_time = current_time;
    }

    /// Moves the world screen position by a certain distance (`xdelta` and
    /// `ydelta`).
    fn move_delta(&mut self, xdelta: i32, ydelta: i32) {
        self.world_state.screen = (
            self.world_state.screen.0 + xdelta,
            self.world_state.screen.1 + ydelta,
        );
    }

    /// Handles mouse clicks, and movement, including UI interaction, and map
    /// interaction. This behaves differently based on which state `self.screen`
    /// is in, because there are different UI elements depending on
    /// `self.screen`.
    fn handle_mouse(&mut self, mouse: Option<&MouseButton>) {
        match self.screen {
            GameScreen::GetSeed => {}
            GameScreen::Loading => {}
            GameScreen::SelectArea => {
                self.handle_select_area_mouse(mouse)
            }
            GameScreen::JobView => {
                let (cx, cy) = self.world_state.cursor;
                let res = self
                    .back_button
                    .is_intersecting((cx as f64, cy as f64));
                if let Some(val) = res {
                    if let Some(MouseButton::Left) = mouse {
                        self.screen = GameScreen::Game;
                        self.center_on_drones();
                    }
                }
            }
            GameScreen::WorldChoice => {
                self.handle_world_choice_mouse(mouse)
            }
            GameScreen::Menu => self.handle_menu_mouse(mouse),
            GameScreen::Game => self.handle_game_mouse(mouse),
            GameScreen::Paused => self.handle_pause_mouse(mouse),
            GameScreen::SelectMaterialType => {
                let res = self.material_types.is_intersecting((
                    self.world_state.cursor.0 as f64,
                    self.world_state.cursor.1 as f64,
                ));
                if let Some(val) = res {
                    if let Some(MouseButton::Left) = mouse {
                        let mt = match val.as_str() {
                            "Wood" => ItemType::Wood,
                            "Stone" => ItemType::Stone,
                            "Metal" => ItemType::Metal,
                            x => {
                                panic!("Impossible building type option: {:?}", x);
                            }
                        };
                        self.world_state.commands.push((
                            life::Order::BuildBlock(
                                ((0, 0), (0, 0), 0),
                                mt,
                            ),
                            0,
                        ));
                        self.screen = GameScreen::SelectArea;
                    }
                }
            }
        }
    }

    fn handle_select_area_mouse(
        &mut self,
        mouse: Option<&MouseButton>,
    ) {
        let (point1, point2) = self.selection;

        if let Some(MouseButton::Left) = mouse {
            let offset = self.world_state.screen;
            let (cx, cy) = utils::math::iso_to_2d((
                self.world_state.cursor.0 - offset.0 as i32,
                self.world_state.cursor.1 - offset.1 as i32,
            ));
            let (cx, cy) = utils::math::rot_switch(
                (cx, cy),
                self.world_state.xswitch,
                self.world_state.yswitch,
                MAP_SIZE,
            );
            if self.world_state.commands.len() > 0 {
                let order = {
                    let popped_order =
                        self.world_state.commands.pop();
                    popped_order.unwrap()
                };
                let (order, count) = order;
                use crate::life::Order::*;
                match order {
                    Go(_) => {}
                    order => {
                        if point1.is_none() && point2.is_none() {
                            self.selection = (Some((cx, cy)), None);
                            self.world_state
                                .commands
                                .push((order, 0));
                        } else if point1.is_some() && point2.is_none()
                        {
                            self.selection = (point1, Some((cx, cy)));
                            self.world_state
                                .commands
                                .push((order, 0));
                        } else if point1.is_some() && point2.is_some()
                        {
                            let no = self.position_order(
                                &order,
                                point1.unwrap(),
                                point2.unwrap(),
                            );
                            if let Some(ref map) =
                                self.world_state.map
                            {
                                if let Some(t) = self.inspected_tile {
                                    if let Tile::Empty(Some(i)) = t.1
                                    {
                                        if let Some(ref a) =
                                            map.life[i]
                                        {
                                            let mut actor =
                                                a.borrow_mut();
                                            actor.do_order(
                                                no,
                                                &mut self
                                                    .world_state
                                                    .commands,
                                            );
                                        } else {
                                            self.world_state
                                                .commands
                                                .push((no, 0));
                                        }
                                    } else {
                                        self.world_state
                                            .commands
                                            .push((no, 0));
                                    }
                                } else {
                                    self.world_state
                                        .commands
                                        .push((no, 0));
                                }
                            }
                            self.selection = (None, None);
                            self.screen = GameScreen::Game;
                        }
                    }
                }
            } else {
                println!("No commands");
            }
        } else if let Some(MouseButton::Right) = mouse {
            self.selection = (None, None);
            self.world_state.commands.pop();
            self.screen = GameScreen::Game;
        }
    }

    fn handle_world_choice_mouse(
        &mut self,
        mouse: Option<&MouseButton>,
    ) {
        let res = self.world_defaults.is_intersecting((
            self.world_state.cursor.0 as f64,
            self.world_state.cursor.1 as f64,
        ));
        if let Some(val) = res {
            if let Some(MouseButton::Left) = mouse {
                self.init_game(
                    None,
                    &match val.as_str() {
                        "Desert" => GenProps {
                            mountainousness: 0.2,
                            hilliness: 10,
                            raininess: 0.5,
                            sea_level: 0.0,
                            temp_bias: 105.0,
                            biomes: vec![(BiomeType::Pasture, 100)],
                        },
                        "Humid" => GenProps {
                            mountainousness: 2.95,
                            hilliness: 650,
                            raininess: 0.1,
                            sea_level: 1.0,
                            temp_bias: 11.0,
                            biomes: vec![(BiomeType::Pasture, 20)],
                        },
                        "Island" => GenProps {
                            mountainousness: 1.4,
                            hilliness: 640,
                            raininess: 1.3,
                            sea_level: 3.0,
                            temp_bias: 10.0,
                            biomes: vec![
                                (BiomeType::Pasture, 40),
                                (BiomeType::Forest, 50),
                            ],
                        },
                        "Forest" => GenProps {
                            mountainousness: 2.20,
                            hilliness: 500,
                            raininess: 0.2,
                            sea_level: 1.0,
                            temp_bias: -5.0,
                            biomes: vec![
                                (BiomeType::Forest, 90),
                                (BiomeType::Pasture, 30),
                            ],
                        },
                        "Cold" => GenProps {
                            mountainousness: 7.20,
                            hilliness: 300,
                            raininess: 0.0,
                            sea_level: 1.0,
                            temp_bias: -105.0,
                            biomes: vec![
                                (BiomeType::Forest, 70),
                                (BiomeType::Pasture, 30),
                            ],
                        },
                        _ => unreachable!(),
                    },
                );
            }
        }
    }

    fn handle_menu_mouse(&mut self, mouse: Option<&MouseButton>) {
        let res = self.main_menu.is_intersecting((
            self.world_state.cursor.0 as f64,
            self.world_state.cursor.1 as f64,
        ));
        if let Some(val) = res {
            if let Some(MouseButton::Left) = mouse {
                match val.as_str() {
                    "Start Game" => {
                        self.screen = GameScreen::WorldChoice;
                    }
                    "From Seed" => {
                        self.screen = GameScreen::GetSeed;
                    }
                    "Exit" => std::process::exit(0),
                    _ => {}
                }
            }
        }
    }

    fn handle_pause_mouse(&mut self, mouse: Option<&MouseButton>) {
        let res = self.pause_menu.is_intersecting((
            self.world_state.cursor.0 as f64,
            self.world_state.cursor.1 as f64,
        ));
        if let Some(val) = res {
            if let Some(MouseButton::Left) = mouse {
                match val.as_str() {
                    "Resume" => {
                        self.center_on_drones();
                        self.screen = GameScreen::Game;
                    }
                    "Exit" => {
                        std::process::exit(0);
                    }
                    _ => {}
                }
                return;
            }
        }
    }

    fn handle_game_mouse(&mut self, mouse: Option<&MouseButton>) {
        let res = self.game_tabs.is_intersecting((
            self.world_state.cursor.0 as f64,
            self.world_state.cursor.1 as f64,
        ));
        let res2 = self.game_functions.is_intersecting((
            self.world_state.cursor.0 as f64,
            self.world_state.cursor.1 as f64,
        ));

        let res3 = self.command_selector.is_intersecting((
            self.world_state.cursor.0 as f64,
            self.world_state.cursor.1 as f64,
        ));
        if let Some(MouseButton::Left) = mouse {
            if let Some(val) = res2 {
                match val.as_str() {
                    "Jump to Drones" => {
                        if let Some(ref map) = self.world_state.map {
                            let s2 = life::Species::Robot;
                            let ccount = map
                                .life
                                .iter()
                                .filter(|l| {
                                    if let Some(l) = l {
                                        let s = l
                                            .borrow()
                                            .species()
                                            .species;
                                        s == s2
                                    } else {
                                        false
                                    }
                                })
                                .count();
                            self.drone_focus =
                                (self.drone_focus + 1) % ccount;
                        }
                        self.center_on_drones();
                    }
                    "Jump to Sea Level" => {
                        self.world_state.level =
                            self.props.sea_level as i32 + 9;
                    }
                    "Pause" => {
                        self.screen = GameScreen::Paused;
                    }
                    _ => {}
                }
            } else if self.select_command && res3.is_some() {
                match res3.unwrap().as_str() {
                    "FELL TREES" => {
                        self.world_state.commands.push((
                            life::Order::FellTrees((
                                (0, 0),
                                (0, 0),
                                0,
                            )),
                            0,
                        ));
                    }
                    "MINE" => {
                        self.world_state.commands.push((
                            life::Order::Mine(((0, 0), (0, 0), 0)),
                            0,
                        ));
                    }
                    "CHANNEL" => {
                        self.world_state.commands.push((
                            life::Order::Channel(((0, 0), (0, 0), 0)),
                            0,
                        ));
                    }
                    "BUILD WALL" => {
                        self.screen = GameScreen::SelectMaterialType;
                        self.select_command = false;
                    }
                    "CANCEL" => {
                        self.select_command = false;
                        self.selection = (None, None);
                    }
                    _ => {}
                }
                if self.select_command {
                    self.screen = GameScreen::SelectArea;
                }
                self.select_command = false;
            } else if let Some(val) = res {
                match val.as_str() {
                    "Drones" => {
                        self.screen = GameScreen::JobView;
                        self.setup_drone_cards();
                    }
                    "Orders" => {
                        self.select_command = true;
                    }
                    _ => {}
                }
            } else if let Some(ref map) = self.world_state.map {
                let offset = self.world_state.screen;
                let (cx, cy) = utils::math::iso_to_2d((
                    self.world_state.cursor.0 - offset.0 as i32,
                    self.world_state.cursor.1 - offset.1 as i32,
                ));
                let (cx, cy) = utils::math::rot_switch(
                    (cx, cy),
                    self.world_state.xswitch,
                    self.world_state.yswitch,
                    MAP_SIZE,
                );
                let command = life::Order::Go((
                    cx,
                    cy,
                    self.world_state.level as usize,
                ));
                if let Some(t) = self.inspected_tile {
                    if let Tile::Empty(Some(i)) = t.1 {
                        if let Some(ref a) = map.life[i] {
                            let mut actor = a.borrow_mut();
                            actor.do_order(
                                command,
                                &mut self.world_state.commands,
                            );
                        }
                    }
                } else if self.screen == GameScreen::Game
                    && self.selection.0.is_none()
                {
                    self.select_command = true;
                    let offset = self.world_state.screen;
                    let (cx, cy) = utils::math::iso_to_2d((
                        self.world_state.cursor.0 - offset.0 as i32,
                        self.world_state.cursor.1 - offset.1 as i32,
                    ));
                    let (cx, cy) = utils::math::rot_switch(
                        (cx, cy),
                        self.world_state.xswitch,
                        self.world_state.yswitch,
                        MAP_SIZE,
                    );
                    self.selection = (Some((cx, cy)), None);
                }
            }
        } else if let Some(MouseButton::Right) = mouse {
            if let Some(ref map) = self.world_state.map {
                let offset = self.world_state.screen;
                let (cx, cy) = utils::math::iso_to_2d((
                    self.world_state.cursor.0 - offset.0 as i32,
                    self.world_state.cursor.1 - offset.1 as i32,
                ));

                let (cx, cy) = utils::math::rot_switch(
                    (cx, cy),
                    self.world_state.xswitch,
                    self.world_state.yswitch,
                    MAP_SIZE,
                );
                let z = self.world_state.level as usize;
                get_tile!(tile: map = map, (cx, cy), z);
                if let Some(t) = tile {
                    self.inspected_tile =
                        Some(((cx, cy, z), t.clone()));
                }
            }
        }
    }

    /// Center the screen on the currently selected drone, and then increment
    /// the selected drone id
    fn center_on_drones(&mut self) {
        let mut pos = (0, 0, 0);
        if let Some(ref map) = self.world_state.map {
            let mut i = 0;
            for l in map.life.iter() {
                if let Some(l) = l {
                    let l = l.borrow();
                    if l.species().species == life::Species::Robot {
                        if i == self.drone_focus {
                            pos = l.current_pos();
                        }
                        i += 1;
                    }
                }
            }
        }
        let pos2 = utils::math::rot_switch(
            (pos.0 as usize, pos.1 as usize),
            self.world_state.xswitch,
            self.world_state.yswitch,
            MAP_SIZE,
        );
        let (x, y) = utils::math::twod_to_iso(pos2);
        self.world_state.screen =
            (-(x as i32) + 1000, -(y as i32) + pos.2 as i32 * 64);
        self.world_state.level = pos.2 as i32 + 1;
    }

    /// Adjusts the size and position of UI items so that they match the new
    /// viewport size
    pub fn adjust_viewport(&mut self, w: u32, h: u32) {
        let (w, h) = (w as f64 * self.gui_scale, h as f64 * self.gui_scale);
        self.main_menu.set_pos((w / 6.0, h * (2.0 / 3.0)));

        self.game_tabs.set_pos((w / 2.0 - (w / 6.4), 5.0));
        self.game_tabs.set_size((w / 3.2, 30.0));
        self.game_functions.set_pos((w - (w / 4.95 + 5.0), 35.0));
        self.game_functions.set_size((w / 4.95, h / 6.5));

        self.world_defaults
            .set_pos((w / 2.0 - 150.0, h / 2.0 - 100.0));
        self.pause_menu.set_size((w / 1.28, h / 2.72));
        self.pause_menu.set_pos((
            w / 2.0 - (w / (2.0 * 1.28)),
            h / 2.0 - (h / (2.0 * 2.72)),
        ));
        self.command_selector
            .set_pos((w / 2.0 - 150.0, h / 2.0 - 200.0));
    }

    pub fn adjust_map_scale(&mut self, dx: f64) {
        let ndx = dx / 4.0;
        let new = self.map_scale + ndx;
        if new > 3.0 {
            self.map_scale = 3.0;
        } else if new < 1.0 {
            self.map_scale = 1.0;
        } else {
            self.map_scale = new;
        }
    }

    pub fn restore_camera_pos(
        &mut self,
        switched_x: bool,
        switched_y: bool,
    ) {
        let og_offset = self.world_state.screen;
        let og_tile_pos =
            utils::math::iso_to_2d((-og_offset.0, -og_offset.1));
        let new_tile_pos = utils::math::rot_switch(
            og_tile_pos,
            switched_x,
            switched_y,
            MAP_SIZE,
        );
        let new_offset = utils::math::twod_to_iso(new_tile_pos);
        self.world_state.screen = (-new_offset.0, -new_offset.1);
    }

    pub fn handle_game_keys(&mut self, key: &piston::input::Key) {
        match key {
            piston::input::Key::X => {
                self.world_state.xswitch = !self.world_state.xswitch;
                self.restore_camera_pos(true, false);
            }
            piston::input::Key::Y => {
                self.world_state.yswitch = !self.world_state.yswitch;
                self.restore_camera_pos(false, true);
            }
            piston::input::Key::Space => {
                self.screen = GameScreen::Paused
            }
            piston::input::Key::Tab => {
                self.show_hud = !self.show_hud;
            }
            piston::input::Key::O => {
                self.select_command = true;
            }
            piston::input::Key::J => {
                self.world_state.screen = (0, 0);
                self.screen = GameScreen::JobView;
                self.setup_drone_cards();
            }
            piston::input::Key::Return => {
                self.center_on_drones();
            }
            piston::input::Key::S => {
                self.world_state.level = clamp(
                    self.world_state.level - 1,
                    self.constants.highest_world,
                    0,
                );
            }
            piston::input::Key::W => {
                self.world_state.level = clamp(
                    self.world_state.level + 1,
                    self.constants.highest_world,
                    0,
                );
            }
            piston::input::Key::Up => self.move_delta(0, MOVE_DIST),
            piston::input::Key::Down => {
                self.move_delta(0, -MOVE_DIST)
            }
            piston::input::Key::Left => self.move_delta(MOVE_DIST, 0),
            piston::input::Key::Right => {
                self.move_delta(-MOVE_DIST, 0)
            }
            _ => {}
        };
    }

    fn handle_area_keys(&mut self, key: &piston::input::Key) {
        match key {
            piston::input::Key::W => {
                self.world_state.level = clamp(
                    self.world_state.level + 1,
                    self.constants.highest_world,
                    0,
                );
            }
            piston::input::Key::S => {
                self.world_state.level = clamp(
                    self.world_state.level - 1,
                    self.constants.highest_world,
                    0,
                );
            }
            piston::input::Key::Up => self.move_delta(0, MOVE_DIST),
            piston::input::Key::Down => {
                self.move_delta(0, -MOVE_DIST)
            }
            piston::input::Key::Left => self.move_delta(MOVE_DIST, 0),
            piston::input::Key::Right => {
                self.move_delta(-MOVE_DIST, 0)
            }
            _ => {}
        }
    }

    /// Handles the different key presses/releases
    fn handle_key(&mut self, key: &piston::input::Key) {
        match self.screen {
            GameScreen::SelectMaterialType => {}
            GameScreen::WorldChoice => {}
            GameScreen::JobView => {}
            GameScreen::SelectArea => self.handle_area_keys(key),
            GameScreen::GetSeed => {}
            GameScreen::Loading => {}
            GameScreen::Menu => {}
            GameScreen::Paused => {}
            GameScreen::Game => self.handle_game_keys(key),
        }
    }
}

fn load_textures(window: &mut PistonWindow) -> TextureMap {
    let mut texture_map: TextureMap = HashMap::new();
    let walker = walkdir::WalkDir::new("assets").into_iter();

    // Iterate through every .PNG file, recursively, in assets
    for entry in walker.filter_map(|e| e.ok()).filter(|e| {
        e.file_type().is_file()
            && e.file_name()
                .to_str()
                .unwrap()
                .to_string()
                .find(".png")
                .is_some()
    }) {
        // Texture settings
        let mut ts = TextureSettings::new();
        ts.set_mag(texture::Filter::Nearest);
        ts.set_min(texture::Filter::Nearest);

        // File's name, without postfix
        let name =
            entry.path().file_stem().unwrap().to_str().unwrap();

        // Grab the asset
        let asset: Result<G2dTexture, _> = Texture::from_path(
            &mut window.factory,
            entry.path(),
            Flip::None,
            &ts,
        );
        if asset.is_err() {
            panic!(
                "Can't load tile {}: {}",
                name,
                entry.path().display()
            );
        }

        let number = name.parse::<usize>();
        match number {
            // If it has a number for a name, look at the directory name, etc...
            Ok(n) => {
                let name = if let Some(Component::Normal(dir_name)) =
                    entry.path().parent().unwrap().components().last()
                {
                    dir_name.to_str().unwrap()
                } else {
                    name
                };

                // Add key if not already there
                if !texture_map.contains_key(name) {
                    texture_map.insert(name.to_string(), Vec::new());
                }

                let texture = texture_map.get_mut(name).unwrap();
                if n >= texture.len() {
                    texture.resize(
                        n + 1,
                        G2dTexture::empty(&mut window.factory)
                            .unwrap(),
                    );
                }
                texture[n] = asset.unwrap();
            }
            Err(_) => {
                texture_map
                    .insert(name.to_string(), vec![asset.unwrap()]);
            }
        }
    }
    texture_map
}

fn main() {
    //Create Window
    let mut window: PistonWindow =
        WindowSettings::new("Astra Terra", [WINDOW_SIZE.0 as u32, WINDOW_SIZE.1 as u32])
            .exit_on_esc(false)
            .build()
            .unwrap();
    let mut game = Game::new((WINDOW_SIZE.0 / 32, WINDOW_SIZE.1 / 32));
    game.texture_map = load_textures(&mut window);

    let factory = window.factory.clone();

    game.glyphs = Some(
        Glyphs::new(
            Path::new("assets/fonts/TerminusTTF-Bold-4.46.0.ttf"),
            factory,
            TextureSettings::new(),
        )
        .unwrap(),
    );

    window.set_max_fps(120);
    window.set_ups(60);

    game.gui_scale = window.window.window.hidpi_factor() as f64;
    game.map_scale = window.window.window.hidpi_factor() as f64;
    game.adjust_viewport(window.size().width, window.size().height);

    while let Some(e) = window.next() {
        if let Some(r) = e.render_args() {
            game.draw(&e, &r, &mut window);
            game.world_state.frame += 1;
        }

        if let Some(piston::input::Button::Mouse(b)) = e.press_args()
        {
            game.handle_mouse(Some(&b));
        }

        if let Some([x, y]) = e.mouse_cursor_args() {
            let mouse_fix = 85.7 * game.gui_scale;
            let nplace =
                ((x as f64 * game.gui_scale) as i32,
                 (y as f64 * game.gui_scale + mouse_fix) as i32);
            game.world_state.cursor = nplace;
            game.handle_mouse(None);
        }
        if let Some([x, y]) = e.mouse_scroll_args() {
            if game.screen == GameScreen::JobView {
                game.move_delta(-x as i32 * 6, y as i32 * 6);
            } else {
                game.adjust_map_scale(y);
            }
        }
        if let Some([w, h]) = e.resize_args() {
            game.adjust_viewport(w, h);
        }

        if let Some(piston::input::Button::Keyboard(b)) =
            e.press_args()
        {
            game.handle_key(&b);
        }

        if let Some(u) = e.update_args() {
            game.update(&u);
            game.constants.screen_size = (
                window.size().width as i32 * game.gui_scale as i32,
                window.size().height as i32 * game.gui_scale as i32,
            );
        }
    }
    unsafe {
        match game.world_state.map {
            Some(x) => x.delete_heightmap(),
            None => {}
        }
    }
}
