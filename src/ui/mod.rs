extern crate graphics;
use crate::time;
use crate::utils;
use piston_window::*;
use std;

pub trait UIElement {
    fn draw(
        &self,
        e: &Event,
        c: &graphics::Context,
        g: &mut G2d,
        args: &RenderArgs,
        glyphs: &mut Glyphs,
    );
    fn enabled(&self) -> bool;
    fn is_intersecting(
        &mut self,
        cursor: (f64, f64),
    ) -> Option<String>;
    fn set_size(&mut self, sz: (f64, f64));
    fn set_pos(&mut self, pos: (f64, f64));
}

pub struct ButtonGroup {
    pub pos_x: f64,
    pub pos_y: f64,

    pub width: f64,
    pub height: f64,

    buttons: Vec<Button>,
    orientation: Orientation,
}

#[derive(Copy, Clone)]
pub enum Orientation {
    Horizontal,
    Vertical,
}

impl ButtonGroup {
    pub fn new(
        [sx, sy, w, h]: [f64; 4],
        labels: Vec<&str>,
        orientation: Orientation,
        colors: Vec<[f32; 4]>,
    ) -> Self {
        let len = labels.len() as f64;
        ButtonGroup {
            pos_x: sx,
            pos_y: sy,
            width: w,
            height: h,
            orientation: orientation.clone(),
            buttons: labels
                .iter()
                .enumerate()
                .map(|(i, l)| {
                    Button::new(
                        match orientation {
                            Orientation::Horizontal => [
                                i as f64 * (w / len + 10.0),
                                0.0,
                                w / len,
                                h,
                            ],
                            Orientation::Vertical => [
                                0.0,
                                i as f64 * (h / len + 10.0),
                                w,
                                h / len,
                            ],
                        },
                        l.to_string(),
                        *colors.get(i).unwrap_or(&colors[0]),
                        false,
                    )
                })
                .collect(),
        }
    }
}

impl UIElement for ButtonGroup {
    fn draw(
        &self,
        e: &Event,
        c: &graphics::Context,
        g: &mut G2d,
        args: &RenderArgs,
        glyphs: &mut Glyphs,
    ) {
        let mut c2 = c.clone();
        c2.transform = c.transform.trans(self.pos_x, self.pos_y);
        for b in self.buttons.iter() {
            b.draw(e, &c2, g, args, glyphs);
        }
    }
    fn enabled(&self) -> bool {
        true
    }
    fn is_intersecting(
        &mut self,
        cursor: (f64, f64),
    ) -> Option<String> {
        let cursor = (cursor.0 - self.pos_x, cursor.1 - self.pos_y);
        self.buttons
            .iter_mut()
            .filter_map(|b| b.is_intersecting(cursor))
            .nth(0)
    }
    fn set_size(&mut self, sz: (f64, f64)) {
        self.width = sz.0;
        self.height = sz.1;
        let len = self.buttons.len() as f64;
        for (i, b) in self.buttons.iter_mut().enumerate() {
            b.set_size(match self.orientation {
                Orientation::Horizontal => (sz.0 / len, sz.1),
                Orientation::Vertical => (sz.0, sz.1 / len),
            });
            b.set_pos(match self.orientation {
                Orientation::Horizontal => {
                    (i as f64 * (sz.0 / len + 10.0), 0.0)
                }
                Orientation::Vertical => {
                    (0.0, i as f64 * (sz.1 / len + 10.0))
                }
            });
        }
    }
    fn set_pos(&mut self, sz: (f64, f64)) {
        self.pos_x = sz.0;
        self.pos_y = sz.1 + 100.0;
    }
}

pub struct Button {
    pub pos_x: f64,
    pub pos_y: f64,

    pub width: f64,
    pub height: f64,

    enabled: bool,
    color: [f32; 4],

    text: String,
    select_time: usize,
}

impl Button {
    pub fn new(
        [sx, sy, ex, ey]: [f64; 4],
        text: String,
        color: [f32; 4],
        enabled: bool,
    ) -> Self {
        Button {
            pos_x: sx,
            pos_y: sy,
            width: ex,
            height: ey,
            text: text,
            color: color,
            enabled: enabled,
            select_time: 0,
        }
    }
}

impl UIElement for Button {
    fn draw(
        &self,
        e: &Event,
        c: &graphics::Context,
        g: &mut G2d,
        args: &RenderArgs,
        glyphs: &mut Glyphs,
    ) {
        let transform = c.transform.trans(self.pos_x, self.pos_y);
        let color = if self.enabled {
            [78.0 / 255.0, 134.0 / 255.0, 144.0 / 255.0, 0.9]
        } else {
            self.color
        };
        let time_diff = (time::get_millisecond_time() as usize
            - self.select_time) as f64;
        graphics::Polygon::new(color).draw_tween_lerp(
            &[
                &[
                    [10.0, 0.0],
                    [self.width, 0.0],
                    [self.width, self.height - 10.0],
                    [self.width - 10.0, self.height],
                    [0.0, self.height],
                    [0.0, 10.0],
                ],
                &[
                    [20.0, 0.0],
                    [self.width, 0.0],
                    [self.width, self.height - 20.0],
                    [self.width - 20.0, self.height],
                    [0.0, self.height],
                    [0.0, 20.0],
                ],
            ],
            if self.enabled {
                if time_diff < (1000.0 / 2.0) {
                    time_diff / 1000.0
                } else {
                    0.5
                }
            } else {
                0.0
            },
            &c.draw_state,
            transform,
            g,
        );
        let [r, g_, b, a] = color;
        let avg = (r + g_ + b + a) / 4.0;
        let text_color = if avg > 0.5 {
            [0.0, 0.0, 0.0, 1.0]
        } else {
            [1.0, 1.0, 1.0, 1.0]
        };
        let ts = std::cmp::min(
            self.height as u32 / 2,
            (self.width as u32 / 4)
                - self.text.len() as u32 / (self.width as u32 / 3),
        );
        text::Text::new_color(text_color, ts)
            .draw(
                &self.text.to_uppercase(),
                glyphs,
                &c.draw_state,
                transform
                    .trans(16.0, self.height / 2.0 + ts as f64 / 2.0),
                g,
            )
            .unwrap();
    }
    fn enabled(&self) -> bool {
        self.enabled
    }
    fn is_intersecting(
        &mut self,
        cursor: (f64, f64),
    ) -> Option<String> {
        let res = cursor.0 > self.pos_x
            && cursor.0 < self.pos_x + self.width
            && cursor.1 > self.pos_y
            && cursor.1 < self.pos_y + self.height;
        if res != self.enabled {
            self.select_time = time::get_millisecond_time() as usize;
        }
        self.enabled = res;
        if self.enabled {
            Some(self.text.clone())
        } else {
            None
        }
    }
    fn set_size(&mut self, sz: (f64, f64)) {
        self.width = sz.0;
        self.height = sz.1;
    }
    fn set_pos(&mut self, sz: (f64, f64)) {
        self.pos_x = sz.0;
        self.pos_y = sz.1;
    }
}

#[derive(Debug)]
/// A DSL for drawing basic things inside a UIView
pub enum UITypes {
    /// An image, on a new line
    Image(G2dTexture, f64),
    /// Text, on a new line
    Text(String, i32),
    /// An Image, with text wrapped around it
    ImageTextGroup(G2dTexture, String, i32, f64),
}

pub struct UIView {
    pub pos_x: f64,
    pub pos_y: f64,

    pub width: f64,
    pub height: f64,

    ui_elements: Vec<UITypes>,
}

impl UIView {
    pub fn new([x, y, w, h]: [f64; 4], uie: Vec<UITypes>) -> Self {
        UIView {
            pos_x: x,
            pos_y: y,
            width: w,
            height: h,
            ui_elements: uie,
        }
    }
}

impl UIElement for UIView {
    fn draw(
        &self,
        e: &Event,
        c: &graphics::Context,
        g: &mut G2d,
        args: &RenderArgs,
        glyphs: &mut Glyphs,
    ) {
        let c2 = c.clone();
        let mut transform =
            c2.transform.trans(self.pos_x, self.pos_y);
        graphics::Polygon::new([0.0, 0.0, 0.0, 0.75]).draw(
            &[
                [20.0, 0.0],
                [self.width, 0.0],
                [self.width, self.height],
                [0.0, self.height],
                [0.0, 20.0],
            ],
            &c.draw_state,
            transform,
            g,
        );
        transform = transform.trans(40.0, 20.0);
        for e in self.ui_elements.iter() {
            match e {
                &UITypes::Image(ref texture, scale) => {
                    image(texture, transform.zoom(scale), g);
                    transform = transform.trans(
                        0.0,
                        texture.get_height() as f64 * scale + 20.0,
                    );
                }
                &UITypes::ImageTextGroup(
                    ref texture,
                    ref text,
                    size,
                    scale,
                ) => {
                    image(texture, transform.zoom(scale), g);
                    transform = transform.trans(
                        texture.get_width() as f64 * scale + 20.0,
                        20.0,
                    );
                    use std::str;
                    let lines = text.split("\n").collect::<Vec<_>>();
                    let wrap_chars = ((self.width - 80.0) as f64
                        / (size as f64 / 1.5))
                        as usize;
                    let subs = lines
                        .iter()
                        .flat_map(|line| {
                            line.as_bytes()
                                .chunks(wrap_chars)
                                .map(str::from_utf8)
                                .collect::<Result<Vec<&str>, _>>()
                                .unwrap()
                        })
                        .collect::<Vec<&str>>();
                    for (i, line) in subs.iter().enumerate() {
                        text::Text::new_color(
                            [1.0, 1.0, 1.0, 1.0],
                            size as u32,
                        )
                        .draw(
                            line.trim(),
                            glyphs,
                            &c.draw_state,
                            transform,
                            g,
                        )
                        .unwrap();
                        transform = transform.trans(
                            0.0,
                            utils::misc::line_height(size),
                        );
                    }
                    let h = texture.get_height() as f64 * scale;
                    let th = subs.len() as f64
                        * utils::misc::line_height(size);
                    transform = transform.trans(
                        -(texture.get_width() as f64 * scale + 20.0),
                        if subs.len() > 1 {
                            (0.0 as f64).max(
                                h - subs.len() as f64
                                    * utils::misc::line_height(size),
                            )
                        } else {
                            h
                        },
                    );
                }
                &UITypes::Text(ref text, size) => {
                    let subs = utils::misc::wrap(
                        text.clone(),
                        self.width,
                        size,
                    );
                    for (i, line) in subs.iter().enumerate() {
                        text::Text::new_color(
                            [1.0, 1.0, 1.0, 1.0],
                            size as u32,
                        )
                        .draw(
                            line.trim(),
                            glyphs,
                            &c.draw_state,
                            transform,
                            g,
                        )
                        .unwrap();
                        transform = transform.trans(
                            0.0,
                            utils::misc::line_height(size),
                        );
                    }
                }
            }
        }
    }
    fn enabled(&self) -> bool {
        true
    }
    fn is_intersecting(
        &mut self,
        cursor: (f64, f64),
    ) -> Option<String> {
        let res = cursor.0 > self.pos_x
            && cursor.0 < self.pos_x + self.width
            && cursor.1 > self.pos_y
            && cursor.1 < self.pos_y + self.height;
        if res {
            let mut fake_trans = (self.pos_x, self.pos_y);
            for ui in self.ui_elements.iter() {
                let (height, text) = match ui {
                    UITypes::Text(text, s) => (
                        utils::misc::wrap(
                            text.clone(),
                            self.width,
                            *s,
                        )
                        .len() as f64
                            * utils::misc::line_height(*s),
                        text.clone(),
                    ),
                    UITypes::ImageTextGroup(i, t, s, sz) => (
                        utils::misc::wrap(t.clone(), self.width, *s)
                            .len() as f64
                            * utils::misc::line_height(*s),
                        t.clone(),
                    ),
                    UITypes::Image(t, _) => {
                        (t.get_height() as f64, format!("{:?}", t))
                    }
                };
                if cursor.0 > fake_trans.0
                    && cursor.0 < fake_trans.0 + self.width
                    && cursor.1 > fake_trans.1
                    && cursor.1 < fake_trans.1 + height
                {
                    return Some(text);
                } else {
                    fake_trans =
                        (fake_trans.0, fake_trans.1 + height);
                }
            }
            None
        } else {
            None
        }
    }
    fn set_size(&mut self, sz: (f64, f64)) {
        self.width = sz.0;
        self.height = sz.1;
    }
    fn set_pos(&mut self, sz: (f64, f64)) {
        self.pos_x = sz.0;
        self.pos_y = sz.1;
    }
}
