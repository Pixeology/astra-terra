extern crate graphics;
extern crate piston_window;

use crate::worldgen::GenProps;
use crate::ORect2D;
use graphics::{rectangle, Transformed, Rectangle};
use piston::input::*;
use piston_window::{text, G2d, G2dTexture, Glyphs};
use std::cell::Ref;

use crate::utils;
use std;

use crate::worldgen::terrain::{
    Item, Material, SoilTypes, State, StoneTypes, Tile, TreePart,
    VegType,
};

use crate::worldgen::{World, WorldState};

const MAP_SIZE: (usize, usize) = (500, 500); // TODO fix dup

pub trait Describe {
    fn describe(&self) -> String;
}

/// Draws a map, based on the current screen offset. It only iterates through
/// the units that are on screen at the time, and then also culls tiles after
/// checking their draw position. Only iterating through units that are on
/// screen can cause gaps in the world since units can be any number of tiles
/// tall. Does not cull tiles that are going to be completely covered by upper Z
/// levels. Also rotates the map view based on world state, by either 180 or 90
/// degrees. Draws the cursor over the map, by drawing the number of levels the
/// cursor is away from ground level, and also drawing another cursor below it
/// for each of those levels.
pub fn draw_map(
    e: &Event,
    args: &RenderArgs,
    props: &GenProps,
    world: &WorldState,
    selection: ORect2D,
    c: &mut graphics::Context,
    g: &mut G2d,
    (show_width, show_height): (u32, u32),
    map_scale: f64,
    texture_map: &std::collections::HashMap<String, Vec<G2dTexture>>,
    glyphs: &mut Glyphs,
    time: usize,
) {
    let offset = (world.screen.0 as f64, world.screen.1 as f64);
    c.transform = c.transform.trans(offset.0, offset.1);

    if let Some(ref map) = world.map {
        let s = world.screen;
        let cursor_screen_pos = (
            ((world.cursor.0 as f64 - 64.0) * map_scale - offset.0) as i32,
            ((world.cursor.1 as f64 - 64.0) * map_scale - offset.1) as i32,
        );
        // def for later
        let (cx, cy) = utils::math::iso_to_2d_cursor(cursor_screen_pos);

        let height_adjustment =
            ((world.level as f64).powf(1.35) / 2.0 + 1.0).round()
                as i32;
        for y in 0..(show_height as i32 / 32 + height_adjustment + 1)
        {
            for x in 0..(show_width as i32 / 64 + 3) {
                let (x, y) = utils::math::iso_to_2d((
                    x * 64 - offset.0 as i32 - 128,
                    y * 32 - offset.1 as i32 - height_adjustment * 32,
                ));
                let tile =
                    map.get_rot((x, y), world.xswitch, world.yswitch);
                if let Some(ref unit) = tile {
                    let arc = unit.tiles.clone();
                    let ut = arc.read().unwrap();
                    let surrounding_height = vec![
                        (x + 1, y),
                        (x.checked_sub(1).unwrap_or(x), y),
                        (x, y.checked_sub(1).unwrap_or(y)),
                        (x, y + 1),
                    ]
                    .into_iter()
                    .filter_map(|pnt| {
                        if pnt != (x, y) {
                            Some(std::cmp::min(
                                world.level,
                                std::cmp::min(
                                    utils::misc::unit_height(
                                        &map.map, pnt,
                                    ),
                                    utils::misc::location_z(
                                        &map.map, pnt,
                                    )
                                        as i32,
                                ),
                            ))
                        } else {
                            None
                        }
                    })
                    .fold(None, |min, x| match min {
                        None => Some(x),
                        Some(min) => {
                            Some(if min >= x { x } else { min })
                        }
                    })
                    .unwrap();
                    for l in std::cmp::max(0, surrounding_height - 1)
                        ..(world.level + 1)
                    {
                        let pos = (x, y, l - world.level);
                        let height =
                            utils::misc::location_z(&map.map, (x, y))
                                as i32;
                        if let Some(t) = ut.get(l as usize) {
                            draw_tile(
                                g,
                                c,
                                args,
                                pos,
                                t,
                                height,
                                l,
                                props,
                                texture_map,
                                world,
                                map,
                                offset,
                            );
                        } else {
                            break;
                        }
                        let h = l - height;
                        if cx == x
                            && cy == y
                            && h >= 0
                            && world.level != l
                        {
                            utils::misc::display_terrain(
                                g,
                                c,
                                args,
                                pos,
                                &texture_map.get("cursor").unwrap()
                                    [0],
                            );
                            let (x, y) =
                                utils::math::twod_to_iso((x, y));
                            let trans = c.transform.trans(
                                x as f64 + 50.0,
                                y as f64 - pos.2 as f64 * 64.0 + 32.0,
                            );
                            text::Text::new_color(
                                [1.0, 1.0, 1.0, 1.0],
                                20,
                            )
                            .draw(
                                &format!("{:?}", (h, cx, cy)),
                                glyphs,
                                &c.draw_state,
                                trans,
                                g,
                            )
                            .unwrap();
                        }
                    }
                }
                if cx == x && cy == y && tile.is_some() {
                    utils::misc::display_terrain(
                        g,
                        c,
                        args,
                        (cx, cy, 0),
                        &texture_map.get("cursor").unwrap()[0],
                    );
                    let (x, y) = utils::math::twod_to_iso((x, y));
                    let trans = c
                        .transform
                        .trans(x as f64 + 50.0, y as f64 + 32.0);
                    let (cx, cy) = utils::math::rot_switch(
                        (cx, cy),
                        world.xswitch,
                        world.yswitch,
                        MAP_SIZE,
                    );
                    text::Text::new_color([1.0, 1.0, 1.0, 1.0], 20)
                        .draw(
                            &format!("{:?}", (cx, cy)),
                            glyphs,
                            &c.draw_state,
                            trans,
                            g,
                        )
                        .unwrap();
                }
                match selection {
                    (Some((x1, y1)), Some((x2, y2))) => {
                        if (x >= x1 && x <= x2 && y >= y1 && y <= y2)
                            || (x >= x1
                                && x <= x2
                                && y <= y1
                                && y >= y2)
                            || (x >= x2
                                && x <= x1
                                && y >= y2
                                && y <= y1)
                            || (x >= x2
                                && x <= x1
                                && y <= y2
                                && y >= y1)
                        {
                            utils::misc::display_terrain(
                                g,
                                c,
                                args,
                                (x, y, 0),
                                &texture_map.get("cursor").unwrap()
                                    [0],
                            );
                        }
                    }

                    (Some((x1, y1)), None) => {
                        if (x >= x1 && x <= cx && y >= y1 && y <= cy)
                            || (x >= x1
                                && x <= cx
                                && y <= y1
                                && y >= cy)
                            || (x >= cx
                                && x <= x1
                                && y >= cy
                                && y <= y1)
                            || (x >= cx
                                && x <= x1
                                && y <= cy
                                && y >= y1)
                        {
                            utils::misc::display_terrain(
                                g,
                                c,
                                args,
                                (x, y, 0),
                                &texture_map.get("cursor").unwrap()
                                    [0],
                            );
                        }
                    }
                    _ => {}
                }
            }
        }
        rectangle(
            [1.0, 0.0, 1.0, 1.0],
            [cursor_screen_pos.0 as f64, cursor_screen_pos.1 as f64, 64.0, 32.0],
            c.transform,
            g
        );
    }
}

/// Selects the correct texture name for the given tile.
pub fn texture_name(
    t: Tile,
    l: usize,
    height: usize,
    sea_level: usize,
) -> String {
    match t {
        Tile::SolidBlock(_) => "stone".to_string(),
        Tile::Water(_, State::Liquid, _) => if l < sea_level - 1 {
            "water_cube"
        } else {
            "water"
        }
        .to_string(),
        Tile::Fire => "fire".to_string(),
        Tile::Water(_, State::Solid, _) => "ice".to_string(),
        Tile::Item(Item::Material(mt, ..), ..) => match mt {
            Material::Stone(..) => "stone_chunk".to_string(),
            Material::Metal(..) => "metal_chunk".to_string(),
            Material::Wood(..) => "wood_chunk".to_string(),
        },
        Tile::Stone(st, ..) => match st {
            StoneTypes::Sedimentary(ref s) => "stone".to_string(),
            StoneTypes::Metamorphic(ref s) => "stone".to_string(),
            StoneTypes::Igneous(ref s) => "stone".to_string(),
            StoneTypes::Soil(ref s) => match s {
                &SoilTypes::DesertSand => {
                    "desert_block_1".to_string()
                }
                &SoilTypes::DisturbedDesertSand => {
                    "sandstone".to_string()
                }
                &SoilTypes::Sandy => "sand".to_string(),
                &SoilTypes::Silty => "dirt".to_string(),
                &SoilTypes::Watery => "water_shallow".to_string(),
                &SoilTypes::Snowy => {
                    if l < height.checked_sub(1).unwrap_or(0) {
                        "snow_cube".to_string()
                    } else {
                        "snow".to_string()
                    }
                }
                &SoilTypes::Peaty => "pasture_block_1".to_string(),
                _ => "dirt".to_string(),
            },
        },
        Tile::Vegetation(vt, ..) => {
            use self::VegType::*;
            match vt {
                // Desert Biome
                DesertGrass1 => "desert_grass_1",
                DesertGrass2 => "desert_grass_2",
                DesertGrass3 => "desert_grass_3",
                DesertGrass4 => "desert_grass_4",
                DesertGrass5 => "desert_grass_5",
                DesertTree1(TreePart::Base) => "generic_tree_1_base",
                DesertTree1(TreePart::Trunk) => {
                    "generic_tree_1_trunk"
                }
                DesertTree1(TreePart::Branches(1)) => {
                    "generic_tree_1_branch_1"
                }
                DesertTree1(TreePart::Branches(2)) => {
                    "generic_tree_1_branch_3"
                }
                DesertTree1(TreePart::Branches(3)) => {
                    "generic_tree_1_branch_2"
                }
                DesertTree1(TreePart::Midsection) => {
                    "generic_tree_1_midsection"
                }
                DesertTree1(TreePart::Leaves(_)) => {
                    "generic_tree_1_leaves"
                }
                // Forest Biome
                ForestGrass1 => "forest_grass_1",
                ForestGrass2 => "forest_grass_2",
                ForestGrass3 => "forest_grass_3",
                ForestGrass4 => "forest_grass_4",
                ForestGrass5 => "forest_grass_5",
                ForestTree1(TreePart::Base) => "generic_tree_1_base",
                ForestTree1(TreePart::Trunk) => {
                    "generic_tree_1_trunk"
                }
                ForestTree1(TreePart::Branches(1)) => {
                    "generic_tree_1_branch_1"
                }
                ForestTree1(TreePart::Branches(2)) => {
                    "generic_tree_1_branch_3"
                }
                ForestTree1(TreePart::Branches(3)) => {
                    "generic_tree_1_branch_2"
                }
                ForestTree1(TreePart::Midsection) => {
                    "generic_tree_1_midsection"
                }
                ForestTree1(TreePart::Leaves(_)) => {
                    "generic_tree_1_leaves"
                }
                ForestTree2(TreePart::Base) => "generic_tree_1_base",
                ForestTree2(TreePart::Trunk) => {
                    "generic_tree_1_trunk"
                }
                ForestTree2(TreePart::Branches(1)) => {
                    "generic_tree_1_branch_1"
                }
                ForestTree2(TreePart::Branches(2)) => {
                    "generic_tree_1_branch_3"
                }
                ForestTree2(TreePart::Branches(3)) => {
                    "generic_tree_1_branch_2"
                }
                ForestTree2(TreePart::Midsection) => {
                    "generic_tree_1_midsection"
                }
                ForestTree2(TreePart::Leaves(1)) => "empty",
                ForestTree2(TreePart::Leaves(_)) => "empty",
                // Pasture Biome
                PastureGrass1 => "pasture_grass_1",
                PastureGrass2 => "pasture_grass_2",
                PastureGrass3 => "pasture_grass_3",
                PastureGrass4 => "pasture_grass_4",
                PastureGrass5 => "pasture_grass_5",
                PastureTree1(TreePart::Base) => "generic_tree_1_base",
                PastureTree1(TreePart::Trunk) => {
                    "generic_tree_1_trunk"
                }
                PastureTree1(TreePart::Midsection) => {
                    "generic_tree_1_midsection"
                }
                PastureTree1(TreePart::Branches(1)) => {
                    "generic_tree_1_branch_1"
                }
                PastureTree1(TreePart::Branches(2)) => {
                    "generic_tree_1_branch_3"
                }
                PastureTree1(TreePart::Branches(3)) => {
                    "generic_tree_1_branch_2"
                }
                PastureTree1(TreePart::Leaves(_)) => {
                    "generic_tree_1_leaves"
                }
                PastureTree2(TreePart::Base) => "generic_tree_1_base",
                PastureTree2(TreePart::Trunk) => {
                    "generic_tree_1_trunk"
                }
                PastureTree2(TreePart::Branches(1)) => {
                    "generic_tree_1_branch_1"
                }
                PastureTree2(TreePart::Branches(2)) => {
                    "generic_tree_1_branch_3"
                }
                PastureTree2(TreePart::Branches(3)) => {
                    "generic_tree_1_branch_2"
                }
                PastureTree2(TreePart::Midsection) => {
                    "generic_tree_1_midsection"
                }
                PastureTree2(TreePart::Leaves(_)) => {
                    "generic_tree_1_leaves"
                }
                // Beach Biome
                PalmTree(TreePart::Base) => "generic_tree_1_base",
                PalmTree(TreePart::Trunk) => "generic_tree_1_trunk",
                PalmTree(TreePart::Midsection) => {
                    "generic_tree_1_midsection"
                }
                PalmTree(TreePart::Branches(1)) => {
                    "generic_tree_1_branch_1"
                }
                PalmTree(TreePart::Branches(2)) => {
                    "generic_tree_1_branch_3"
                }
                PalmTree(TreePart::Branches(3)) => {
                    "generic_tree_1_branch_2"
                }
                PalmTree(TreePart::Leaves(_)) => {
                    "generic_tree_1_leaves"
                }
                GroupedPalmTree(TreePart::Base) => {
                    "generic_tree_1_base"
                }
                GroupedPalmTree(TreePart::Trunk) => {
                    "generic_tree_1_trunk"
                }
                GroupedPalmTree(TreePart::Branches(1)) => {
                    "generic_tree_1_branch_1"
                }
                GroupedPalmTree(TreePart::Branches(2)) => {
                    "generic_tree_1_branch_3"
                }
                GroupedPalmTree(TreePart::Branches(3)) => {
                    "generic_tree_1_branch_2"
                }
                GroupedPalmTree(TreePart::Midsection) => {
                    "generic_tree_1_midsection"
                }
                GroupedPalmTree(TreePart::Leaves(_)) => {
                    "generic_tree_1_leaves"
                }
                _ => panic!("Unknown veg {:?}", vt),
            }
            .to_string()
        }
        _ => "grass_3".to_string(),
    }
}

use crate::life::drone;
use crate::life::Living;
use crate::life::Species;
/// Chooses the appropriate stack of textures, in order of closeness to the
/// 'camera', for the given species and actor.
pub fn life_texture(
    s: Species,
    life: &Ref<Box<dyn Living>>,
) -> Vec<String> {
    match s {
        Species::Robot => {
            let delta = life.current_delta();
            let mut texture_names = vec![];
            let l: &drone::Drone =
                match life.as_any().downcast_ref::<drone::Drone>() {
                    Some(b) => b,
                    None => unreachable!(),
                };
            if delta.0 < 0 {
                let mut i: i32 = 2;
                while i >= 0 {
                    texture_names.push(
                        l.addons[i as usize].get_graphic(delta),
                    );
                    i -= 1;
                }
            } else if delta.0 >= 0 {
                let mut i = 0;
                while i < 3 {
                    texture_names
                        .push(l.addons[i].get_graphic(delta));
                    i += 1;
                }
            }
            texture_names
        }
        _ => unimplemented!(),
    }
}

use crate::worldgen;
pub fn draw_actor(
    g: &mut G2d,
    c: &graphics::Context,
    args: &RenderArgs,
    id: Option<usize>,
    world: &WorldState,
    map: &worldgen::World,
    pos: utils::math::Screenable3D,
    texture_map: &std::collections::HashMap<String, Vec<G2dTexture>>,
    offset: (f64, f64),
) {
    if let Some(id) = id {
        let actor = &map.life[id];
        if let Some(actor) = actor {
            let life = actor.borrow();
            /*let mut place = utils::misc::get_delta_place(
                    life.current_pos(),
                    life.next_pos(),
                    life.last_pos(),
                    world.frame,
                    life.frame_last_action(),
                    world.level as f64,
                    life.action_speed(),
                    pos,
                    life.current_delta(),
                    life.last_delta(),
            );*/
            let place = {
                let screen = utils::math::twod_to_iso((pos.0, pos.1));
                (screen.0, screen.1 - pos.2 as i32 * 64)
            };
            let s = life.species().species;
            let delta = life.current_delta();
            let (behind, front) = match delta {
                // "fl"
                (1, -1) | (0, -1) | (0, 0) => ((-46, -24), (0, 0)),
                // "bl"
                (1, _) | (0, 1) => ((46, 0), (0, 0)),
                // "fr"
                (-1, -1) => ((46, 48), (-32, 32)),
                // "br"
                (-1, _) => ((64, 64), (32, 32)),
                _ => unreachable!(),
            };
            let offsets = vec![
                // behind
                behind,
                (0, 0),
                front,
            ];
            for (i, texture_name) in
                life_texture(s, &life).iter().enumerate()
            {
                if texture_name != "fl_empty"
                    && texture_name != "fr_empty"
                    && texture_name != "bl_empty"
                    && texture_name != "br_empty"
                {
                    let (x, y) = (
                        place.0 as i32 + offsets[i].0,
                        place.1 as i32 - 16 + offsets[i].1,
                    );
                    let texture =
                        &texture_map.get(texture_name).unwrap()[0];
                    utils::misc::display_image(g, c, (x, y), texture);
                }
            }
        }
    }
}

pub fn draw_tile(
    g: &mut G2d,
    c: &graphics::Context,
    args: &RenderArgs,
    pos: utils::math::Screenable3D,
    t: &Tile,
    height: i32,
    l: i32,
    props: &GenProps,
    texture_map: &std::collections::HashMap<String, Vec<G2dTexture>>,
    world: &WorldState,
    map: &World,
    offset: (f64, f64),
) {
    let anim_id = {
        let multi = height as f64 + pos.0 as f64 + pos.1 as f64;
        (0.5 * multi * (multi + 1.0) * pos.0 as f64) as usize
    };
    match t {
        &Tile::Item(x) => {
            // <temp>
            let x = texture_name(
                *t,
                l as usize,
                height as usize,
                props.sea_level as usize,
            );
            // </temp>
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    &format!("{}", x),
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::SolidBlock(ref s) => {
            let s = texture_name(*t, l as usize, height as usize, props.sea_level as usize);
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    &s,
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::Stone(ref s, State::Solid) => {
            let s = texture_name(
                *t,
                l as usize,
                height as usize,
                props.sea_level as usize,
            );
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    &s,
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::Stone(_, State::Liquid) => {
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    "stone",
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::Stone(_, State::Gas) => unreachable!(),
        &Tile::Water(_, State::Solid, _) => {
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    "ice",
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::Water(_, State::Liquid, _) => {
            let s = texture_name(
                *t,
                l as usize,
                height as usize,
                props.sea_level as usize,
            );
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    &s,
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::Water(_, State::Gas, _) => {
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    "steam",
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::Vegetation(ref v, ..) => {
            let v = texture_name(
                *t,
                l as usize,
                height as usize,
                props.sea_level as usize,
            );
            let tex = &utils::misc::texture_get(
                texture_map,
                &format!("{}", v),
                anim_id,
                0.41,
            );
            if v.contains("_leaves") {
                utils::misc::display_terrain(
                    g,
                    c,
                    args,
                    (
                        pos.0.checked_sub(3).unwrap_or(pos.0),
                        pos.1,
                        pos.2 + 2,
                    ),
                    tex,
                );
            } else {
                utils::misc::display_terrain(g, c, args, pos, tex);
            }
        }
        &Tile::Fire => {
            utils::misc::display_terrain(
                g,
                c,
                args,
                pos,
                &utils::misc::texture_get(
                    texture_map,
                    "fire",
                    anim_id,
                    0.41,
                ),
            );
        }
        &Tile::Empty(id) => draw_actor(
            g,
            c,
            args,
            id,
            world,
            map,
            pos,
            texture_map,
            offset,
        ),
        _ => {}
    }
}
