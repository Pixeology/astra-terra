extern crate rand;

use std::collections::BTreeMap;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::sync::Mutex;

use graphics::Transformed;
use piston::input::RenderArgs;

use std;
use std::iter::Iterator;

use piston_window::{image, Context, G2d, G2dTexture};

use crate::utils::math::*;

use crate::worldgen;
use crate::worldgen::terrain::{
    IgneousRocks, Material, MetalState, MetalType, MetamorphicRocks,
    StoneTypes, Tile, VegType,
};
use crate::worldgen::{Unit, WorldMap};

use crate::physics::PhysicsActor;

use self::rand::Rng;
use crate::time;

pub fn nearest_rect_points<F>(
    map: &WorldMap,
    rect: Rect2D3D,
    start: Point3D,
    test: F,
) -> Vec<Point3D>
where
    F: Fn(&Unit, Point3D) -> Option<usize>,
{
    let mut pnts = rect_points(map, rect, test);
    pnts.sort_by(|x, y| {
        distance_3d(*x, start)
            .partial_cmp(&distance_3d(*y, start))
            .unwrap_or(std::cmp::Ordering::Equal)
    });
    pnts
}

pub fn rect_points<F>(
    map: &WorldMap,
    rect: Rect2D3D,
    test: F
) -> Vec<Point3D>
where F: Fn(&Unit, Point3D) -> Option<usize>,
{
    let pnts = points_in_rect(map, rect);
    pnts
        .iter()
        .filter_map(|x| {
            let unit = map_get(map, (x.0, x.1));
            if let Some(ref u) = unit {
                test(u, *x).map(|z| (x.0, x.1, z))
            } else {
                None
            }
        })
        .collect::<Vec<_>>()
}

pub fn nearest_rect_point<F>(
    map: &WorldMap,
    rect: Rect2D3D,
    start: Point3D,
    test: F,
) -> Option<Point3D>
where
    F: Fn(&Unit, Point3D) -> Option<usize>,
{
    nearest_rect_points(map, rect, start, test)
        .first()
        .map(|x| *x)
}

pub fn is_traversable(
    map: &WorldMap,
    pnt: Point3D,
) -> Option<Point3D> {
    let row = map.get(pnt.0)?;
    let unit = &row.get(pnt.1)?;
    let arc = unit.tiles.clone();
    let tiles = arc.read().unwrap();

    let tile = tiles[pnt.2];
    let ground_height = unit_height(map, (pnt.0, pnt.1)) as usize;

    if ground_height <= pnt.2 + 1 {
        Some((pnt.0, pnt.1, ground_height))
    } else if !tile.solid() && pnt.2 == ground_height {
        Some(pnt)
    } else {
        None
    }
}

fn reconstruct_path(
    came_from: BTreeMap<Point3D, Option<Point3D>>,
    current: &Point3D,
) -> Option<Vec<Point3D>> {
    let mut path = vec![*current];
    if let Some(mut parent) = came_from[current] {
        path.push(parent);

        while let Some(Some(next_parent)) = came_from.get(&parent) {
            parent = *next_parent;
            path.insert(0, parent);
        }

        Some(path)
    } else {
        Some(path)
    }
}

pub fn find_path<'a>(
    map: &WorldMap,
    start: Point3D,
    goal: Point3D,
    can_move: impl Fn(&WorldMap, Point3D) -> bool + 'a,
) -> Option<Vec<Point3D>> {
    let mut frontier = BinaryHeap::new();
    frontier.push((0, start));

    let mut came_from = BTreeMap::new();
    came_from.insert(start, None);

    while let Some((_, current)) = frontier.pop() {
        if current == goal {
            return reconstruct_path(came_from, &current);
        }
        for next in strict_3d_adjacent(current)
            .iter()
            .filter_map(|pnt| is_traversable(map, *pnt))
        {
            if !came_from.contains_key(&next) {
                frontier
                    .push((-distance_3d(goal, next) as isize, next));
                came_from.insert(next, Some(current));
            }
        }
    }
    None
}

pub fn location_z(map: &WorldMap, pos: Point2D) -> usize {
    let unit = map_get(map, pos);
    if let Some(u) = unit {
        let mut len = 0;
        let arc = u.tiles.clone();
        let tiles = &arc.read().unwrap();
        for t in tiles.iter() {
            if matches!(t, Tile::Empty(None)) {
                break;
            }
            len += 1;
        }
        len
    } else {
        0
    }
}

pub fn easy_3d_adjacent(
    (x, y, z): Point3D,
    map: &WorldMap,
) -> Vec<Point3D> {
    strict_adjacent((x, y))
        .iter()
        .map(|pnt| (pnt.0, pnt.1, location_z(map, *pnt)))
        .filter(|pnt| *pnt != (x, y, z))
        .collect()
}

pub fn location_z_from_to(
    map: &WorldMap,
    pos: Point2D,
    sz: usize,
    gz: usize,
) -> usize {
    let unit = map_get(map, pos);
    if let Some(u) = unit {
        let mut len = 0;
        let arc = u.tiles.clone();
        let tiles = &arc.read().unwrap();
        for t in tiles.iter() {
            if ((gz <= sz && len <= sz) || (gz >= sz && len >= sz))
                && matches!(t, &Tile::Empty(None))
            {
                break;
            }
            len += 1;
        }
        len
    } else {
        0
    }
}

pub fn map_get(map: &WorldMap, pos: Point2D) -> Option<&Unit> {
    map.get(pos.1).and_then(|row| row.get(pos.0))
}

pub fn title_case(s: String) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f
            .to_uppercase()
            .chain(c.flat_map(|t| t.to_lowercase()))
            .collect(),
    }
}

pub fn namegen() -> String {
    let syllables: Vec<String> = vec![
        "kath".to_string(),
        "bud".to_string(),
        "bar".to_string(),
        "tarr".to_string(),
        "mon".to_string(),
        "bat".to_string(),
        "bor".to_string(),
        "dum".to_string(),
        "mat".to_string(),
        "rut".to_string(),
        "gar".to_string(),
        "stil".to_string(),
    ];
    let syllables2: Vec<String> = vec![
        "dzur".to_string(),
        "tsur".to_string(),
        "kzar".to_string(),
        "tcal".to_string(),
        "tral".to_string(),
        "kzad".to_string(),
        "tsum".to_string(),
        "dar".to_string(),
        "zzaz".to_string(),
        "satz".to_string(),
        "forr".to_string(),
        "marr".to_string(),
    ];

    let mut trng = rand::thread_rng();
    let syllable_num_fst = trng.gen_range(1, 2);
    let firstname = (0..syllable_num_fst)
        .map(|i| {
            syllables[trng.gen_range(0, syllables.len() - 1)].clone()
        })
        .fold("".to_string(), |s, x| x + &s);

    let syllable_num_snd = trng.gen_range(2, 3);
    let lastname = (0..syllable_num_snd)
        .map(|i| {
            syllables2[trng.gen_range(0, syllables.len() - 1)].clone()
        })
        .fold("".to_string(), |s, x| x + &s);

    title_case(firstname) + " " + &title_case(lastname)
}

pub fn is_tree(t: Tile) -> bool {
    match t {
        Tile::Vegetation(t, ..) => {
            use self::VegType::*;
            match t {
                ForestTree1(_) => true,
                ForestTree2(_) => true,
                DesertTree1(_) => true,
                PastureTree1(_) => true,
                PastureTree2(_) => true,
                PalmTree(_) => true,
                GroupedPalmTree(_) => true,
                _ => false,
            }
        }
        _ => false,
    }
}

pub fn get_material_from(
    mat: StoneTypes,
    state: worldgen::terrain::State,
) -> Material {
    match mat {
        StoneTypes::Igneous(IgneousRocks::Basalt) => {
            let mut trng = rand::thread_rng();
            if trng.gen_weighted_bool(2) {
                let metal = *trng
                    .choose(&[
                        MetalType::Copper,
                        MetalType::Copper,
                        MetalType::Gold,
                    ])
                    .unwrap();
                Material::Metal(
                    metal,
                    match state {
                        worldgen::terrain::State::Liquid => {
                            MetalState::Molten
                        }
                        _ => MetalState::Ore,
                    },
                )
            } else {
                Material::Stone(mat)
            }
        }
        StoneTypes::Igneous(IgneousRocks::Obsidian) => {
            let mut trng = rand::thread_rng();
            if trng.gen_weighted_bool(2) {
                let metal = *trng
                    .choose(&[
                        MetalType::Bronze,
                        MetalType::Bronze,
                        MetalType::Steel,
                    ])
                    .unwrap();
                Material::Metal(
                    metal,
                    match state {
                        worldgen::terrain::State::Liquid => {
                            MetalState::Molten
                        }
                        _ => MetalState::Ore,
                    },
                )
            } else {
                Material::Stone(mat)
            }
        }
        StoneTypes::Metamorphic(MetamorphicRocks::Gneiss) => {
            let mut trng = rand::thread_rng();
            if trng.gen_weighted_bool(2) {
                let metal = *trng
                    .choose(&[
                        MetalType::Iron,
                        MetalType::Bronze,
                        MetalType::Bronze,
                    ])
                    .unwrap();
                Material::Metal(
                    metal,
                    match state {
                        worldgen::terrain::State::Liquid => {
                            MetalState::Molten
                        }
                        _ => MetalState::Ore,
                    },
                )
            } else {
                Material::Stone(mat)
            }
        }
        _ => {
            println!("Mined stone");
            Material::Stone(mat)
        }
    }
}

// creates random point between min_x, max_x, min_y, max_y
pub fn random_point(
    min_x: usize,
    max_x: usize,
    min_y: usize,
    max_y: usize,
) -> Point2D {
    let mut trng = rand::thread_rng();
    (trng.gen_range(min_x, max_x), trng.gen_range(min_y, max_y))
}

pub fn points_in_rect(map: &WorldMap, r: Rect2D3D) -> Vec<Point3D> {
    let mut pnts = vec![];
    let ((sx1, sy1), (sx2, sy2), z) = r;
    let yr = if sy1 > sy2 {
        (sy2..(sy1 + 1)).rev().collect::<Vec<usize>>()
    } else {
        (sy1..(sy2 + 1)).collect::<Vec<usize>>()
    };
    let xr = if sx1 > sx2 {
        (sx2..(sx1 + 1)).rev().collect::<Vec<usize>>()
    } else {
        (sx1..(sx2 + 1)).collect::<Vec<usize>>()
    };
    for x in xr {
        for y in &yr {
            pnts.push((x, *y, z));
        }
    }
    pnts
}

pub fn display_terrain(
    gl: &mut G2d,
    c: &Context,
    args: &RenderArgs,
    (x, y, z): Screenable3D,
    tile: &G2dTexture,
) {
    let (iso_x, iso_y) = twod_to_iso((x, y));
    let transform = c
        .transform
        .trans(iso_x as f64, iso_y as f64 - z as f64 * 64.0)
        .zoom(2.0);
    image(tile, transform, gl);
}

pub fn display_image(
    gl: &mut G2d,
    c: &Context,
    (x, y): ScreenCoord,
    asset: &G2dTexture,
) {
    image(asset, c.transform.trans(x as f64, y as f64).zoom(2.0), gl);
}

pub fn unit_height(map: &WorldMap, pnt: Point2D) -> i32 {
    map.get(pnt.1)
        .and_then(|row| row.get(pnt.0))
        .map_or(0, |unit| {
            let arc = unit.tiles.clone();
            let ut = arc.read().unwrap();
            let mut h = 0;
            let ul = ut.len();
            for i in 0..ul {
                if !ut[i].solid() {
                    return h;
                } else {
                    h += 1;
                }
            }
            h
        })
}

lazy_static! {
    static ref ANIMATIONS: Mutex<HashMap<String, (u64, usize)>> =
        Mutex::new(HashMap::with_capacity(8750000));
}

pub fn texture_get(
    texture_map: &std::collections::HashMap<String, Vec<G2dTexture>>,
    s: &str,
    id: usize,
    speed: f64,
) -> G2dTexture {
    // TODO: Make pairing function work for individual animations per tile, if needed
    let time = time::get_millisecond_time();
    let mut anims = ANIMATIONS.lock().unwrap();
    let entry = anims.entry(s.to_string()).or_insert((time, 0));

    if let Some(frames) = texture_map.get(s) {
        if time.checked_sub(entry.0).unwrap_or(0) as f64
            > speed * 100.0
        {
            entry.1 = (entry.1 + 1) % frames.len();
            entry.0 = time;
        }

        frames[entry.1].clone()
    } else {
        panic!("Unknown texture: {:?}", s);
    }
}

pub fn line_height(size: i32) -> f64 {
    if size > 15 {
        (size as f64) / 1.8
    } else {
        size as f64 + (size as f64) * 0.5
    }
}

use std::str;
pub fn wrap(text: String, width: f64, size: i32) -> Vec<String> {
    let lines = text.split("\n").collect::<Vec<_>>();
    let wrap_chars = ((width - 80.0) / (size as f64 / 1.5)) as usize;
    lines
        .iter()
        .flat_map(|line| {
            line.as_bytes()
                .chunks(wrap_chars)
                .map(str::from_utf8)
                .collect::<Result<Vec<&str>, _>>()
                .unwrap()
        })
        .map(|x| x.to_string())
        .collect::<Vec<String>>()
}

pub fn get_delta_place(
    p: Point3D,
    np: Point3D,
    pp: Point3D,
    frame: usize,
    frame_last_action: usize,
    level: f64,
    action_speed: usize,
    pos: Screenable3D,
    current_delta: (i32, i32),
    last_delta: (i32, i32),
) -> Point2D {
    let place = twod_to_iso((p.0, p.1));
    let place_z = (p.2 as f64 - level) * 64.0;

    let next_place = twod_to_iso((np.0, np.1));
    let next_z = (np.2 as f64 - level) * 64.0;

    let prev_place = twod_to_iso((pp.0, pp.1));
    let prev_z = (pp.2 as f64 - level) * 64.0;
    if next_place != place {
        let mov_percent = (frame as f64 - frame_last_action as f64)
            / (action_speed as f64)
            - 0.5;
        if mov_percent != std::f64::NAN {
            let delta_x;
            let delta_y;
            if mov_percent > 0.0 {
                delta_x = ((place.0 as f64 - next_place.0 as f64)
                    * mov_percent)
                    .ceil();
                delta_y = ((place.1 as f64 - next_place.1 as f64)
                    * mov_percent)
                    .ceil();
            /*delta_z = ((place_z as f64 - next_z as f64) *
                 mov_percent)
            .ceil();*/
            } else {
                delta_x = ((prev_place.0 as f64 - place.0 as f64)
                    * mov_percent
                    * 2.0)
                    .ceil();
                delta_y = ((prev_place.1 as f64 - place.1 as f64)
                    * mov_percent
                    * 2.0)
                    .ceil();
                /*delta_z = ((prev_z as f64 - place_z as f64) *
                     mov_percent *
                     2.0)
                .ceil();*/
            }

            (
                (place.0 as f64 - delta_x) as usize,
                (place.1 as f64
                    - delta_y
                    - if delta_x.abs() as usize > 32
                        || delta_y.abs() as usize > 16
                    {
                        next_z
                    } else {
                        place_z
                    }) as usize,
            )
        } else {
            (
                place.0 as usize,
                (place.1 as isize - pos.2 as isize * 64) as usize,
            )
        }
    } else {
        (
            place.0 as usize,
            (place.1 as isize - pos.2 as isize * 64) as usize,
        )
    }
}

pub fn direction(delta: (i32, i32)) -> &'static str {
    match delta {
        (1, -1) => "fl",
        (1, _) => "bl",

        (-1, -1) => "fr",
        (-1, _) => "br",

        (0, -1) => "fl",
        (0, 1) => "bl",

        _ => "fl",
    }
}
