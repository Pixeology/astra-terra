/*
 * This file has two parts: The first part is a generalized voxel world
 * serialization and management library, plus some tools for dealing with it
 * (Dijikstra Maps, right now). The second part is an interface to the Dijikstra
 * map implementation (which transparently uses the voxel implementation in the
 * background) that presents a simple pathfinding utility, which is what the
 * rest of this repo actually needs.
 */

use crate::physics::PhysicsActor;
use std::cmp::Ordering;

use std::hash::{Hash, Hasher};

use std::collections::hash_map::DefaultHasher;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;

use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;

use crate::utils::*;
use crate::worldgen::*;

const CHUNK_X_SIZE: usize = 16;
const CHUNK_Y_SIZE: usize = 16;
const CHUNK_Z_SIZE: usize = 16;
const CHUNK_VOLUME: usize =
    CHUNK_X_SIZE * CHUNK_Y_SIZE * CHUNK_Z_SIZE;
const DATA_SEGMENT_SIZE: usize = 256;

/// 256 bytes of data, to be used for any purpose
#[derive(Copy, Clone)]
pub struct DataSegment {
    data: [u8; DATA_SEGMENT_SIZE],
}

///A point in 3D space
#[derive(Copy, Clone, Default, Hash, PartialEq, Eq)]
pub struct Location3D {
    x: usize,
    y: usize,
    z: usize,
}

/// The location of a Chunk in relation to the world
pub type ChunkLocation = Location3D;

/// The location of a single voxel in relation to the world
pub type GlobalLocation = Location3D;

/// The location of a single voxel in relation to its chunk
pub type VoxelLocation = Location3D;

/// Represents a collection of voxels that may be loaded and unloaded together
#[derive(Clone)]
struct Chunk<T> {
    /// the voxels contained within this chunk, it's a cube
    voxels: [T; CHUNK_VOLUME],
    /// Extra data
    extra_data: Option<DataSegment>,
}

/// Represents many chunks that form a world
#[derive(Clone)]
struct Dimension<T> {
    /// The chunks that are actually loaded
    loaded_chunks: HashMap<ChunkLocation, Chunk<T>>,
    /// List of all chunk locations that are specially defined
    all_chunk_locations: HashSet<ChunkLocation>,
    ///the folder where the dimension will be saved
    disk_cache: Option<String>,
}

///Represents a particular section of a dimension
#[derive(Clone)]
pub struct Volume<T> {
    start_location: GlobalLocation,
    end_location: GlobalLocation,
    x_size: usize,
    y_size: usize,
    z_size: usize,
    voxels: Vec<T>,
}

impl Location3D {
    fn new(x: usize, y: usize, z: usize) -> Location3D {
        Location3D { x: x, y: y, z: z }
    }
    fn as_point3d(&self) -> math::Point3D {
        (self.x, self.y, self.z)
    }
}

impl std::ops::Add for Location3D {
    type Output = Location3D;

    fn add(self, other: Location3D) -> Location3D {
        Location3D {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl std::ops::Sub for Location3D {
    type Output = Location3D;

    fn sub(self, other: Location3D) -> Location3D {
        Location3D {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

/// TODO please flesh out the scope struct. It represents an arbitrary 3d
/// portion of the world that is backed up by chunks, kinda like a world

impl DataSegment {
    fn new() -> DataSegment {
        DataSegment {
            data: [0; DATA_SEGMENT_SIZE],
        }
    }

    /// Creates data segment from string, truncating it if it is too long
    fn from(string: &str) -> DataSegment {
        let mut data = DataSegment::new();
        for i in 0..(std::cmp::min(string.len(), data.data.len()) - 1)
        {
            data.data[i] = string.as_bytes()[i];
        }
        data
    }
}

impl<T: Copy + Default> Chunk<T> {
    fn new() -> Chunk<T> {
        Chunk::from_value(Default::default())
    }

    /// a new chunk initialized to all value
    fn from_value(value: T) -> Chunk<T> {
        Chunk::from_value_with_extra_data(value, None)
    }

    fn from_value_with_extra_data(
        value: T,
        extra_data: Option<DataSegment>,
    ) -> Chunk<T> {
        Chunk {
            voxels: [value; CHUNK_VOLUME],
            extra_data: extra_data,
        }
    }

    fn from_buf_reader(stream: &mut BufReader<File>) -> Chunk<T> {
        let mut chunk = Chunk::new();
        chunk.read(stream);
        chunk
    }

    fn get_index(location: VoxelLocation) -> usize {
        (location.z as usize) * CHUNK_X_SIZE * CHUNK_Y_SIZE
            + (location.y as usize) * CHUNK_X_SIZE
            + (location.x as usize)
    }

    fn get(&self, location: VoxelLocation) -> T {
        self.voxels[Self::get_index(location)]
    }

    fn set(&mut self, location: VoxelLocation, value: T) -> () {
        self.voxels[Self::get_index(location)] = value;
    }

    /// Reads from saved file
    fn read(&mut self, stream: &mut BufReader<File>) -> () {
        //TODO implement serde serialization
    }

    /// writes to file
    fn write(stream: &mut BufWriter<File>, chunk: Chunk<T>) -> () {
        //TODO implement serde serialization
    }
}

impl<T: Copy + Default> Dimension<T> {
    fn new() -> Dimension<T> {
        Dimension {
            loaded_chunks: HashMap::new(),
            all_chunk_locations: HashSet::new(),
            disk_cache: None, //TODO please set disk cache and figure this out
        }
    }

    /// Adds a chunk to the location
    fn add_chunk_in_place(
        &mut self,
        location: ChunkLocation,
        chunk: Chunk<T>,
    ) -> () {
        self.all_chunk_locations.insert(location);
        self.loaded_chunks.insert(location, chunk);
    }

    /// Remove chunk from location, if it exists
    fn remove_chunk_in_place(
        &mut self,
        location: ChunkLocation,
    ) -> () {
        self.all_chunk_locations.remove(&location.clone());
        self.loaded_chunks.remove(&location.clone());
    }

    /// Gets a chunk, loading it if unavailable
    fn get_chunk(&mut self, location: ChunkLocation) -> &Chunk<T> {
        if !self.chunk_defined(location) {
            panic!("chunk undefined");
        } else if !self.chunk_loaded(location) {
            self.load_chunk(location);
        }
        self.loaded_chunks.get(&location).unwrap()
    }

    /// If a chunk has been loaded
    fn chunk_loaded(&self, location: ChunkLocation) -> bool {
        self.loaded_chunks.contains_key(&location)
    }

    /// If a chunk has been defined to exist
    fn chunk_defined(&self, location: ChunkLocation) -> bool {
        self.all_chunk_locations.contains(&location)
    }

    /// Loads chunk from disk
    fn load_chunk(&mut self, location: ChunkLocation) -> () {
        //TODO load chunk from disk cache
    }

    ///Syncs the disk version to the version in memory
    fn sync_chunk(&mut self, location: ChunkLocation) -> () {
        //TODO write chunk to disk
    }

    /// writes out all chunks to disk (sync all)
    fn flush(&mut self) -> () {
        //TODO implement flush, write out all chunks to disk
    }

    /// Gets the location of the chunk where this voxel lies
    fn get_chunk_location(location: GlobalLocation) -> ChunkLocation {
        ChunkLocation {
            x: location.x / (CHUNK_X_SIZE as usize),
            y: location.y / (CHUNK_Y_SIZE as usize),
            z: location.z / (CHUNK_Z_SIZE as usize),
        }
    }

    /// Gets the location of the voxel in the chunk where this global location lies
    fn get_voxel_location(location: GlobalLocation) -> VoxelLocation {
        VoxelLocation {
            x: location.x % (CHUNK_X_SIZE as usize),
            y: location.y % (CHUNK_Y_SIZE as usize),
            z: location.z % (CHUNK_Z_SIZE as usize),
        }
    }

    /// gets voxel at location if available. It is preffered to use get_Volume for better
    /// performance
    fn get_voxel(&mut self, location: GlobalLocation) -> T {
        let chunk =
            self.get_chunk(Self::get_chunk_location(location));
        chunk.get(Self::get_voxel_location(location))
    }
}

impl<T: Copy> Volume<T> {
    pub fn new(
        start_location: GlobalLocation,
        end_location: GlobalLocation,
        value: T,
    ) -> Volume<T> {
        let x_size = end_location.x - start_location.x;
        let y_size = end_location.y - start_location.y;
        let z_size = end_location.z - start_location.z;
        Volume {
            x_size: x_size,
            y_size: y_size,
            z_size: z_size,
            start_location: start_location,
            end_location: end_location,
            voxels: vec![value; (x_size * y_size * z_size) as usize],
        }
    }

    pub fn get_index(&self, location: GlobalLocation) -> usize {
        (location.z * self.x_size * self.y_size
            + location.y * self.x_size
            + location.x) as usize
    }

    pub fn get_location(&self, index: usize) -> GlobalLocation {
        Location3D {
            z: (index as usize) / (self.x_size * self.y_size),
            y: (index as usize) % (self.x_size * self.y_size),
            x: (index as usize) % (self.y_size),
        }
    }

    pub fn within_bounds(&self, location: GlobalLocation) -> bool {
        self.get_index(location) < self.voxels.len()
    }

    pub fn get(&self, location: GlobalLocation) -> T {
        self.voxels[self.get_index(location)]
    }

    pub fn set(&mut self, location: GlobalLocation, value: T) -> () {
        let loc = self.get_index(location);
        self.voxels[loc] = value;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////implementation///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

struct VoxelType {
    id: usize,
    name: String,
    solid: bool,
}

#[derive(Clone, Copy, Default)]
struct Voxel {
    id: usize,
    extra_data: Option<DataSegment>,
}

impl Voxel {
    fn get_type(&self) -> VoxelType {
        match self.id {
            0 => VoxelType {
                id: 0,
                name: String::from("unknown"),
                solid: true,
            },
            1 => VoxelType {
                id: 1,
                name: String::from("air"),
                solid: false,
            },
            2 => VoxelType {
                id: 2,
                name: String::from("water"),
                solid: false,
            },
            3 => VoxelType {
                id: 3,
                name: String::from("stone"),
                solid: true,
            },
            _ => panic!("material id undefined"),
        }
    }
}

#[derive(Clone, Copy, Default, Hash, Eq, PartialEq)]
struct Node {
    location: GlobalLocation,
    cost: usize,
}

impl Node {
    fn calculate_hash(&self) -> u64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish()
    }
}

// The priority queue depends on `Ord`.
// Explicitly implement the trait so the queue becomes a min-heap
// instead of a max-heap.
impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we order randomly (by hash)
        other.cost.cmp(&self.cost).then_with(|| {
            self.calculate_hash().cmp(&other.calculate_hash())
        })
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// If the current location can be travelled by a droid
pub fn is_traversable(
    map: &Volume<terrain::Tile>,
    location: GlobalLocation,
) -> bool {
    let location_underneath =
        GlobalLocation::new(location.x, location.y, location.z - 1);
    //check that the current location and the location underneath are defined
    map.within_bounds(location) && map.within_bounds(location_underneath)
     //check that current location is not solid
     && (!map.get(location).solid())
     //check that location down one must be solid
     && (map.get(location_underneath).solid())
}

/// Represents the handler to call during pathfinding to test if the path can go
/// into a certain location.
/// **Explanation**:
/// Necissary because the type of the location is unknown (generic) and so the
/// lambda passed in needs to be generic, but Rust monomorphizes types in
/// function declarations at compile time which makes it unable to support
/// Higher-Kinded Types (passing around types as if they were functions, with
/// type parameters to be filled in later). This means that any generic lambda
/// or trait object can't be represented as a method/function argument. So we're
/// doing the Java-dance and using a struct (class in Java's case) as a wrapper
/// to a single method. In Rust, since we use interfaces, not inheritence, and a
/// trait object wouldn't work (so no implementing the struct at the call-site)
/// we're using a property.
pub struct TraversableHandler<T> {
    // The handler property
    pub handler: Box<dyn Fn(&Volume<T>, GlobalLocation) -> bool>,
}

impl<T> TraversableHandler<T> {
    // A pass-thru function, for use-site convenience.
    fn can_pass(&self, vol: &Volume<T>, loc: GlobalLocation) -> bool {
        (self.handler)(vol, loc)
    }

    // No constructor is possible here, since it would run into the same problem
    // of putting a generic lambda in a method/function's type signature.
}

fn get_djikstra_map<T>(
    map: &Volume<T>,
    weights: Vec<(GlobalLocation, usize)>,
    traversable: TraversableHandler<T>,
) -> Volume<usize> {
    // The nodes that are on the exploring front of the djikstra map
    let mut frontier: BinaryHeap<Node> = BinaryHeap::new();
    // The nodes that used to be on the exploring front
    let mut visited: HashSet<Node> = HashSet::new();

    // insert original weights into node tree
    for (location, weight) in weights.iter() {
        frontier.push(Node {
            location: location.clone(),
            cost: weight.clone(),
        });
    }

    //while there are still pending nodes
    while frontier.len() > 0 {
        let current_node = frontier.pop().unwrap();
        visited.insert(current_node);
        for location in [
            current_node.location - GlobalLocation::new(1, 0, 0),
            current_node.location + GlobalLocation::new(1, 0, 0),
            current_node.location - GlobalLocation::new(0, 1, 0),
            current_node.location + GlobalLocation::new(0, 1, 0),
            current_node.location - GlobalLocation::new(0, 0, 1),
            current_node.location + GlobalLocation::new(0, 0, 1),
        ]
        .iter()
        {
            //if it can be traversed,
            if traversable.can_pass(map, location.clone())
            //if it has not been visited
                && !visited.iter().find(|x| &x.location == location).is_some()
            {
                // add it to the priority queue
                frontier.push(Node {
                    location: location.clone(),
                    cost: current_node.cost + 1,
                });
            }
        }
    }

    // Create djikstra map
    let mut potential_map: Volume<usize> = Volume::new(
        map.start_location,
        map.end_location,
        usize::max_value(),
    );
    //overwrite map with nodes
    for node in visited.iter() {
        potential_map.set(node.location, node.cost);
    }
    potential_map
}

//Compatibility

pub fn map_to_volume(map: &WorldMap) -> Volume<terrain::Tile> {
    let x_size = map.len() as usize;
    let y_size = map[0].len() as usize;
    // TODO: Make a reasonable number, or something, since *there is no max Z size*
    let z_size = 1000;

    let mut vol: Volume<terrain::Tile> = Volume::new(
        GlobalLocation::new(0, 0, 0),
        GlobalLocation::new(x_size, y_size, z_size),
        terrain::Tile::Empty(None),
    );
    for x in 0..x_size {
        for y in 0..y_size {
            let arc = map[x][y].clone();
            let tile_vec = arc.tiles.read().unwrap();
            for (i, z) in tile_vec.iter().enumerate() {
                vol.set(GlobalLocation::new(x, y, i), *z);
            }
        }
    }
    vol
}

pub fn find_path(
    world_map: &WorldMap,
    source: math::Point3D,
    destination: math::Point3D,
    traversable: Option<TraversableHandler<terrain::Tile>>,
) -> Option<Vec<math::Point3D>> {
    let dest = GlobalLocation::new(
        destination.0 as usize,
        destination.1 as usize,
        destination.2 as usize,
    );
    let volume = map_to_volume(world_map);
    let dmap = get_djikstra_map(
        &volume,
        vec![(dest, 0)],
        traversable.unwrap_or(TraversableHandler {
            handler: Box::new(is_traversable),
        }),
    );

    let mut path: Vec<math::Point3D> = Vec::new();
    let current_loc =
        GlobalLocation::new(source.0, source.1, source.2);
    while current_loc.as_point3d() != dest.as_point3d() {
        path.push(current_loc.as_point3d());
        if let Some((lowest_loc, _value)) = [
            current_loc - GlobalLocation::new(1, 0, 0),
            current_loc + GlobalLocation::new(1, 0, 0),
            current_loc - GlobalLocation::new(0, 1, 0),
            current_loc + GlobalLocation::new(0, 1, 0),
            current_loc - GlobalLocation::new(0, 0, 1),
            current_loc + GlobalLocation::new(0, 0, 1),
        ]
        .iter()
        .map(|l| (l, dmap.get(*l)))
        .min_by_key(|(_, x)| *x)
        {
            path.push(lowest_loc.as_point3d());
        }
    }
    path.push(destination);
    Some(path)
}
