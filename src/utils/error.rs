use crate::utils::constants::PROGRAM_NAME;

pub fn log(warn_level:&str, error_message:&str) -> () {
    println!("{}: {}: {}",PROGRAM_NAME, warn_level, error_message);
}