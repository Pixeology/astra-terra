//Name of this program
pub const PROGRAM_NAME: &'static str = "AstraTerra";
pub const ERROR_LEVEL_DEBUG: &'static str = "Debug";
pub const ERROR_LEVEL_INFO: &'static str = "Info";
pub const ERROR_LEVEL_WARNING: &'static str = "Warning";
pub const ERROR_LEVEL_ERROR: &'static str = "Error";
pub const ERROR_LEVEL_FATAL: &'static str = "Fatal";

pub const ASSET_PATH_TILE: &'static str = "assets/tiles";