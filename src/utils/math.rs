use std::cmp;

pub type ScreenCoord = (i32, i32);
pub type Screenable3D = (usize, usize, i32);
pub type Point2D = (usize, usize);
pub type Point3D = (usize, usize, usize);
pub type Rect2D3D = ((usize, usize), (usize, usize), usize);
pub type Rect2D = ((usize, usize), (usize, usize));

static D1: isize = 1;
static D2: isize = 2;
static D3: isize = 2;

//returns the 3d distance
pub fn distance_3d(
    (x1, y1, z1): Point3D,
    (x2, y2, z2): Point3D,
) -> isize {
    let x1 = x1 as isize;
    let y1 = y1 as isize;
    let z1 = z1 as isize;
    let x2 = x2 as isize;
    let y2 = y2 as isize;
    let z2 = z2 as isize;

    let dx = (x1 - x2).abs();
    let dy = (y1 - y2).abs();
    let dz = (z1 - z2).abs();
    let dmin = std::cmp::min(dx, std::cmp::min(dy, dz));
    let dmax = std::cmp::max(dx, std::cmp::max(dy, dz));
    let dmid = dx + dy + dz - dmin - dmax;
    (D3 - D2) * dmin + (D2 - D1) * dmid + D1 * dmax
}

/// Returns the coordinates adjacent to the given point.
pub fn strict_adjacent(
    (x, y): (usize, usize),
) -> Vec<(usize, usize)> {
    vec![
        (x + 1, y),
        (x.checked_sub(1).unwrap_or(0), y),
        (x, y.checked_sub(1).unwrap_or(0)),
        (x, y + 1),
    ]
    .into_iter()
    .filter(|z| *z != (x, y))
    .collect()
}

/// Returns the coordinates adjacent to the given point, and the point itself.
pub fn weak_adjacent(p: Point2D) -> Vec<Point2D> {
    let mut v = strict_adjacent(p);
    v.push(p);
    v
}

/// Returns the 3d coordinates adjacent to the given 3d point
pub fn strict_3d_adjacent((x, y, z): Point3D) -> Vec<Point3D> {
    let mut vec = strict_adjacent((x, y))
        .into_iter()
        .map(|(x, y)| (x, y, z))
        .collect::<Vec<_>>();
    vec.append(&mut vec![
        (x + 1, y, z),
        (x.checked_sub(1).unwrap_or(0), y, z),
        (x, y.checked_sub(1).unwrap_or(0), z),
        (x, y + 1, z),
        (x, y, z + 1),
        (x, y, z.checked_sub(1).unwrap_or(0)),
        (x + 1, y, z + 1),
        (x + 1, y, z.checked_sub(1).unwrap_or(0)),
        (x, y + 1, z + 1),
        (x, y + 1, z.checked_sub(1).unwrap_or(0)),
        (x.checked_sub(1).unwrap_or(0), y, z + 1),
        (
            x.checked_sub(1).unwrap_or(0),
            y,
            z.checked_sub(1).unwrap_or(0),
        ),
        (x, y.checked_sub(1).unwrap_or(0), z + 1),
        (
            x,
            y.checked_sub(1).unwrap_or(0),
            z.checked_sub(1).unwrap_or(0),
        ),
    ]);
    vec.into_iter().filter(|p| *p != (x, y, z)).collect()
}
pub fn distance((x1, y1): Point2D, (x2, y2): Point2D) -> isize {
    let x1 = x1 as isize;
    let y1 = y1 as isize;
    let x2 = x2 as isize;
    let y2 = y2 as isize;

    let dx = (x1 - x2).abs();
    let dy = (y1 - y2).abs();
    D1 * (dx + dy) + (D2 - 2 * D1) * std::cmp::min(dx, dy)
}

/// Make sure a value is between two others. Values are required to be Ord.
pub fn clamp<T: Ord>(value: T, max: T, min: T) -> T {
    cmp::min(max, cmp::max(min, value))
}

///determines whether a 3d point is inside of a 3d rectangle
pub fn inside_rect(p: Point3D, rect: Rect2D3D) -> bool {
    p.2 <= rect.2
        && p.0 <= (rect.1).0
        && p.0 >= (rect.0).0
        && p.1 <= (rect.1).1
        && p.1 >= (rect.0).1
}

const TILE_WIDTH: i32  = 64;
const TILE_HEIGHT: i32 = 32;

pub fn iso_to_2d(spt: ScreenCoord) -> Point2D {
    let x = ((spt.0 / TILE_WIDTH + spt.1 / TILE_HEIGHT) / 2) as usize;
    let y = ((spt.1 / TILE_HEIGHT - (spt.0 / TILE_WIDTH)) / 2) as usize;
    (x, y)
}

pub fn iso_to_2d_cursor(spt: ScreenCoord) -> Point2D {
    let w = TILE_WIDTH; let h = TILE_HEIGHT;
    let spt = (spt.0 - w / 2, spt.1 + 20);
    let x = ((spt.0 / w + spt.1 / h) / 2) as usize;
    let y = ((spt.1 / h - (spt.0 / w)) / 2) as usize;
    (x, y)
}

pub fn twod_to_iso(pt: Point2D) -> ScreenCoord {
    let x = (pt.0 as i32 - pt.1 as i32) * TILE_WIDTH;
    let y = (pt.0 as i32 + pt.1 as i32 ) * TILE_HEIGHT;
    (x, y)
}

pub fn rot_switch(
    (x, y): Point2D,
    xswitch: bool,
    yswitch: bool,
    size: Point2D,
) -> Point2D {
    if yswitch {
        (x, (size.1).checked_sub(y).unwrap_or(0))
    } else if xswitch {
        ((size.0).checked_sub(x).unwrap_or(0), y)
    } else if yswitch && xswitch {
        let (x, y) = ((size.0).checked_sub(x).unwrap_or(0), y);
        (x, (size.1).checked_sub(y).unwrap_or(0))
    } else {
        (x, y)
    }
}
