extern crate colored;
extern crate rand;
extern crate tcod;
extern crate tcod_sys;

use crate::GameScreen;
use crate::MAP_SIZE;
use std;
use std::cell::RefCell;
use std::collections::HashMap;
use std::ops::{Index, Range};
use std::sync::{Arc, RwLock};

use self::tcod::noise::{Noise, NoiseType};
use self::tcod::random;

pub mod terrain;
use self::rand::Rng;
use self::terrain::*;

use crate::life::{Living, Order, WorldDiff};
use crate::physics::PhysicsActor;
use crate::time::{Calendar, Clock, Time};
use crate::utils;
use crate::utils::math::{strict_adjacent, Point2D, Point3D};

use std::sync::mpsc::*;

/// A general method for dealing with generating random hills of a limited size,
/// position, and height.
fn random_hill_operation<F>(
    heightmap: *mut tcod_sys::TCOD_heightmap_t,
    (hmw, hmh): Point2D,
    num_hills: i32,
    base_radius: f32,
    radius: f32,
    height: f32,
    rndn: tcod_sys::TCOD_random_t,
    operation: &F,
) where
    F: Fn(*mut tcod_sys::TCOD_heightmap_t, f32, f32, f32, f32),
{
    unsafe {
        for _ in 0..num_hills {
            let radius = tcod_sys::TCOD_random_get_float(
                rndn,
                base_radius * (1.0 - radius),
                base_radius * (1.0 + radius),
            );
            let xh = tcod_sys::TCOD_random_get_int(
                rndn,
                0,
                hmw as i32 - 1,
            );
            let yh = tcod_sys::TCOD_random_get_int(
                rndn,
                0,
                hmh as i32 - 1,
            );
            operation(
                heightmap, xh as f32, yh as f32, radius, height,
            );
        }
    }
}

/// Extrudes random hills.
fn add_random_hills(
    hm: *mut tcod_sys::TCOD_heightmap_t,
    sz: Point2D,
    nh: i32,
    br: f32,
    r: f32,
    h: f32,
    n: tcod_sys::TCOD_random_t,
) {
    random_hill_operation(
        hm,
        sz,
        nh,
        br,
        r,
        h,
        n,
        &|a, b, c, d, e| unsafe {
            tcod_sys::TCOD_heightmap_add_hill(a, b, c, d, e)
        },
    );
}

/// Digs random hills.
fn dig_random_hills(
    hm: *mut tcod_sys::TCOD_heightmap_t,
    sz: Point2D,
    nh: i32,
    br: f32,
    r: f32,
    h: f32,
    n: tcod_sys::TCOD_random_t,
) {
    random_hill_operation(
        hm,
        sz,
        nh,
        br,
        r,
        h,
        n,
        &|a, b, c, d, e| unsafe {
            tcod_sys::TCOD_heightmap_dig_hill(a, b, c, d, e)
        },
    );
}

/// Generate biome groupings using a heightmap
struct GroupingsGen {
    hm: *mut tcod_sys::TCOD_heightmap_t,
}

impl GroupingsGen {
    /// Initialize and generate biomes
    pub fn new(
        seed: u32,
        max: i32,
        min: i32,
        sx: usize,
        sy: usize,
    ) -> Self {
        unsafe {
            let heightmap =
                tcod_sys::TCOD_heightmap_new(sx as i32, sy as i32);
            let rndn = tcod_sys::TCOD_random_new_from_seed(
                tcod_sys::TCOD_RNG_MT,
                seed,
            );
            add_random_hills(
                heightmap,
                (sx, sy),
                300,
                20.0,
                0.7,
                0.3,
                rndn,
            );
            dig_random_hills(
                heightmap,
                (sx, sy),
                100,
                20.0,
                0.6,
                0.3,
                rndn,
            );
            tcod_sys::TCOD_heightmap_normalize(
                heightmap, max as f32, min as f32,
            );
            GroupingsGen { hm: heightmap }
        }
    }

    pub fn get(&self, (x, y): Point2D) -> f32 {
        unsafe {
            tcod_sys::TCOD_heightmap_get_value(
                self.hm, x as i32, y as i32,
            )
        }
    }
}

/// Returns the Some() of the restricted version of a Tile if it can
/// be restricted, if not, returns None.
fn restricted_from_tile(tile: Tile) -> Option<RestrictedTile> {
    match tile {
        Tile::Stone(a, b) => Some(RestrictedTile::Stone(a, b)),
        Tile::Vegetation(a, b, c) => {
            Some(RestrictedTile::Vegetation(a, b, c))
        }
        _ => None,
    }
}

/// A 1m x 1m cross section of the layered world (so its a stack of tiles), including a ref to the
/// biome it is part of.
#[derive(Clone)]
pub struct Unit {
    pub biome: Option<Biome>,
    pub tiles: Arc<RwLock<Vec<Tile>>>,
}

/// A mapping of a unique string (based on the type of thing animated)
/// to its FrameAssoc. This is out of date, don't use this
pub type Frames = HashMap<String, Vec<usize>>;

/// A type for the world's map. Meant to be multithreaded.
pub type WorldMap = Vec<Vec<Unit>>;

/// A type alias for the biome map.
pub type BiomeMap = HashMap<String, RefCell<Vec<(usize, usize)>>>;

/// General properties for world generation:
/// 1. `mountainousness`: how high the average height of the terrain is as a percentage, and also how noisy the hills are
/// 2. `hilliness`: how often hills occur, and also how often vallies and lakes occur
/// 3. `temp_bias`: the bias of that planet, in degrees
/// 4. `raininess`: how much and how effective the erosion is (if we still used it) **depreciated**
/// 5. `sea_level`: how many layers high the sea is
/// 6. `biomes`: what biomes are allowed and the range they occur in out of roughly 200 'points' where points get less likely to happen the higher they go
#[derive(Clone, Debug)]
pub struct GenProps {
    pub mountainousness: f32,
    pub hilliness: usize,
    pub temp_bias: f32,
    pub raininess: f32,
    pub sea_level: f32,
    pub biomes: Vec<(BiomeType, usize)>,
}

/// Keeps track of the state of the physical map during gameplay, including:
/// * the animation state
/// * the map size and unit map
/// * the biomes (and where they are)
/// * the life forms
/// * the list of AIs
/// * the heightmap
/// * the vegetation and stone noise
/// Also deals with generating the world.
pub struct World {
    heightmap: *mut tcod_sys::TCOD_heightmap_t,
    stone_vein_noise: Noise,
    biome_groupings: GroupingsGen,
    seed: u32,
    pub map_size: Point2D,
    pub frames: Frames,
    pub biome_map: BiomeMap,
    pub map: WorldMap,
    pub life: Vec<Option<RefCell<Box<dyn Living>>>>,
    pub drones: Vec<usize>,
}

impl Index<usize> for World {
    type Output = Vec<Unit>;
    fn index(&self, location: usize) -> &Self::Output {
        &self.map[location]
    }
}

impl Index<Range<usize>> for World {
    type Output = [Vec<Unit>];
    fn index(&self, location: Range<usize>) -> &Self::Output {
        &self.map[location]
    }
}

const THRESHOLD: f32 = 0.6;
const VEG_THRESHOLD: f32 = 200.0;
const RAMP_THRESHOLD: f32 = 0.015;
const ANIMAL_COUNT: usize = 0;
const BEACH_THRESHOLD: f32 = 2.0;
const DESERT_THRESHOLD: f32 = 5.0;

impl World {
    /// Generates a new hightmap-based world map of the specified
    /// size. The generation order goes roughly thus:
    /// * generate noise for vegetation
    /// * create a heightmap
    /// * generate low-level noise for the heightmap
    /// * add the noise (through FBM) to the heightmap
    /// * run erosion simulation on heightmap
    /// * add randomly sized hills
    /// * dig randomly sized hills
    pub fn new(size: Point2D, seed: u32, props: &GenProps) -> World {
        println!("Generating world from seed {}", seed);
        let rng = random::Rng::new_with_seed(random::Algo::MT, seed);

        // Vegetation
        let mut world: World = World {
            map_size: size,
            heightmap: Self::generate_heightmap(size, seed, props),
            map: vec![],
            biome_groupings: GroupingsGen::new(
                seed, 100, 0, size.0, size.1,
            ),
            drones: vec![],
            biome_map: props
                .biomes
                .iter()
                .map(|x| x.0.stringified())
                .map(|x| (x.to_string(), RefCell::new(vec![])))
                .collect(),
            stone_vein_noise: Noise::init_with_dimensions(3)
                .lacunarity(0.43)
                .hurst(-0.9)
                .noise_type(NoiseType::Simplex)
                .random(rng)
                .init(),
            life: vec![],
            seed: seed,
            frames: [
                ("Water".to_string(), vec![16, 32, 33, 34, 35, 36]),
                (
                    "Watery".to_string(),
                    vec![118, 119, 120, 121, 122, 123],
                ),
            ]
            .iter()
            .cloned()
            .map(|(x, y)| (x, y))
            .collect(),
        };
        world.map = Self::map_from(size, &world, props);
        world
    }

    pub fn kill(&mut self, i: usize) {
        {
            let l = &self.life[i];
            if let Some(ref l) = l {
                let l = l.borrow();
                let pos = l.current_pos();
                let arc = self.map[pos.0][pos.1].tiles.clone();
                let mut unit = arc.write().unwrap();
                unit[pos.2] = Tile::Empty(None);
            }
        }
        self.life[i] = None;
    }

    /// Creates a vector of animals based on biome and height.
    pub fn generate_life(&mut self) -> Point2D {
        use crate::life::drone::*;
        let addons = vec![
            [
                Addon::MiningLaser,
                Addon::LithiumIonCore,
                Addon::PlateArmor,
            ],
            [
                Addon::MiningLaser,
                Addon::LithiumIonCore,
                Addon::PlateArmor,
            ],
            [
                Addon::MachineGun,
                Addon::LithiumIonCore,
                Addon::LifterArm,
            ],
            [
                Addon::RifleBoreGun,
                Addon::LithiumIonCore,
                Addon::LifterArm,
            ],
        ];
        let (bx, by) = (50, 50);
        for x in 0..4 {
            let px = bx + (x + 1) % 2;
            let py = by + std::cmp::max(0, x as i32 - 1) as usize;
            let pnt = (
                px,
                py,
                utils::misc::unit_height(&self.map, (px, py))
                    as usize,
            );
            let end_z = utils::misc::location_z(&self.map, (px, py));

            let unit = self.get((pnt.0, pnt.1)).unwrap();
            let arc = unit.tiles.clone();
            let mut ut = arc.write().unwrap();

            let drone = Drone::new(x, pnt, addons[x]);
            ut[pnt.2] = Tile::Empty(Some(drone.id));
            for z in (pnt.2)..end_z {
                ut[z] = Tile::Empty(None);
            }
            println!("Drone #{}", drone.id);
            println!(
                "{:?} total P.D.: {}",
                addons[x][0],
                addons[x]
                    .iter()
                    .map(|x| x.get_stats().power_draw)
                    .fold(0, |acc, x| acc + x)
            );
            self.life.push(Some(RefCell::new(
                Box::new(drone) as Box<dyn Living>
            )));
            self.drones.push(x);
        }
        (bx, by)
    }

    unsafe fn get_slope(&self, x: usize, y: usize) -> f32 {
        tcod_sys::TCOD_heightmap_get_slope(
            self.heightmap,
            x as i32,
            y as i32,
        )
    }

    /// Step 1 of map generation:
    /// Generate bedrock and mountains/hills from terrain info
    fn rock_from_terrain(
        ws: &World,
        (sw, sh): Point2D,
        props: &GenProps,
    ) -> WorldMap {
        (0..sh)
            .map(|y| {
                (0..sw)
                    .map(|x| {
                        let height = unsafe { ws.get_height(x, y) }
                            .ceil()
                            as usize;
                        Unit {
                            biome: None,
                            tiles: Arc::new(RwLock::new(
                                (0..std::cmp::max(height as i32, 1))
                                    .map(|h| {
                                        ws.rock_type(
                                            (x, y),
                                            h as isize,
                                            props,
                                        )
                                    })
                                    .collect(),
                            )),
                        }
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>()
    }

    /// Step 2 of map generation:
    /// Replace low rock with water of a similar depth.
    /// The sea level is raised when inland to allow for rivers and pools.
    fn water_from_low(world: &WorldMap, props: &GenProps) {
        // Use Simplex turbulence!
        for (y, row) in world.iter().enumerate() {
            for (x, unit) in row.iter().enumerate() {
                let arc = unit.tiles.clone();
                let mut ut = arc.write().unwrap();
                let unit_height = ut.len();
                if unit_height < props.sea_level as usize {
                    for depth in 0..(props.sea_level as usize) {
                        let wt = Tile::Water(
                            World::purity(),
                            State::Liquid,
                            (depth as i32 - unit_height as i32).abs(),
                        );
                        if depth < unit_height {
                            ut[depth] = wt;
                        } else {
                            ut.push(wt);
                        }
                    }
                } else if unit_height == props.sea_level as usize {
                    ut[unit_height - 1] = Tile::Stone(
                        StoneTypes::Soil(SoilTypes::Watery),
                        State::Solid,
                    );
                }
            }
        }
    }

    /// Step 3 of map generation:
    /// Generate biomes (temp, percipitation) from altitude and a noise
    fn biomes_from_height_and_noise(
        world_map: &mut WorldMap,
        world: &World,
        props: &GenProps,
    ) {
        fn split_out_mut<T>(
            xs: &mut Vec<T>,
            i: usize,
        ) -> (&[T], &mut T, &[T]) {
            assert!(i < xs.len());
            let (left, right) = xs.split_at_mut(i);
            let (x, right) = right.split_first_mut().unwrap();
            (left, x, right)
        }
        for y in 0..(MAP_SIZE.1) {
            let (up, row, down) = split_out_mut(world_map, y);
            for x in 0..(MAP_SIZE.0) {
                let (left, unit, right) = split_out_mut(row, x);
                let arc = unit.tiles.clone();
                let tiles = arc.write().unwrap();
                let (sh, n) = strict_adjacent((x, y))
                    .iter()
                    .map(|&(x, y)| unsafe {
                        world.get_height(x, y) as usize
                    })
                    .fold((0, 0), |(s, n), x| (s + x, n + 1));
                let noise = world.biome_groupings.get((x, y));

                let mut water_close = false;
                for wx in (-4)..4 {
                    for wy in (-4)..4 {
                        let px = wx + (x as i32);
                        let py = wy + (y as i32);
                        if px < MAP_SIZE.0 as i32
                            && px >= 0
                            && py < MAP_SIZE.1 as i32
                            && py >= 0
                            && !(wx == 0 && wy == 0)
                        {
                            let (px, py) = (px as usize, py as usize);
                            let unit = if py == y && px < x {
                                &left[px]
                            } else if py == y && px > x {
                                &right[px.checked_sub(x + 1).unwrap()]
                            } else if py < y {
                                &up[py][px]
                            } else {
                                &down[py.checked_sub(y + 1).unwrap()]
                                    [px]
                            };
                            let arc = unit.tiles.clone();
                            let tiles = arc.read().unwrap();
                            if tiles
                                .iter()
                                .rev()
                                .find(|x| {
                                    matches!(**x, Tile::Water(..))
                                })
                                .is_some()
                            {
                                water_close = true;
                                break;
                            }
                        }
                    }
                }

                let mut biome = World::biome_from_noise(
                    noise as i32,
                    (sh as i32 / n) as f32,
                    props,
                    water_close,
                );
                let mut height = 0;
                for h in 0..tiles.len() {
                    if !tiles[h].solid() {
                        break;
                    } else {
                        height += 1;
                    }
                }
                if height <= props.sea_level as i32 {
                    biome = Biome {
                        biome_type: BiomeType::Water,
                        temperature_day_f: biome.temperature_day_f,
                        temperature_night_f: biome
                            .temperature_night_f,
                        percipitation_chance: biome
                            .percipitation_chance,
                    };
                } else if height == props.sea_level as i32 + 1
                    && biome.temperature_day_f < 80.0
                {
                    biome = Biome {
                        biome_type: BiomeType::Beach,
                        temperature_day_f: biome.temperature_day_f,
                        temperature_night_f: biome
                            .temperature_night_f,
                        percipitation_chance: biome
                            .percipitation_chance,
                    };
                }
                let bn = &biome.biome_type.stringified();
                if let Some(bml) = world.biome_map.get(bn) {
                    bml.borrow_mut().push((x, y));
                }
                if unit.biome.is_none() {
                    unit.biome = Some(biome);
                }
            }
        }
    }

    /// Step 4 of map generation:
    /// Generate vegetation based on what survives where in the biomes, and height.
    fn vegetation_from_biomes(
        world: &WorldMap,
        seed: u32,
        props: &GenProps,
    ) {
        let vnoise = Noise::init_with_dimensions(2)
            .lacunarity(0.1)
            .hurst(1.0)
            .noise_type(NoiseType::Simplex)
            .random(random::Rng::new_with_seed(
                random::Algo::MT,
                seed,
            ))
            .init();

        let mut height_avg = 0.0;
        let mut max_height = 0;
        let mut min_height = 6;
        let mut height_count = 0;
        for (y, row) in world.iter().enumerate() {
            for (x, unit) in row.iter().enumerate() {
                let arc = unit.tiles.clone();
                let mut ut = arc.write().unwrap();
                if ut.len() > props.sea_level as usize
                    && unit.biome.unwrap().biome_type
                        != BiomeType::Water
                {
                    let vego = World::get_vegetation(
                        &vnoise,
                        (x, y),
                        unit.biome.unwrap(),
                    );
                    if let Some(veg) = vego {
                        if utils::misc::is_tree(veg) {
                            if let Tile::Vegetation(vt, ..) = veg {
                                if let TreePart::All(w, h) =
                                    vt.unwrap_to_tree_part().unwrap()
                                {
                                    let height = h;
                                    ut.push(Tile::Vegetation(
                                        vt.convert_to_base(),
                                        height as i32 + 2,
                                        State::Solid,
                                    ));
                                    if height > max_height {
                                        max_height = height;
                                    } else if height < min_height {
                                        min_height = height;
                                    }
                                    height_avg += height as f64;
                                    height_count += 1;
                                    for i in 0..height {
                                        ut.push(Tile::Vegetation(
                                            vt.convert_to_midsection(
                                            ),
                                            (height + 2 - i) as i32,
                                            State::Solid,
                                        ));
                                    }
                                    ut.push(Tile::Vegetation(
                                        vt.convert_to_trunk(),
                                        height as i32 + 1,
                                        State::Solid,
                                    ));
                                    ut.push(Tile::Vegetation(
                                        vt.convert_to_leaves(2),
                                        height as i32,
                                        State::Solid,
                                    ));
                                }
                            }
                        } else {
                            ut.push(veg);
                        }
                    }
                }
            }
        }
    }

    /// Step 5 of map generation:
    /// Generate the soil (and snow) based on plant and biome.
    fn add_soil(world: &WorldMap, seed: u32, props: &GenProps) {
        fn get_op<'a>(
            p: Point2D,
            world: &'a WorldMap,
        ) -> Option<&'a Unit> {
            let row = get!(world.get(p.1));
            let unit = get!(row.get(p.0));
            Some(unit)
        }

        for (y, row) in world.iter().enumerate() {
            for (x, unit) in row.iter().enumerate() {
                let arc = unit.tiles.clone();
                let mut t = arc.write().unwrap();
                let mut height = 0;
                for x in 0..t.len() {
                    if matches!(t[x], Tile::Empty(_)) {
                        break;
                    } else {
                        height += 1;
                    }
                }

                let bt = unit.biome.unwrap().biome_type;
                if height <= props.sea_level as usize
                    && bt == BiomeType::Desert
                {
                    t[height - 1] = Tile::Stone(
                        StoneTypes::Soil(SoilTypes::DesertSand),
                        State::Solid,
                    );
                }
                for h in (std::cmp::max(0, height - 1))..height {
                    if let Some(sc) = World::soil_choice(
                        h,
                        *t.get(h.checked_sub(0).unwrap_or(99999))
                            .unwrap_or(&Tile::Empty(None)),
                        &unit.biome.unwrap(),
                        seed,
                    ) {
                        if t[h].solid() && bt == BiomeType::Beach {
                            t[h] = Tile::Stone(
                                StoneTypes::Soil(SoilTypes::Sandy),
                                State::Solid,
                            );
                        } else if matches!(sc, SoilTypes::Snowy) {
                            t.push(Tile::Stone(
                                StoneTypes::Soil(sc),
                                State::Solid,
                            ));
                            t.push(Tile::Stone(
                                StoneTypes::Soil(sc),
                                State::Solid,
                            ));
                        } else {
                            t.push(Tile::Stone(
                                StoneTypes::Soil(sc),
                                State::Solid,
                            ));
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }

    /// Step 6 of world gen:
    /// Make sure there's empty space on top of every tile to walk on!
    fn add_walkspace(world: &WorldMap) {
        fn get_op<'a>(
            p: Point2D,
            world: &'a WorldMap,
        ) -> Option<&'a Unit> {
            let row = get!(world.get(p.1));
            let unit = get!(row.get(p.0));
            Some(unit)
        }

        for (y, row) in world.iter().enumerate() {
            for (x, unit) in row.iter().enumerate() {
                let arc = unit.tiles.clone();
                let mut t = arc.write().unwrap();
                while t.len() < 55 {
                    t.push(Tile::Empty(None));
                }
            }
        }
    }

    /// Generates a new unit map for World from the given (incomplete) World.
    /// The steps go as follows:
    /// * Generate bedrock and mountains/hills from terrain info
    /// * Replace low rock with water (for sea, pools)
    /// * Generate biomes (temp, percipitation) from altitude and a noise
    /// * Generate vegetation based on what survives where in the biomes
    /// * Generate the soil (and snow) based on plant and biome.
    fn map_from(
        size: Point2D,
        ws: &World,
        props: &GenProps,
    ) -> WorldMap {
        let mut map = World::rock_from_terrain(ws, size, props);
        World::water_from_low(&map, props);
        World::biomes_from_height_and_noise(&mut map, ws, props);
        World::add_soil(&mut map, ws.seed, props);
        World::vegetation_from_biomes(&mut map, ws.seed, props);
        World::add_walkspace(&mut map);
        map
    }
    /// Generates a new heightmap: this is all unsafe code, and uses the low-level FFI to libtcod 5.1.
    fn generate_heightmap(
        (sx, sy): Point2D,
        seed: u32,
        props: &GenProps,
    ) -> *mut tcod_sys::TCOD_heightmap_t {
        let heightmap = unsafe {
            tcod_sys::TCOD_heightmap_new(sx as i32, sy as i32)
        };

        unsafe {
            let rndn = tcod_sys::TCOD_random_new_from_seed(
                tcod_sys::TCOD_RNG_MT,
                seed,
            );
            /*let noise = tcod_sys::TCOD_noise_new(2, 0.7, 0.1, rndn);
            tcod_sys::TCOD_noise_set_type(
                noise,
                tcod_sys::TCOD_NOISE_SIMPLEX,
            );
            tcod_sys::TCOD_heightmap_add_fbm(
                heightmap,
                noise,
                props.mountainousness * (sx as f32) / 400.0,
                props.mountainousness * (sx as f32) / 400.0,
                0.0,
                0.0,
                10.0,
                1.0,
                4.0,
            );*/
            add_random_hills(
                heightmap,
                (sx, sy),
                (props.mountainousness * props.hilliness as f32 * 5.0)
                    as i32,
                16.0 * sx as f32 / 400.0,
                0.8,
                0.3,
                rndn,
            );
            dig_random_hills(
                heightmap,
                (sx, sy),
                props.hilliness as i32 * 5,
                16.0 * sx as f32 / 100.0,
                0.6,
                0.3,
                rndn,
            );
            tcod_sys::TCOD_heightmap_normalize(heightmap, 0.0, 15.0);
        };
        heightmap
    }

    /// Get the unit at the specified position. Returns an optional type.
    pub fn get_rot(
        &self,
        posn: Point2D,
        xswitch: bool,
        yswitch: bool,
    ) -> Option<Unit> {
        if self.located_inside(posn) {
            let (x, y) = utils::math::rot_switch(
                (posn.0 as usize, posn.1 as usize),
                xswitch,
                yswitch,
                self.map_size,
            );
            if self.located_inside((x, y)) {
                Some(self.map[y][x].clone())
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn get(&self, pos: Point2D) -> Option<Unit> {
        return self.get_rot(pos, false, false);
    }

    /// Test if the given point is on the World plane.
    pub fn located_inside(&self, pos: Point2D) -> bool {
        return pos.0 < self.map_size.0 && pos.1 < self.map_size.1;
    }

    /// For memory cleanup.
    pub unsafe fn delete_heightmap(&self) {
        tcod_sys::TCOD_heightmap_delete(self.heightmap);
    }

    /// Gets the height of the map at the current point.
    unsafe fn get_height(&self, x: usize, y: usize) -> f32 {
        tcod_sys::TCOD_heightmap_get_value(
            self.heightmap,
            x as i32,
            y as i32,
        ) * THRESHOLD
    }

    /// Gets the liquid purity.
    ///
    /// TODO: Make this based on proximity to dirt and plants, instead
    /// of being random.
    pub fn purity() -> LiquidPurity {
        *rand::thread_rng()
            .choose(&[
                LiquidPurity::Clean,
                LiquidPurity::Clear,
                LiquidPurity::Clear,
                LiquidPurity::Dirty,
                LiquidPurity::Dirty,
                LiquidPurity::Dirty,
                LiquidPurity::Dirty,
                LiquidPurity::Muddy,
                LiquidPurity::Muddy,
                LiquidPurity::Muddy,
                LiquidPurity::Murky,
                LiquidPurity::Pure,
                LiquidPurity::Sandy,
                LiquidPurity::Toxic,
            ])
            .unwrap()
    }

    pub fn len(&self) -> usize {
        match *self {
            World { ref map, .. } => map.len(),
        }
    }

    pub fn push(&mut self, value: Vec<Unit>) {
        match *self {
            World { ref mut map, .. } => map.push(value),
        }
    }

    fn biome_type_from_noise(
        point_val: i32,
        allowed_biomes: &Vec<(BiomeType, usize)>,
    ) -> BiomeType {
        let sum = allowed_biomes.iter().fold(0, |sum, a| sum + a.1);
        let mut start = 0;
        for (b, sz) in allowed_biomes {
            let sz = sz + sum / allowed_biomes.len();
            if point_val >= start && point_val <= start + sz as i32 {
                return b.clone();
            } else {
                start += sz as i32;
            }
        }
        return BiomeType::Forest;
    }

    /// Gets the biome at a certain height.
    pub fn biome_from_noise(
        point_val: i32,
        avg_height: f32,
        props: &GenProps,
        water_close: bool,
    ) -> Biome {
        let sea_level = props.sea_level;
        let temp_day = -11.71 * avg_height + 87.30 + props.temp_bias;
        Biome {
            biome_type: if avg_height <= sea_level && temp_day <= 80.0
            {
                BiomeType::Water
            } else if temp_day >= 105.0 && !water_close {
                BiomeType::Desert
            } else {
                World::biome_type_from_noise(point_val, &props.biomes)
            },
            temperature_day_f: temp_day,
            temperature_night_f: 200f32 - (avg_height),
            percipitation_chance: point_val as f32,
        }
    }

    pub fn is_water(x: Tile) -> bool {
        matches!(x, Tile::Water(..))
    }

    pub fn is_sand(x: Tile) -> bool {
        matches!(
            x,
            Tile::Stone(StoneTypes::Soil(SoilTypes::Sandy), _)
        )
    }

    pub fn is_veg(x: Tile) -> bool {
        matches!(x, Tile::Vegetation(..))
    }

    pub fn is_stone(x: Tile) -> bool {
        matches!(x, Tile::Stone(..))
    }

    /// Chooses a type of soil based on adjacency to certain features.
    fn soil_choice(
        height: usize,
        last_tile: Tile,
        biome: &Biome,
        seed: u32,
    ) -> Option<SoilTypes> {
        let mut trng = rand::thread_rng();
        if biome.temperature_day_f < 32.0 {
            if trng.gen_range(0, 100)
                < (100 - biome.temperature_day_f as i32)
            {
                Some(SoilTypes::Snowy)
            } else {
                Some(SoilTypes::Loamy)
            }
        } else if biome.biome_type == BiomeType::Beach {
            Some(SoilTypes::Sandy)
        } else if biome.biome_type == BiomeType::Desert {
            Some(SoilTypes::DesertSand)
        } else if biome.biome_type == BiomeType::Forest {
            if World::is_stone(last_tile) || height > 50 {
                Some(SoilTypes::Silty)
            } else {
                Some(SoilTypes::Loamy)
            }
        } else if biome.biome_type == BiomeType::Pasture {
            Some(SoilTypes::Peaty)
        } else if biome.biome_type == BiomeType::Water {
            None
        } else {
            Some(SoilTypes::Clay)
        }
    }

    fn random_lowlayer(rn: f32) -> StoneTypes {
        let choices = vec![
            StoneTypes::Igneous(IgneousRocks::Basalt),
            StoneTypes::Igneous(IgneousRocks::Basalt),
            StoneTypes::Igneous(IgneousRocks::Obsidian),
            StoneTypes::Igneous(IgneousRocks::Obsidian),
            StoneTypes::Metamorphic(MetamorphicRocks::Marble),
        ];
        let n = (rn / 30.0) * (choices.len() - 1) as f32;
        choices[n.abs() as usize]
    }

    fn random_midlayer(rn: f32) -> StoneTypes {
        // largely metamorphic, some igneous and sedimentary
        let choices = vec![
            StoneTypes::Igneous(IgneousRocks::Obsidian),
            StoneTypes::Igneous(IgneousRocks::Obsidian),
            StoneTypes::Metamorphic(MetamorphicRocks::Marble),
            StoneTypes::Metamorphic(MetamorphicRocks::Marble),
            StoneTypes::Metamorphic(MetamorphicRocks::Marble),
            StoneTypes::Metamorphic(MetamorphicRocks::Gneiss),
            StoneTypes::Metamorphic(MetamorphicRocks::Gneiss),
            StoneTypes::Metamorphic(MetamorphicRocks::Gneiss),
            StoneTypes::Sedimentary(SedimentaryRocks::Conglomerate),
        ];
        let n = (rn / 30.0) * (choices.len() - 1) as f32;
        choices[n.abs() as usize]
    }
    fn random_highlayer(rn: f32) -> StoneTypes {
        // largely sedimentary, some metamorphic
        let choices = vec![
            StoneTypes::Sedimentary(SedimentaryRocks::Conglomerate),
            StoneTypes::Sedimentary(SedimentaryRocks::Conglomerate),
            StoneTypes::Sedimentary(SedimentaryRocks::Limestone),
            StoneTypes::Sedimentary(SedimentaryRocks::Limestone),
            StoneTypes::Sedimentary(SedimentaryRocks::Limestone),
            StoneTypes::Metamorphic(MetamorphicRocks::Marble),
            StoneTypes::Metamorphic(MetamorphicRocks::Marble),
        ];
        let n = (rn / 30.0) * (choices.len() - 1) as f32;
        choices[n.abs() as usize]
    }
    fn random_upperlayer(rn: f32) -> StoneTypes {
        // only sedimentary
        let choices = vec![
            StoneTypes::Sedimentary(SedimentaryRocks::Conglomerate),
            StoneTypes::Sedimentary(SedimentaryRocks::Limestone),
        ];
        let n = (rn / 30.0) * (choices.len() - 1) as f32;
        choices[n.abs() as usize]
    }

    /// Chooses a type of rock based on the World's stone_noise FBM and height.
    pub fn rock_type(
        &self,
        (x, y): Point2D,
        height: isize,
        props: &GenProps,
    ) -> Tile {
        let rn = self
            .stone_vein_noise
            .get_fbm(&mut [x as f32, y as f32, height as f32], 2)
            * 30.0;
        Tile::Stone(
            // Stone type
            if height <= 2 {
                World::random_lowlayer(rn)
            } else if height as f32 <= props.sea_level - 1.0 {
                World::random_midlayer(rn)
            } else if height as f32 <= props.sea_level + 5.0 {
                World::random_highlayer(rn)
            } else {
                World::random_upperlayer(rn)
            },
            // State
            if height == 0 {
                if rn < 0.0 {
                    State::Liquid
                } else {
                    State::Solid
                }
            } else {
                State::Solid
            },
        )
    }

    /// Gets the correct vegetation based on the heightmap's height and random selection.
    pub fn get_vegetation(
        noise: &Noise,
        (x, y): Point2D,
        biome: Biome,
    ) -> Option<Tile> {
        let vn = noise.get_fbm(&mut [x as f32, y as f32], 1) * 20.0;
        use self::VegType::*;
        let veg_levels = match biome.biome_type {
            BiomeType::Forest => vec![
                vec![ForestGrass2],
                vec![ForestGrass3],
                vec![ForestGrass4],
                vec![ForestGrass1, ForestGrass5],
                vec![ForestGrass4, ForestGrass3],
                vec![ForestTree1(TreePart::All(0, 0))],
            ],
            BiomeType::Desert => vec![
                vec![DesertGrass1],
                vec![DesertGrass2],
                vec![DesertGrass3],
                vec![DesertGrass4, DesertGrass5],
                vec![DesertGrass3, DesertGrass2],
                vec![
                    DesertGrass3,
                    DesertGrass2,
                    DesertTree1(TreePart::All(0, 0)),
                ],
            ],
            BiomeType::Pasture => vec![
                vec![PastureGrass1],
                vec![PastureGrass2],
                vec![PastureGrass3],
                vec![PastureGrass4],
                vec![PastureGrass5],
                vec![
                    PastureGrass5,
                    PastureGrass5,
                    PastureGrass5,
                    PastureGrass3,
                    PastureGrass3,
                    PastureTree1(TreePart::All(0, 0)),
                    PastureTree1(TreePart::All(0, 0)),
                    PastureTree2(TreePart::All(0, 0)),
                ],
            ],
            BiomeType::Beach => vec![
                vec![],
                vec![],
                vec![],
                vec![],
                vec![],
                vec![
                    PalmTree(TreePart::All(0, 0)),
                    PalmTree(TreePart::All(0, 0)),
                    GroupedPalmTree(TreePart::All(0, 0)),
                ],
            ],
            BiomeType::Water => {
                vec![vec![], vec![], vec![], vec![], vec![], vec![]]
            }
        };
        let empty = vec![];
        let mut trng = rand::thread_rng();
        let (vopts, height) = if biome.biome_type
            == BiomeType::Pasture
            || biome.biome_type == BiomeType::Desert
                && biome.temperature_day_f > 35.0
        {
            if vn < 0.0 {
                (&veg_levels[0], 1)
            } else if vn < 2.0 {
                (&veg_levels[1], 1)
            } else if vn < 10.0 {
                (&veg_levels[2], 1)
            } else if vn < 13.0 {
                (&veg_levels[3], 1)
            } else if vn < 15.0 {
                (&veg_levels[3], 1)
            } else if vn < 17.5 {
                if trng.gen_weighted_bool(4) {
                    (&veg_levels[4], 2)
                } else {
                    (&veg_levels[2], 1)
                }
            } else {
                (&veg_levels[5], 4)
            }
        } else if biome.temperature_day_f > 32.0 {
            if vn < 0.0 {
                (&veg_levels[0], 1)
            } else if vn < 2.0 {
                (&veg_levels[1], 1)
            } else if vn < 10.0 {
                (&veg_levels[2], 1)
            } else if vn < 13.0 {
                (&veg_levels[3], 1)
            } else if vn < 15.0 {
                (&veg_levels[4], 1)
            } else if vn < 18.0 {
                (&veg_levels[5], 2)
            } else {
                (&veg_levels[5], 4)
            }
        } else {
            if vn < 15.0 {
                (&empty, 4)
            } else if vn < 18.0 {
                (&veg_levels[5], 3)
            } else {
                (&empty, vn as i32 / 5)
            }
        };

        if vopts.len() != 0 {
            let vn2 = (noise.get_fbm(&mut [x as f32, y as f32], 1)
                * vopts.len() as f32)
                .abs() as usize;
            let v = match vopts[vn2].clone() {
                DesertTree1(tp) => {
                    DesertTree1(tp.set_height(height as usize))
                }
                PastureTree1(tp) => {
                    PastureTree1(tp.set_height(height as usize))
                }
                PastureTree2(tp) => {
                    PastureTree2(tp.set_height(height as usize))
                }
                ForestTree1(tp) => {
                    ForestTree1(tp.set_height(height as usize))
                }
                ForestTree2(tp) => {
                    ForestTree2(tp.set_height(height as usize))
                }
                PalmTree(tp) => {
                    PalmTree(tp.set_height(height as usize))
                }
                GroupedPalmTree(tp) => {
                    GroupedPalmTree(tp.set_height(height as usize))
                }
                x => x,
            };
            Some(Tile::Vegetation(v, height, State::Solid))
        } else {
            None
        }
    }
}

/// Handles general time:
/// * calendar date
/// * time of day (fuzzy)
/// * absolute time (clock)
/// * days since universe start
pub struct TimeHandler {
    pub calendar: Calendar,
    pub time_of_day: Time,
    pub clock: Clock,
    pub days: usize,
}

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Clone, Copy, Hash)]
pub enum ItemType {
    Wood,
    Metal,
    Stone,
    Tool,
}
impl ItemType {
    pub fn to_item(&self) -> Item {
        match self {
            &ItemType::Tool => Item::Tool(Tool::Boots),
            &ItemType::Wood => {
                Item::Material(Material::Wood(VegType::PastureGrass1))
            }
            &ItemType::Metal => Item::Material(Material::Metal(
                MetalType::Bronze,
                MetalState::Molten,
            )),
            &ItemType::Stone => Item::Material(Material::Stone(
                StoneTypes::Igneous(IgneousRocks::Basalt),
            )),
        }
    }
    pub fn to_item_tile(&self) -> Tile {
        Tile::Item(self.to_item())
    }
}

pub type Orders = Vec<(Order, usize)>;

/// Handles overall world state:
///
/// * the 3D screen location and cursor position
/// * the organisms
/// * meta info about the physical aspect of the game and the map itself
/// * and time, including dates and clock time.
///
/// WorldState also handles telling the physical aspect generating a new map,
/// which, for performance reasons, is not requred on the creation of the
/// struct, instead relying on after-the-fact linking.
pub struct WorldState {
    path_receivers: Vec<Receiver<(usize, Option<Vec<Point3D>>)>>,
    pub frame: usize,
    pub diff_queue: Vec<(WorldDiff, usize)>,
    pub xswitch: bool,
    pub yswitch: bool,
    pub screen: (i32, i32),
    pub cursor: (i32, i32),
    pub level: i32,
    pub commands: Orders,
    pub map: Option<World>,
    pub highest_level: usize,
    pub time: TimeHandler,
    /// Represents how much of each type of item the player has
    pub stockpiles: HashMap<ItemType, usize>,
}

impl WorldState {
    fn update_time(&mut self, time: usize, dt: usize) {
        self.time.clock.update_deltatime(dt);
        self.time.time_of_day =
            Time::from_clock_time(&self.time.clock);
        if self.time.time_of_day == Time::Midnight {
            self.time.clock.time = (0, 0);
            self.time.days += 1;
            self.time
                .calendar
                .update_to_day(self.time.days, &self.time.clock);
        }
    }

    fn update_life(&mut self, time: usize, dt: usize) {
        if let Some(ref mut world) = self.map {
            for i in 0..world.life.len() {
                let res = {
                    if let Some(ref actor) = world.life[i] {
                        let mut actor = actor.borrow_mut();
                        let mut orders = self.commands.clone();
                        let res = actor.execute_mission(
                            world,
                            time,
                            dt,
                            self.frame,
                            &mut orders,
                            &mut self.stockpiles,
                        );
                        self.commands = orders;

                        res
                    } else {
                        WorldDiff::NoResult
                    }
                };
                Self::make_modification(
                    world,
                    i,
                    res,
                    &mut self.stockpiles,
                );
            }
        }
    }

    /// Applies a diff structure to the world destructively
    fn make_modification(
        world: &mut World,
        i: usize,
        diff: WorldDiff,
        stockpiles: &mut HashMap<ItemType, usize>,
    ) {
        use self::WorldDiff::*;
        match diff {
            Die => {
                world.kill(i);
            }
            Kill(i) => {
                world.kill(i);
            }
            Move(old, new) => {
                if old != new {
                    if let Some(ref a) = world.life[i] {
                        let old_unit = &world.map[old.1][old.0];
                        let old_arc = old_unit.tiles.clone();
                        let mut old_tiles = old_arc.write().unwrap();
                        let actor = a.borrow();
                        let so = actor.standing_on();
                        old_tiles[old.2] = so;
                    }
                    if let Some(ref a) = world.life[i] {
                        let new_unit = &world.map[new.1][new.0];
                        let new_arc = new_unit.tiles.clone();
                        let mut new_tiles = new_arc.write().unwrap();
                        let mut actor = a.borrow_mut();
                        actor.update_standing_on(
                            new_tiles[new.2],
                            &world.life,
                        );
                        new_tiles[new.2] = Tile::Empty(Some(i));
                    }
                }
            }
            RemoveTile(pnt) => {
                let unit = &world.map[pnt.1][pnt.0];
                let arc = unit.tiles.clone();
                let mut tiles = arc.write().unwrap();
                tiles[pnt.2] = match tiles[pnt.2] {
                    Tile::Empty(id) => Tile::Empty(id),
                    _ => Tile::Empty(None),
                };
            }
            ReplaceTile(pnt, tile) => {
                let unit = &world.map[pnt.1][pnt.0];
                let arc = unit.tiles.clone();
                let mut tiles = arc.write().unwrap();
                tiles[pnt.2] = tile;
            }
            ReplaceTileStack(pnt, ztop, rtiles) => {
                let unit = &world.map[pnt.1][pnt.0];
                let arc = unit.tiles.clone();
                let mut tiles = arc.write().unwrap();
                for z in 0..ztop {
                    tiles[pnt.2 + z] =
                        *rtiles.get(z).unwrap_or(&Tile::Empty(None));
                }
            }
            _ => (),
        }
    }

    /// Adds a diff struct to the queue to be carried out in the regular update
    /// cycle. Diffs are not garunteed to be executed as soon as you queue them,
    /// and in fact probably will not be. Also, they are not garunteed to be
    /// executed in order that they were pushed, although currently they are.
    /// They are also always executed before life is updated and make their
    /// changes.
    pub fn queue_modification(&mut self, i: usize, diff: WorldDiff) {
        self.diff_queue.push((diff, i));
    }

    /// Updates world time and then deligates to the physics engine.
    pub fn update(
        &mut self,
        time: usize,
        dt: usize,
        screen: GameScreen,
    ) {
        self.update_time(time, dt);
        if let Some(ref mut world) = self.map {
            for (diff, i) in self.diff_queue.iter() {
                Self::make_modification(
                    world,
                    *i,
                    diff.clone(),
                    &mut self.stockpiles,
                );
            }
        }

        if screen == GameScreen::Game {
            self.update_life(time, dt);
        }
        //physics::run(self, dt);
    }

    /// Add a world map and update its meta layer.
    pub fn add_map(&mut self, world: World) {
        let max = world
            .map
            .iter()
            .flat_map(|r| {
                r.iter()
                    .map(|unit| {
                        let arc = unit.tiles.clone();
                        let unit = arc.read().unwrap();
                        unit.len()
                    })
                    .max()
            })
            .max();
        self.map = Some(world);
        self.highest_level = max.unwrap_or(11);
        let (cx, cy) = self.map.as_mut().unwrap().generate_life();
        self.screen = (0, 0);
    }

    /// Create a new WorldState, loaded with sensable defaults.
    pub fn new() -> WorldState {
        let clock = Clock { time: (12, 30) };
        WorldState {
            frame: 0,
            diff_queue: vec![],
            xswitch: false,
            yswitch: false,
            commands: vec![],
            stockpiles: HashMap::new(),
            screen: (0, 0),
            path_receivers: vec![],
            level: 20,
            highest_level: 0,
            cursor: (0, 0),
            time: TimeHandler {
                days: 36512,
                calendar: Calendar::new(12, 6, 100, &clock),
                time_of_day: Time::Morning,
                clock: clock,
            },
            map: None,
        }
    }
}
