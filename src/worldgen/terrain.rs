extern crate glutin_window;
extern crate graphics;
extern crate piston_window;
extern crate rand;

use crate::draw::Describe;
use crate::physics::PhysicsActor;
use crate::time::Calendar;

use crate::worldgen;

pub const TILES: bool = true;
pub const BASE: u32 = 256;

/////// ROCK
// Possible igneous rock kinds
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum IgneousRocks {
    Obsidian,
    Basalt,
}

impl Describe for IgneousRocks {
    fn describe(&self) -> String {
        match self {
            &IgneousRocks::Obsidian => {
                "Igneous obisidian".to_string()
            }
            &IgneousRocks::Basalt => "Igneous basalt".to_string(),
        }
    }
}

// Possible metamorphic rock kinds
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MetamorphicRocks {
    Gneiss,
    Marble,
}

impl Describe for MetamorphicRocks {
    fn describe(&self) -> String {
        match self {
            &MetamorphicRocks::Gneiss => {
                "Metamorphic gneiss".to_string()
            }
            &MetamorphicRocks::Marble => {
                "Metamorphic marble".to_string()
            }
        }
    }
}

// Possible Sedimentary rock kinds
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SedimentaryRocks {
    Limestone,
    Conglomerate,
}

impl Describe for SedimentaryRocks {
    fn describe(&self) -> String {
        match self {
            &SedimentaryRocks::Limestone => {
                "Sedimentary limestone".to_string()
            }
            &SedimentaryRocks::Conglomerate => {
                "Sedimentary conglomerate".to_string()
            }
        }
    }
}

// Soil types
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SoilTypes {
    Clay,
    Sandy,
    Silty,
    Peaty,
    Loamy,
    Watery,
    DesertSand,
    Snowy,
    DisturbedDesertSand,
}

impl Describe for SoilTypes {
    fn describe(&self) -> String {
        match self {
            &SoilTypes::Clay => "Clay soil".to_string(),
            &SoilTypes::Sandy => "Sandy soil".to_string(),
            &SoilTypes::Silty => "Silty soil".to_string(),
            &SoilTypes::Snowy => "Snow".to_string(),
            &SoilTypes::Peaty => "Peaty soil".to_string(),
            &SoilTypes::Loamy => "Loamy soil".to_string(),
            &SoilTypes::Watery => "Watery soil".to_string(),
            &SoilTypes::DesertSand => "Desert sand".to_string(),
            &SoilTypes::DisturbedDesertSand => {
                "Desert sand".to_string()
            }
        }
    }
}

// Stone types (SCIENCE!)
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum StoneTypes {
    Sedimentary(SedimentaryRocks),
    Igneous(IgneousRocks),
    Metamorphic(MetamorphicRocks),
    Soil(SoilTypes),
}

impl Describe for StoneTypes {
    fn describe(&self) -> String {
        match self {
            &StoneTypes::Sedimentary(v) => v.describe(),
            &StoneTypes::Igneous(v) => v.describe(),
            &StoneTypes::Metamorphic(v) => v.describe(),
            &StoneTypes::Soil(v) => v.describe(),
        }
    }
}

/////// WATER
// This is a DF-type game, so... extra fidelty!
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum LiquidPurity {
    // Helps with healing
    Pure,
    Clear,
    // Normal
    Clean,
    // Might cause health issues
    Sandy,
    Dirty,
    Murky,
    // Will kill eventually
    Muddy,
    Toxic,
}

/////// VEGITATION
// Vegiatation type, least to most rare, common to least common.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum VegType {
    // Desert
    DesertGrass1,
    DesertGrass2,
    DesertGrass3,
    DesertGrass4,
    DesertGrass5,
    DesertTree1(TreePart),
    // Forest
    ForestGrass1,
    ForestGrass2,
    ForestGrass3,
    ForestGrass4,
    ForestGrass5,
    ForestTree1(TreePart),
    ForestTree2(TreePart),
    // Pasture
    PastureGrass1,
    PastureGrass2,
    PastureGrass3,
    PastureGrass4,
    PastureGrass5,
    PastureTree1(TreePart),
    PastureTree2(TreePart),
    // Beach Biome
    PalmTree(TreePart),
    GroupedPalmTree(TreePart),
}
impl Describe for VegType {
    fn describe(&self) -> String {
        format!("{:?}", self)
    }
}
impl VegType {
    pub fn convert_to_base(&self) -> Self {
        use self::VegType::*;
        match *self {
            PastureTree1(tp) => PastureTree1(TreePart::Base),
            PastureTree2(tp) => PastureTree2(TreePart::Base),
            ForestTree1(tp) => ForestTree1(TreePart::Base),
            ForestTree2(tp) => ForestTree2(TreePart::Base),
            DesertTree1(tp) => DesertTree1(TreePart::Base),
            GroupedPalmTree(tp) => GroupedPalmTree(TreePart::Base),
            PalmTree(tp) => PalmTree(TreePart::Base),
            _ => panic!(
                "Called tree specific function on a {:?}",
                *self
            ),
        }
    }
    pub fn convert_to_trunk(&self) -> Self {
        use self::VegType::*;
        match *self {
            PastureTree1(tp) => PastureTree1(TreePart::Trunk),
            PastureTree2(tp) => PastureTree2(TreePart::Trunk),
            ForestTree1(tp) => ForestTree1(TreePart::Trunk),
            ForestTree2(tp) => ForestTree2(TreePart::Trunk),
            DesertTree1(tp) => DesertTree1(TreePart::Trunk),
            GroupedPalmTree(tp) => GroupedPalmTree(TreePart::Trunk),
            PalmTree(tp) => PalmTree(TreePart::Trunk),
            _ => panic!(
                "Called tree specific function on a {:?}",
                *self
            ),
        }
    }
    pub fn convert_to_midsection(&self) -> Self {
        use self::VegType::*;
        match *self {
            PastureTree1(tp) => PastureTree1(TreePart::Midsection),
            PastureTree2(tp) => PastureTree2(TreePart::Midsection),
            ForestTree1(tp) => ForestTree1(TreePart::Midsection),
            ForestTree2(tp) => ForestTree2(TreePart::Midsection),
            DesertTree1(tp) => DesertTree1(TreePart::Midsection),
            GroupedPalmTree(tp) => {
                GroupedPalmTree(TreePart::Midsection)
            }
            PalmTree(tp) => PalmTree(TreePart::Midsection),
            _ => panic!(
                "Called tree specific function on a {:?}",
                *self
            ),
        }
    }
    pub fn convert_to_branches(&self, x: usize) -> Self {
        use self::VegType::*;
        match *self {
            PastureTree1(tp) => PastureTree1(TreePart::Branches(x)),
            PastureTree2(tp) => PastureTree2(TreePart::Branches(x)),
            ForestTree1(tp) => ForestTree1(TreePart::Branches(x)),
            ForestTree2(tp) => ForestTree2(TreePart::Branches(x)),
            DesertTree1(tp) => DesertTree1(TreePart::Branches(x)),
            GroupedPalmTree(tp) => {
                GroupedPalmTree(TreePart::Branches(x))
            }
            PalmTree(tp) => PalmTree(TreePart::Branches(x)),
            _ => panic!(
                "Called tree specific function on a {:?}",
                *self
            ),
        }
    }
    pub fn convert_to_leaves(&self, x: usize) -> Self {
        use self::VegType::*;
        match *self {
            PastureTree1(tp) => PastureTree1(TreePart::Leaves(x)),
            PastureTree2(tp) => PastureTree2(TreePart::Leaves(x)),
            ForestTree1(tp) => ForestTree1(TreePart::Leaves(x)),
            ForestTree2(tp) => ForestTree2(TreePart::Leaves(x)),
            DesertTree1(tp) => DesertTree1(TreePart::Leaves(x)),
            GroupedPalmTree(tp) => {
                GroupedPalmTree(TreePart::Leaves(x))
            }
            PalmTree(tp) => PalmTree(TreePart::Leaves(x)),
            _ => panic!(
                "Called tree specific function on a {:?}",
                *self
            ),
        }
    }
    pub fn unwrap_to_tree_part(&self) -> Option<TreePart> {
        use self::VegType::*;
        match *self {
            PastureTree1(tp) => Some(tp),
            PastureTree2(tp) => Some(tp),
            ForestTree1(tp) => Some(tp),
            ForestTree2(tp) => Some(tp),
            DesertTree1(tp) => Some(tp),
            GroupedPalmTree(tp) => Some(tp),
            PalmTree(tp) => Some(tp),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TreePart {
    All(usize, usize),
    Base,
    Trunk,
    Midsection,
    Branches(usize),
    Leaves(usize),
}
impl TreePart {
    pub fn set_height(&self, h: usize) -> Self {
        use self::TreePart::*;
        match *self {
            All(w, _) => All(w, h),
            _ => self.clone(),
        }
    }
}

/////// BIOME

type Ferenheight = f32;
type Percent = f32;
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum BiomeType {
    Forest,
    Desert,
    Pasture,
    Beach,
    Water,
}

impl BiomeType {
    pub fn stringified(&self) -> String {
        use self::BiomeType::*;
        match self {
            &Forest => "f",
            &Desert => "d",
            &Pasture => "p",
            &Beach => "b",
            &Water => "w",
        }
        .to_string()
    }
}

#[derive(Copy, Debug, Clone, PartialEq)]
pub struct Biome {
    pub biome_type: BiomeType,
    pub temperature_night_f: Ferenheight,
    pub temperature_day_f: Ferenheight,
    pub percipitation_chance: Percent,
}

pub const WATER_BIOME: Biome = Biome {
    biome_type: BiomeType::Water,
    temperature_night_f: -5.0,
    temperature_day_f: 70.0,
    percipitation_chance: 80.0,
};

/////// GENERAL
// State: the 3 physical forms + fire because it's convenient.
/// Physical state of an object, based on chemistry.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum State {
    Liquid,
    Solid,
    Gas,
}

/// Descriptive alias (hey, I'm a haskell programmer).
pub type Height = i32;
pub type Depth = i32;

/// North is up, South is down, East is left, West is right.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Compass {
    North,
    South,
    East,
    West,
}

/// Tile types that can be defined to be moveable or as ramps.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum RestrictedTile {
    Stone(StoneTypes, State),
    Vegetation(VegType, Height, State),
}

impl Describe for RestrictedTile {
    fn describe(&self) -> String {
        match self {
            &RestrictedTile::Stone(ref s, ref state) => match state {
                &State::Solid => format!("Rough {}", s.describe()),
                &State::Liquid => format!("Molten {}", s.describe()),
                _ => unreachable!(),
            },
            &RestrictedTile::Vegetation(veg, ..) => veg.describe(),
        }
    }
}

/// The
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Tool {
    // Weapons
    Sword,
    Spear,

    // Armor
    Sheild,
    Breastplate,
    Leggings,
    Helmet,
    Boots,
    Shoes,

    // Tools
    Hammer,
    Pickaxe,
    Net,
    FishingPole,
    Fork,
    Knife,
    Spoon,
    Bowl,
    Goblet,
    Cup,
    Plate,
    Wheel,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Item {
    Material(Material),
    Tool(Tool),
}

impl Item {
    pub fn get_type(&self) -> worldgen::ItemType {
        use crate::worldgen::ItemType::*;
        match self {
            &Item::Material(Material::Metal(..)) => Metal,
            &Item::Material(Material::Stone(..)) => Stone,
            &Item::Material(Material::Wood(..)) => Wood,
            &Item::Tool(..) => Tool,
        }
    }
    pub fn convert_to_tile(&self) -> Option<Tile> {
        use crate::worldgen::Item::*;
        match self {
            Material(_) => Some(Tile::SolidBlock(self.clone())),
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MetalState {
    Ore,
    Solid,
    Molten,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MetalType {
    Gold,
    Bronze,
    Iron,
    Steel,
    Copper,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Material {
    Wood(VegType),
    Stone(StoneTypes),
    Metal(MetalType, MetalState),
}

impl Describe for Material {
    fn describe(&self) -> String {
        match self {
            &Material::Wood(vt) => vt.describe(),
            &Material::Stone(st) => st.describe(),
            &Material::Metal(st, s) => format!("{:?} {:?}", st, s),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Magic {
    potency: u8,
    cursed: bool,
    dates: Option<Calendar>,
}

type Weight = u8;
type Length = u8;
/// General types of tiles (very broad) and their current state.
///
/// FIXME: use restricted tile instead of duplication
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Tile {
    Empty(Option<usize>),
    Ramp(RestrictedTile),
    Moveable(RestrictedTile),
    Item(Item),
    Water(LiquidPurity, State, Depth),
    Stone(StoneTypes, State),
    Vegetation(VegType, Height, State),
    Fire,
    SolidBlock(Item),
}

impl PhysicsActor for Tile {
    fn solid(&self) -> bool {
        match self {
            &Tile::Stone(..) => true,
            &Tile::Ramp(..) => true,
            &Tile::Vegetation(..) => true,
            _ => false,
        }
    }

    fn heavy(&self) -> bool {
        match self {
            &Tile::Stone(..) => true,
            &Tile::Moveable(..) => true,
            _ => false,
        }
    }
}

impl Describe for Tile {
    fn describe(&self) -> String {
        match self {
            &Tile::Item(i) => match i {
                Item::Tool(t) => format!("A {:?}", t),
                Item::Material(t) => {
                    format!("A chunk of {}", t.describe())
                }
            },
            &Tile::SolidBlock(_) => "foo".to_string(),
            &Tile::Empty(_) => "Empty space".to_string(),
            &Tile::Ramp(_) => "A ramp".to_string(),
            &Tile::Moveable(ref s) => {
                format!("Loose pile of {}", s.describe())
            }
            &Tile::Water(ref purity, ref state, _) => {
                let purity_str = match purity {
                    &LiquidPurity::Clean => "clean",
                    &LiquidPurity::Clear => "clear",
                    &LiquidPurity::Dirty => "dirty",
                    &LiquidPurity::Muddy => "muddy",
                    &LiquidPurity::Murky => "murky",
                    &LiquidPurity::Pure => "pure",
                    &LiquidPurity::Sandy => "sandy",
                    &LiquidPurity::Toxic => "toxic",
                };
                match state {
                    &State::Gas => {
                        format!("Cloud of {} steam", purity_str)
                    }
                    &State::Solid => format!("{} ice", purity_str),
                    &State::Liquid => format!("{} water", purity_str),
                }
            }
            &Tile::Stone(ref s, ref state) => match state {
                &State::Solid => format!("Rough {}", s.describe()),
                &State::Liquid => format!("Molten {}", s.describe()),
                _ => unreachable!(),
            },
            &Tile::Fire => "Flames".to_string(),
            &Tile::Vegetation(veg, ..) => veg.describe(),
        }
    }
}
